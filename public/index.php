<?php

require __DIR__ . '/../vendor/autoload.php';

use DI\ContainerBuilder;
use Slim\Factory\AppFactory;

$configPath = __DIR__ . "/../config";
// Instantiate PHP-DI ContainerBuilder
$containerBuilder = new ContainerBuilder();

if (false) { // Should be set to true in production
    //TODO: GAE check which dir is writable
    $containerBuilder->enableCompilation(__DIR__ . '/../var/cache');
}

// Set up settings
$settings = require "{$configPath}/settings.php";
$settings($containerBuilder);

// Set up dependencies
$dependencies = require "{$configPath}/dependencies.php";
$dependencies($containerBuilder);

// Build PHP-DI Container instance
$container = $containerBuilder->build();

// Instantiate the app
AppFactory::setContainer($container);
$app = AppFactory::create();

// Register middleware
$middleware = require "{$configPath}/middleware.php";
$middleware($app);

// Register routes
$routes = require "{$configPath}/routes.php";
$routes($app);

// Add Routing Middleware
$app->addRoutingMiddleware();

// Run App
$app->run();
//echo "<pre>";
//print_r($settings);
//exit;

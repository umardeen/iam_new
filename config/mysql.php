<?php

return function () {
   return [
        'meta' => [
            'entity_path' => [
                'src/App/Entity'
            ],
            'auto_generate_proxies' => true,
            'proxy_dir' => __DIR__ . '/../cache/proxies',
            'cache' => null,
        ],
        'connection' => [
            'master' => [
                'driver' => 'pdo_mysql',
                'host' => 'localhost',
                'dbname' => 'iam',
                'user' => 'root',
                'password' => 'Devesh@123',
                'charset' => 'utf8'
            ],
            'slave' => [
                'driver' => 'pdo_mysql',
                'host' => 'localhost',
                'dbname' => 'iam',
                'user' => 'root',
                'password' => 'Devesh@123',
                'charset' => 'utf8',
                'sessionVariables' => array(
                    'in_predicate_conversion_threshold' => 0
                )
            ]
    ]];
};

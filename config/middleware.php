<?php

declare(strict_types=1);

use Slim\App;

return function (App $app) {
    $container = $app->getContainer();
    $app->add(new \App\Middleware\ApiLogMiddleware($container));
    $app->add(new \App\Middleware\AuthMiddleware($container));
    
};

<?php

declare(strict_types=1);

use DI\ContainerBuilder;
use Psr\Container\ContainerInterface;

return function (ContainerBuilder $containerBuilder) {
    $dependencies = array();
    $dependencies['exDbCon'] = function ($c) {
        return \App\ExternalDb::_getInstance($c);
    };
    // MongoDatabase
    $dependencies['mongo'] = function ($c) {
        $settings = $c->get('settings');
        try {
            if (
                    isset($settings['mongo']['user']) && $settings['mongo']['user'] &&
                    isset($settings['mongo']['password']) && $settings['mongo']['password']
            ) {
                $connectionStr = 'mongodb://' . $settings['mongo']['user'] . ':' . $settings['mongo']['password'] . '@' . $settings['mongo']['host'] . ':' . $settings['mongo']['port'] . $settings['mongo']['role'];
            } else {
                $connectionStr = 'mongodb://' . $settings['mongo']['host'] . ':' . $settings['mongo']['port'] . '';
            }
//            $connection = new MongoClient($connectionStr);
            $connection = new MongoDB\Client($connectionStr);
//            $mongoDb = $connection->$settings['mongo']['dbname'];
//            echo "<pre>";
//            var_dump($connection);
//            exit;
//            return $mongoDb;
            return array("connection" => $connection, "config" => $settings['mongo']);
        } catch (Exception $e) {
            return false;
        }
    };
    $dependencies['em'] = function ($c) {
        try {
            $settings = $c->get('settings');
            $config = \Doctrine\ORM\Tools\Setup::createAnnotationMetadataConfiguration(
                            $settings['doctrine']['meta']['entity_path'],
                            $settings['doctrine']['meta']['auto_generate_proxies'],
                            $settings['doctrine']['meta']['proxy_dir'],
                            !empty($settings['doctrine']['meta']['cache']) ? $settings['doctrine']['meta']['cache'] : NULL,
                            false
            );
            $em = \Doctrine\ORM\EntityManager::create($settings['doctrine']['connection']['master'], $config);
//
            $em->getConfiguration()->addCustomStringFunction('TIMESTAMPDIFF', 'DoctrineExtensions\Query\Mysql\TimestampDiff');
            $em->getConfiguration()->addCustomStringFunction('GROUP_CONCAT', 'DoctrineExtensions\Query\Mysql\GroupConcat');
            $em->getConfiguration()->addCustomStringFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');
            $em->getConfiguration()->addCustomStringFunction('ConvertTz', 'DoctrineExtensions\Query\Mysql\ConvertTz');
            $em->getConfiguration()->addCustomStringFunction('DateFormat', 'DoctrineExtensions\Query\Mysql\DateFormat');
            $em->getConfiguration()->addCustomStringFunction('DATE_FORMAT', 'DoctrineExtensions\Query\Mysql\DateFormat');
            $em->getConfiguration()->addCustomStringFunction('DateDiff', 'DoctrineExtensions\Query\Mysql\DateDiff');
            $em->getConfiguration()->addCustomStringFunction('TimeDiff', 'DoctrineExtensions\Query\Mysql\TimeDiff');
            $em->getConfiguration()->addCustomStringFunction('Hour', 'DoctrineExtensions\Query\Mysql\Hour');
            $em->getConfiguration()->addCustomStringFunction('Minute', 'DoctrineExtensions\Query\Mysql\Minute');
            $em->getConfiguration()->addCustomStringFunction('Second', 'DoctrineExtensions\Query\Mysql\Second');
            $em->getConfiguration()->addCustomStringFunction('Year', 'DoctrineExtensions\Query\Mysql\Year');
            $em->getConfiguration()->addCustomStringFunction('DateAdd', 'DoctrineExtensions\Query\Mysql\DateAdd');
            $em->getConfiguration()->addCustomStringFunction('Now', 'DoctrineExtensions\Query\Mysql\Now');
            $em->getConfiguration()->addCustomStringFunction('FindInSet', 'DoctrineExtensions\Query\Mysql\FindInSet');
            $em->getConfiguration()->addCustomStringFunction('IfElse', 'DoctrineExtensions\Query\Mysql\IfElse');
//
//            $doctrineEventManager = $em->getEventManager();
////            $doctrineEventManager->addEventListener(
////                    array(\Doctrine\ORM\Events::postUpdate, \Doctrine\ORM\Events::postPersist, \Doctrine\ORM\Events::preRemove),
////                    new \App\Entity\Logger\Logger($em)
////            );

            return $em;
        } catch (Doctrine_Connection_Exception $e) {
            echo 'Code : ' . $e->getPortableCode();
            echo 'Message : ' . $e->getPortableMessage();
        }
    };

    $dependencies['App\Controller\UsersController'] = function ($c) {
        $userModel = new \App\Model\UserModel($c);
        $userTokenModel = new \App\Model\UserTokenModel($c);
        $CommonUtilsModel = new \App\Model\CommonUtilsModel($c);
        return new \App\Controller\UsersController($c, $userModel, $userTokenModel, $CommonUtilsModel);
    };

    $dependencies['App\Controller\RoleController'] = function ($c) {
        $roleModel = new \App\Model\RoleModel($c);
        return new \App\Controller\RoleController($c, $roleModel);
    };
    
    $dependencies['App\Controller\GroupController'] = function ($c) {
        $groupModel = new \App\Model\GroupModel($c);
        return new \App\Controller\GroupController($groupModel);
    };

    $containerBuilder->addDefinitions($dependencies);
};

<?php

declare(strict_types=1);

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\App;
use Slim\Interfaces\RouteCollectorProxyInterface as Group;

return function (App $app) {
    $app->options('/{routes:.*}', function (Request $request, Response $response) {
        // CORS Pre-Flight OPTIONS Request Handler
        return $response;
    });

    $app->get('/', function (Request $request, Response $response) {
        $response->getBody()->write('Hello world!');
        return $response;
    });

    $app->group('/api/v1/user/auth', function (Group $group) {
//        GET
        $group->get('/getParentOfUser/{userId}', 'App\Controller\UsersController:getParentUser')->setName('getParentUser');
        $group->get('/getPermission', 'App\Controller\UsersController:getPermissions')->setName('getPermissions');
//        POST
        $group->post('/login', 'App\Controller\UsersController:login')->setName('Login');
        $group->post('/logout', 'App\Controller\UsersController:logout')->setName('Logout');
        $group->post('/changePassword', 'App\Controller\UsersController:changePassword')->setName('changePassword');
        $group->post('/allLogout', 'App\Controller\UsersController:alllogout')->setName('All_Logout');
        $group->post('/saveUserCredential', 'App\Controller\UsersController:saveUserCredential')->setName('saveUserCredential');
//        PUT
    });
    $app->group('/api/v1/user/account', function (Group $group) {
//        GET
        $group->get('/list', 'App\Controller\UsersController:listAllUsers')->setName('listUsers');
        $group->get('/parent', 'App\Controller\UsersController:getUserParents')->setName('UserParents');
        $group->get('/{id}', 'App\Controller\UsersController:getUser')->setName('ViewUser');

//        POST
        $group->post('', 'App\Controller\UsersController:addUser')->setName('AddUser');
        $group->post('/addUserToGroups', 'App\Controller\UsersController:addUserToGroups')->setName('addUserToGroups');
        $group->post('/migrateUsers', 'App\Controller\UsersController:migrateUsers')->setName('migrateUsers');

//        PUT
        $group->put('/{id}', 'App\Controller\UsersController:updateUser')->setName('UpdateUser');

//        Delete
        $group->delete('/{id}', 'App\Controller\UsersController:deleteUser')->setName('DeleteUser');
    });

    $app->group('/api/v1/role', function (Group $group) {
//        GET
        $group->get('/list', 'App\Controller\RoleController:listAllRoles')->setName('ListRoles');
        $group->get('/getAllRoles', 'App\Controller\RoleController:getAllRoles')->setName('getAllRoles');
        $group->get('/{id}', 'App\Controller\RoleController:getRole')->setName('ViewRole');
        $group->get('/getPermissions/{roleId}', 'App\Controller\RoleController:getPermissions')->setName('getPermissions');

//        POST
        $group->post('', 'App\Controller\RoleController:addRole')->setName('AddRole');
        $group->post('/updatePermissions', 'App\Controller\RoleController:updatePermissions')->setName('updatePermissions');

//        PUT
        $group->put('/{id}', 'App\Controller\RoleController:updateRole')->setName('UpdateRole');

//        Delete
        $group->delete('/{id}', 'App\Controller\RoleController:deleteRole')->setName('DeleteRole');
    });

    $app->group('/api/v1/group', function (Group $group) {
//        GET
        $group->get('/list', 'App\Controller\GroupController:getAllGroups')->setName('getAllGroups');

//        POST
        $group->post('', 'App\Controller\GroupController:saveGroup')->setName('saveGroup');

//        PUT
        $group->put('/{id}', 'App\Controller\GroupController:updateGroup')->setName('updateGroup');

//        Delete
        $group->delete('/{id}', 'App\Controller\GroupController:deleteGroup')->setName('deleteGroup');
    });
};

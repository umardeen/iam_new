<?php

declare(strict_types=1);

use DI\ContainerBuilder;
//use Monolog\Logger;

return function (ContainerBuilder $containerBuilder) {
    $configPath = __DIR__;
    $mysql = require "{$configPath}/mysql.php";
    $mongo = require "{$configPath}/mongo.php";
    // Global Settings Object
    $containerBuilder->addDefinitions([
        'settings' => [
            'displayErrorDetails' => true, // set to false in production
            'addContentLengthHeader' => false, // Allow the web server to send the content-length header
            // Renderer settings
            'renderer' => [
                'template_path' => __DIR__ . '/../templates/',
            ],
            // Monolog settings
            'logger' => [
                'name' => 'slim-app',
            ],
            'mongo' => $mongo(),
            'doctrine' => $mysql(),
        ],
    ]);
};

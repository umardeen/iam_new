<?php
namespace App\Entity;

use App\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="tbl_roles", uniqueConstraints={@ORM\UniqueConstraint(name="id", columns={"id"})}))
 */
class Role
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="name_disp", type="string", length=50, nullable=true)
     */
    private $nameDisp;

    /**
     * @var boolean
     *
     * @ORM\Column(name="status", type="boolean", nullable=true)
     */
    private $status;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_deleted", type="boolean", nullable=false)
     */
    private $is_deleted = '0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=true)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=true)
     */
    private $modified;

    /**
     * The "one" side is ALWAYS the INVERSE side in a OneToMany bidirectional relation 
     * 
     * @ORM\OneToMany(targetEntity="RoleCapabilities", mappedBy="role", cascade={"persist", "remove"})
     * The mappedBy attribute contains the name of the association-field on the owning side.
     * 
     */
    //private $capabilities;
    
    /**
     * @ORM\OneToMany(targetEntity="User", mappedBy="role")
     */
    private $users;

    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->users = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return TblRoles
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set nameDisp
     *
     * @param string $nameDisp
     *
     * @return TblRoles
     */
    public function setNameDisp($nameDisp)
    {
        $this->nameDisp = $nameDisp;
    
        return $this;
    }

    /**
     * Get nameDisp
     *
     * @return string
     */
    public function getNameDisp()
    {
        return $this->nameDisp;
    }

    /**
     * Set status
     *
     * @param boolean $status
     *
     * @return TblRoles
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return boolean
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set isDeleted
     *
     * @param boolean $isDeleted
     *
     * @return TblRoles
     */
    public function setIsDeleted($isDeleted)
    {
        $this->is_deleted = $isDeleted;
    
        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return boolean
     */
    public function getIsDeleted()
    {
        return $this->is_deleted;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return TblRoles
     */
    public function setCreated($created)
    {
        $this->created = $created;
    
        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     *
     * @return TblRoles
     */
    public function setModified($modified)
    {
        $this->modified = $modified;
    
        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime
     */
    public function getModified()
    {
        return $this->modified;
    }
    
    /**
     * Get array copy of object
     *
     * @return array
     */
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
    
    public function objectToArray($object) {
       if (!is_object($object) && !is_array($object)) {
           return $object;
       }
       if (is_object($object) && !preg_match("/entity/i", get_class($object))) {
           return $object;
       }
       if (is_object($object) && preg_match("/entity/i", get_class($object))) {
           $object = $object->getArrayCopy();
       }
       return array_map(
                       function ($objList) {
                   return $this->objectToArray($objList);
               }, $object
               );
   }
}

<?php

namespace App\Entity;
use App\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * OtpDetails
 *
 * @ORM\Table(name="tbl_otp_details")
 * @ORM\Entity
 */
class OtpDetails
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="otp_value", type="string", length=20, nullable=true)
     */
    private $otpValue;

    /**
     * @var integer
     *
     * @ORM\Column(name="request_id", type="integer", nullable=true)
     */
    private $requestId;

    /**
     * @var string
     *
     * @ORM\Column(name="mobile_number", type="string", length=15, nullable=true)
     */
    private $mobileNumber;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_verified", type="boolean", nullable=true)
     */
    private $isVerified = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_resend", type="boolean", nullable=true)
     */
    private $isResend = '0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=true)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=true)
     */
    private $modified;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set otpValue
     *
     * @param string $otpValue
     *
     * @return OtpDetails
     */
    public function setOtpValue($otpValue)
    {
        $this->otpValue = $otpValue;

        return $this;
    }

    /**
     * Get otpValue
     *
     * @return string
     */
    public function getOtpValue()
    {
        return $this->otpValue;
    }

    /**
     * Set requestId
     *
     * @param integer $requestId
     *
     * @return OtpDetails
     */
    public function setRequestId($requestId)
    {
        $this->requestId = $requestId;

        return $this;
    }

    /**
     * Get requestId
     *
     * @return integer
     */
    public function getRequestId()
    {
        return $this->requestId;
    }

    /**
     * Set mobileNumber
     *
     * @param string $mobileNumber
     *
     * @return OtpDetails
     */
    public function setMobileNumber($mobileNumber)
    {
        $this->mobileNumber = $mobileNumber;

        return $this;
    }

    /**
     * Get mobileNumber
     *
     * @return string
     */
    public function getMobileNumber()
    {
        return $this->mobileNumber;
    }

    /**
     * Set isVerified
     *
     * @param boolean $isVerified
     *
     * @return OtpDetails
     */
    public function setIsVerified($isVerified)
    {
        $this->isVerified = $isVerified;

        return $this;
    }

    /**
     * Get isVerified
     *
     * @return boolean
     */
    public function getIsVerified()
    {
        return $this->isVerified;
    }

    /**
     * Set isResend
     *
     * @param boolean $isResend
     *
     * @return OtpDetails
     */
    public function setIsResend($isResend)
    {
        $this->isResend = $isResend;

        return $this;
    }

    /**
     * Get isResend
     *
     * @return boolean
     */
    public function getIsResend()
    {
        return $this->isResend;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return OtpDetails
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     *
     * @return OtpDetails
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime
     */
    public function getModified()
    {
        return $this->modified;
    }
}

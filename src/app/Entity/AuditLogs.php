<?php

namespace App\Entity;
use App\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
 * AuditLogs
 *
 * @ORM\Table(name="tbl_audit_logs")
 * @ORM\Entity
 */
class AuditLogs
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="bunch_id", type="string", length=45, nullable=true)
     */
    private $bunchId;

    /**
     * @var string
     *
     * @ORM\Column(name="ref_id", type="string", length=45, nullable=true)
     */
    private $refId;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="ticket_id", type="integer", nullable=true)
     */
    private $ticketId;
    
    /**
     * @var string
     *
     * @ORM\Column(name="user_id", type="string", length=45, nullable=true)
     */
    private $userId;

    /**
     * @var string
     *
     * @ORM\Column(name="table_name", type="string", length=45, nullable=true)
     */
    private $tableName;

    /**
     * @var string
     *
     * @ORM\Column(name="column_name", type="string", length=45, nullable=true)
     */
    private $columnName;

    /**
     * @var string
     *
     * @ORM\Column(name="old_value", type="text", length=65535, nullable=true)
     */
    private $oldValue;

    /**
     * @var string
     *
     * @ORM\Column(name="new_value", type="text", length=65535, nullable=true)
     */
    private $newValue;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_time", type="datetime", nullable=true)
     */
    private $dateTime = '0000-00-00 00:00:00';

    /**
     * @var string
     *
     * @ORM\Column(name="action", type="string", length=45, nullable=true)
     */
    private $action;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set bunchId
     *
     * @param string $bunchId
     *
     * @return TblAuditLogs
     */
    public function setBunchId($bunchId)
    {
        $this->bunchId = $bunchId;

        return $this;
    }

    /**
     * Get bunchId
     *
     * @return string
     */
    public function getBunchId()
    {
        return $this->bunchId;
    }

    /**
     * Set refId
     *
     * @param string $refId
     *
     * @return TblAuditLogs
     */
    public function setRefId($refId)
    {
        $this->refId = $refId;

        return $this;
    }

    /**
     * Get refId
     *
     * @return string
     */
    public function getRefId()
    {
        return $this->refId;
    }
    
        /**
     * Set ticketId
     *
     * @param integer $ticketId
     *
     * @return TblAuditLogs
     */
    public function setTicketId($ticketId)
    {
        $this->ticketId = $ticketId;

        return $this;
    }

    /**
     * Get ticketId
     *
     * @return integer
     */
    public function getTicketId()
    {
        return $this->ticketId;
    }


    /**
     * Set userId
     *
     * @param string $userId
     *
     * @return TblAuditLogs
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return string
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set tableName
     *
     * @param string $tableName
     *
     * @return TblAuditLogs
     */
    public function setTableName($tableName)
    {
        $this->tableName = $tableName;

        return $this;
    }

    /**
     * Get tableName
     *
     * @return string
     */
    public function getTableName()
    {
        return $this->tableName;
    }

    /**
     * Set columnName
     *
     * @param string $columnName
     *
     * @return TblAuditLogs
     */
    public function setColumnName($columnName)
    {
        $this->columnName = $columnName;

        return $this;
    }

    /**
     * Get columnName
     *
     * @return string
     */
    public function getColumnName()
    {
        return $this->columnName;
    }

    /**
     * Set oldValue
     *
     * @param string $oldValue
     *
     * @return TblAuditLogs
     */
    public function setOldValue($oldValue)
    {
        $this->oldValue = $oldValue;

        return $this;
    }

    /**
     * Get oldValue
     *
     * @return string
     */
    public function getOldValue()
    {
        return $this->oldValue;
    }

    /**
     * Set newValue
     *
     * @param string $newValue
     *
     * @return TblAuditLogs
     */
    public function setNewValue($newValue)
    {
        $this->newValue = $newValue;

        return $this;
    }

    /**
     * Get newValue
     *
     * @return string
     */
    public function getNewValue()
    {
        return $this->newValue;
    }

    /**
     * Set dateTime
     *
     * @param \DateTime $dateTime
     *
     * @return TblAuditLogs
     */
    public function setDateTime($dateTime)
    {
        $this->dateTime = $dateTime;

        return $this;
    }

    /**
     * Get dateTime
     *
     * @return \DateTime
     */
    public function getDateTime()
    {
        return $this->dateTime;
    }

    /**
     * Set action
     *
     * @param string $action
     *
     * @return TblAuditLogs
     */
    public function setAction($action)
    {
        $this->action = $action;

        return $this;
    }

    /**
     * Get action
     *
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }
}

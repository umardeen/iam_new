<?php
namespace App\Entity;

use App\Entity;
use Doctrine\ORM\Mapping as ORM;
use App\Model\CommonUtilsModel;

/**
 * Users
 *
 * @ORM\Table(name="tbl_users")
 * @ORM\Entity
 */
class Users
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="parent_user_id", type="integer", nullable=false)
     */
    private $parentUserId;

    /**
     * @var string
     *
     * @ORM\Column(name="user_name", type="string", length=255, nullable=false)
     */
    private $userName;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255, nullable=false)
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=false)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="mobile", type="string", length=20, nullable=false)
     */
    private $mobile;
    /**
     * @var string
     *
     * @ORM\Column(name="emp_code", type="string", length=45, nullable=true)
     */
    private $empCode;

    /**
     * @var string
     *
     * @ORM\Column(name="gender", type="integer",length=1, nullable=false)
     */
    private $gender = '0';
    
    /**
     * @var integer
     *
     * @ORM\Column(name="bucket_size", type="integer",length=1, nullable=false)
     */
    private $bucketSize = 0;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="ranking", type="integer",length=1, nullable=false)
     */
    private $ranking = 0;
    
    /**
     * @var string
     *
     * @ORM\Column(name="user_type", type="string",length=50, nullable=true)
     */
    private $userType = 'itms';
    

     /**
    * @var string
    *
    * @ORM\Column(name="customer_uuid", type="string",length=36, nullable=true)
    */
    private $customerUuid;
    
     /**
    * @var string
    *
    * @ORM\Column(name="sub_sources", type="string",length=1000, nullable=true)
    */
    private $subSources;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt = '0000-00-00 00:00:00';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified_at", type="datetime", nullable=false)
     */
    private $modifiedAt = 'CURRENT_TIMESTAMP';

    /**
     * @var integer
     *
     * @ORM\Column(name="created_by", type="integer", nullable=false)
     */
    private $createdBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="modified_by", type="integer", nullable=false)
     */
    private $modifiedBy;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="sfa_user_id", type="integer", nullable=false)
     */
    private $sfaUserId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="status", type="boolean", nullable=false)
     */
    private $status = '1';
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="is_deleted", type="boolean", nullable=false)
     */
    private $isDeleted = '0';
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="is_super_admin", type="boolean", nullable=false)
     */
    private $isSuperAdmin = '0';
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="access_to_own_team_only", type="boolean", nullable=false)
     */
    private $accessOwnTeamOnly = '0';
    
    /**
     * @var integer
     *
     * @ORM\Column(name="own_team_group_id", type="integer", nullable=false)
     */
    private $ownTeamGroupId = 0;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="allowed_allocation", type="integer", nullable=false)
     */
    private $allowedAllocation = 0;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="is_task_assigned", type="integer", nullable=false)
     */
    private $isTaskAssigned = 0;
    
    
    /**
     * @var integer
     *
     * @ORM\Column(name="aggregator_id", type="integer", nullable=true)
     */
    private $aggregatorId;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="is_login", type="boolean", nullable=false)
     */
    private $isLogin = '0';
    
     /**
     * @var integer
     *
     * @ORM\Column(name="show_region_ticket_only", type="integer", nullable=false)
     */
    private $showRegionTicketOnly = 0;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set parentUserId
     *
     * @param integer $parentUserId
     *
     * @return Users
     */
    public function setParentUserId($parentUserId)
    {
        $this->parentUserId = $parentUserId;

        return $this;
    }

    /**
     * Get parentUserId
     *
     * @return integer
     */
    public function getParentUserId()
    {
        return $this->parentUserId;
    }

    /**
     * Set userName
     *
     * @param string $userName
     *
     * @return Users
     */
    public function setUserName($userName)
    {
        $this->userName = $userName;

        return $this;
    }

    /**
     * Get userName
     *
     * @return string
     */
    public function getUserName()
    {
        return $this->userName;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return Users
     */
    public function setPassword($password)
    {
        $this -> password = CommonUtilsModel::generatePassword($password);

        return $this;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Users
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Users
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set mobile
     *
     * @param string $mobile
     *
     * @return Users
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;

        return $this;
    }

    /**
     * Get mobile
     *
     * @return string
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * Set empCode
     *
     * @param string $empCode
     *
     * @return Users
     */
    public function setEmpCode($empCode)
    {
        $this->empCode = $empCode;

        return $this;
    }

    /**
     * Get empCode
     *
     * @return string
     */
    public function getEmpCode()
    {
        return $this->empCode;
    }

    /**
     * Set gender
     *
     * @param string $gender
     *
     * @return Users
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender
     *
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }
    
    /**
     * Set bucketSize
     *
     * @param string $bucketSize
     *
     * @return Users
     */
    public function setBucketSize($bucketSize)
    {
        $this->bucketSize = $bucketSize;

        return $this;
    }

    /**
     * Get bucketSize
     *
     * @return string
     */
    public function getBucketSize()
    {
        return $this->bucketSize;
    }
    
    /**
     * Set ranking
     *
     * @param string $ranking
     *
     * @return Users
     */
    public function setRanking($ranking)
    {
        $this->ranking = $ranking;

        return $this;
    }

    /**
     * Get ranking
     *
     * @return string
     */
    public function getRanking()
    {
        return $this->ranking;
    }

    /**
     * Set userType
     *
     * @param string $userType
     *
     * @return Users
     */
    public function setUserType($userType)
    {
        $this->userType = $userType;

        return $this;
    }

    /**
     * Get userType
     *
     * @return string
     */
    public function getUserType()
    {
        return $this->userType;
    }
    

    /**
    * Set customerUuid
    *
    * @param string $customerUuid
    *
    * @return Users
    */
   

    public function setCustomerUuid($customerUuid)
    {
        $this->customerUuid = $customerUuid;

        return $this;
    }


   /**
    * Get customerUuid
    *
    * @return string
    */

    public function getCustomerUuid()
    {
        return $this->customerUuid;
    }


    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Users
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set modifiedAt
     *
     * @param \DateTime $modifiedAt
     *
     * @return Users
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    /**
     * Get modifiedAt
     *
     * @return \DateTime
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * Set createdBy
     *
     * @param integer $createdBy
     *
     * @return Users
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return integer
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set modifiedBy
     *
     * @param integer $modifiedBy
     *
     * @return Users
     */
    public function setModifiedBy($modifiedBy)
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    /**
     * Get modifiedBy
     *
     * @return integer
     */
    public function getModifiedBy()
    {
        return $this->modifiedBy;
    }

    /**
     * Set status
     *
     * @param boolean $status
     *
     * @return Users
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean
     */
    public function getStatus()
    {
        return $this->status;
    }
    
    /**
     * Set isDeleted
     *
     * @param boolean $isDeleted
     *
     * @return Users
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return boolean
     */
    public function getIsDeleted()
    {
        return $this->isDeleted;
    }
    
    /**
     * Set isSuperAdmin
     *
     * @param boolean $isSuperAdmin
     *
     * @return Users
     */
    public function setIsSuperAdmin($isSuperAdmin)
    {
        $this->isSuperAdmin = $isSuperAdmin;

        return $this;
    }

    /**
     * Get isSuperAdmin
     *
     * @return boolean
     */
    public function getIsSuperAdmin()
    {
        return $this->isSuperAdmin;
    }
    
    /**
     * Set accessOwnTeamOnly
     *
     * @param boolean $accessOwnTeamOnly
     *
     * @return Users
     */
    public function setAccessOwnTeamOnly($accessOwnTeamOnly)
    {
        $this->accessOwnTeamOnly = $accessOwnTeamOnly;

        return $this;
    }

    /**
     * Get accessOwnTeamOnly
     *
     * @return boolean
     */
    public function getAccessOwnTeamOnly()
    {
        return $this->accessOwnTeamOnly;
    }
    
    /**
     * Set ownTeamGroupId
     *
     * @param boolean $ownTeamGroupId
     *
     * @return Users
     */
    public function setOwnTeamGroupId($ownTeamGroupId)
    {
        $this->ownTeamGroupId = $ownTeamGroupId;

        return $this;
    }

    /**
     * Get ownTeamGroupId
     *
     * @return boolean
     */
    public function getOwnTeamGroupId()
    {
        return $this->ownTeamGroupId;
    }
    

    /**
     * Get array copy of object
     *
     * @return array
     */
    public function getArrayCopy() {
        return get_object_vars($this);
    }

    public function objectToArray($object) {
       if (!is_object($object) && !is_array($object)) {
           return $object;
       }
       if (is_object($object) && !preg_match("/entity/i", get_class($object))) {
           return $object;
       }
       if (is_object($object) && preg_match("/entity/i", get_class($object))) {
           $object = $object->getArrayCopy();
       }
       return array_map(
                       function ($objList) {
                   return $this->objectToArray($objList);
               }, $object
               );
   }
   
   public function unsetVariable($key){
       if(!empty($key) && isset($this->{$key}))
            unset($this->{$key});
   }

    /**
     * Set aggregatorId
     *
     * @param integer $aggregatorId
     *
     * @return Users
     */
    public function setAggregatorId($aggregatorId)
    {
        $this->aggregatorId = $aggregatorId;

        return $this;
    }

    /**
     * Get aggregatorId
     *
     * @return integer
     */
    public function getAggregatorId()
    {
        return $this->aggregatorId;
    }
    
    /**
     * Set isTaskAssigned
     *
     * @param integer $isTaskAssigned
     *
     * @return Users
     */
    public function setIsTaskAssigned($isTaskAssigned)
    {
        $this->isTaskAssigned = $isTaskAssigned;

        return $this;
    }

    /**
     * Get isTaskAssigned
     *
     * @return integer
     */
    public function getIsTaskAssigned()
    {
        return $this->isTaskAssigned;
    }
    
    /**
     * Set allowedAllocation
     *
     * @param integer $allowedAllocation
     *
     * @return Users
     */
    public function setAllowedAllocation($allowedAllocation)
    {
        $this->allowedAllocation = $allowedAllocation;

        return $this;
    }

    /**
     * Get allowedAllocation
     *
     * @return integer
     */
    public function getAllowedAllocation()
    {
        return $this->allowedAllocation;
    }
    
    /**
     * Set isLogin
     *
     * @param integer $isLogin
     *
     * @return Users
     */
    public function setIsLogin($isLogin)
    {
        $this->isLogin = $isLogin;

        return $this;
    }

    /**
     * Get isLogin
     *
     * @return integer
     */
    public function getIsLogin()
    {
        return $this->isLogin;
    }
    
    /**
     * Set sfaUserId
     *
     * @param integer $sfaUserId
     *
     * @return Users
     */
    public function setSfaUserId($sfaUserId)
    {
        $this->sfaUserId = $sfaUserId;

        return $this;
    }

    /**
     * Get sfaUserId
     *
     * @return integer
     */
    public function getSfaUserId()
    {
        return $this->sfaUserId;
    }
    
     /**
     * Set showRegionTicketOnly
     *
     * @param interger $showRegionTicketOnly
     *
     * @return Users
     */
    public function setShowRegionTicketOnly($showRegionTicketOnly )
    {
        $this->showRegionTicketOnly = $showRegionTicketOnly;

        return $this;
    }
   
     /**
     * Get showRegionTicketOnly
     *
     * @return interger
     */
    public function getShowRegionTicketOnly()
    {
        return $this->showRegionTicketOnly;
    }
    
     /**
     * Set subSources 
     *
     * @param string $subSources 
     *
     * @return Users
     */
    public function setSubSources ($subSources)
    {
        if(empty($subSources)){
            $subSources = array();
        }
        $this->subSources  = implode(',',$subSources);
        return $this;
    }
   
     /**
     * Get subSources 
     *
     * @return string
     */
    public function getSubSources ()
    {
        return $this->subSources?explode(',',$this->subSources):array();
    }

}

<?php


namespace App\Entity;

use App\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * UserGroups
 *
 * @ORM\Table(name="tbl_user_groups")
 * @ORM\Entity
 */
class UserGroups
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     */
    private $userId;

    /**
     * @var integer
     *
     * @ORM\Column(name="group_id", type="integer", nullable=true)
     */
    private $groupId;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="is_head", type="integer", nullable=true)
     */
    private $isHead;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return UserGroups
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set groupId
     *
     * @param integer $groupId
     *
     * @return UserGroups
     */
    public function setGroupId($groupId)
    {
        $this->groupId = $groupId;

        return $this;
    }

    /**
     * Get groupId
     *
     * @return integer
     */
    public function getGroupId()
    {
        return $this->groupId;
    }
    /**
     * Set isHead
     *
     * @param integer $isHead
     *
     * @return UserGroups
     */
    public function setIsHead($isHead)
    {
        $this->isHead = $isHead;

        return $this;
    }

    /**
     * Get isHead
     *
     * @return integer
     */
    public function getIsHead()
    {
        return $this->isHead;
    }
}

<?php


namespace App\Entity;

use App\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * SupportUserMapping
 *
 * @ORM\Table(name="support_user_mapping")
 * @ORM\Entity
 */
class SupportUserMapping
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="itms_user_id", type="integer", nullable=false)
     */
    private $itmsUserId;

    /**
     * @var integer
     *
     * @ORM\Column(name="dealer_id", type="integer", nullable=true)
     */
    private $dealerId;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="source_creator_id", type="integer", nullable=true)
     */
    private $sourceCreatorId;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set itmsUserId
     *
     * @param integer $itmsUserId
     *
     * @return SupportUserMapping
     */
    public function setItmsUserId($itmsUserId)
    {
        $this->itmsUserId = $itmsUserId;

        return $this;
    }

    /**
     * Get itmsUserId
     *
     * @return integer
     */
    public function getItmsUserId()
    {
        return $this->itmsUserId;
    }

    /**
     * Set dealerId
     *
     * @param integer $dealerId
     *
     * @return SupportUserMapping
     */
    public function setDealerId($dealerId)
    {
        $this->dealerId = $dealerId;

        return $this;
    }

    /**
     * Get dealerId
     *
     * @return integer
     */
    public function getDealerId()
    {
        return $this->dealerId;
    }
    
    /**
     * Set sourceCreatorId
     *
     * @param integer $sourceCreatorId
     *
     * @return SupportUserMapping
     */
    public function setSourceCreatorId($sourceCreatorId)
    {
        $this->sourceCreatorId = $sourceCreatorId;

        return $this;
    }

    /**
     * Get sourceCreatorId
     *
     * @return integer
     */
    public function getSourceCreatorId()
    {
        return $this->sourceCreatorId;
    }
}

<?php
namespace App\Entity\Logger;
class Logger
{
    const _ACTION_UPDATE = 'update';
    const _ACTION_DELETE = 'preRemove';
    const _ACTION_INSERT_PRIMARY_KEY = 'insert_primary_key';
    private $_logClasses = array(
        'App\Entity\Reconcelation' => array(
            'primary_key_func' => 'getId',
            
            'postUpdate' => true,
            'log_for_columns_only' => array(
                'statement_id',
                'statement_date',
                'statement_doc',
                'insurer_id',
                'payment_cycle_id',
                'agent_code',
                'invoice_id',
                'start_date',
                'end_date',
                'statement_total',
                'status'
            )
        ),
        'App\Entity\InsFinanceDocs' => array(
            'primary_key_func' => 'getId',
            
            'postUpdate' => true,
            'dont_log_for_columns' => array(
                'uploaded_date'
            )
        ),
        'App\Entity\InTransaction' => array(
            'primary_key_func' => 'getId',
            
            'postUpdate' => true,
            'dont_log_for_columns' => array(
                'created','modified'
            )
        ),
        'App\Entity\Invoice' => array(
            'primary_key_func' => 'getId',
            
            'postUpdate' => true,
            'dont_log_for_columns' => array(
                'created','modified'
            )
        ),
        'App\Entity\CustomerAddress' => array(
            'primary_key_func' => 'getId',
            'postPersist' => false,
            'postUpdate' => false
        ),
        'App\Entity\Customer' => array(
            'primary_key_func' => 'getCustomerUuid',
            'postPersist' => false,
            'postUpdate' => true
        ),
        'App\Entity\DealerInfo' => array(
            'primary_key_func' => 'getId',
            'postUpdate' => true,
            'postPersist' => false,
//            'log_for_columns_only' => array(
//                'dealer_id','dealer_org'
//            )
        ),
        'App\Entity\VehicleDetail' => array(
            'primary_key_func' => 'getId',
            'postPersist' => false,
            'postUpdate' => true,
            'dont_log_for_columns' => array(
                'modified'
            )
        ),
        'App\Entity\VehicleDetailPreBooked' => array(
            'primary_key_func' => 'getId',
            'postPersist' => false,
            'postUpdate' => true,
            'dont_log_for_columns' => array(
                'modified'
            )
        ),
        'App\Entity\Ticket' => array(
            'primary_key_func' => 'getId',
            
            'postUpdate' => true,
            'postPersist' => true,
            'log_for_columns_only' => array(
                'status','sub_status','first_name','last_name','agent_id'
            )
        ),
        'App\Entity\TicketInsuranceUcd' => array(
            'primary_key_func' => 'getId',
            
            'postUpdate' => true,
            'postPersist' => false,
            'dont_log_for_columns' => array(
                'modified'
            )
        ),
        'App\Entity\InsOnlineQuoteRequest' => array( 
            'primary_key_func' => 'getId',
            'postPersist' => false,
            'postUpdate' => true,
            'dont_log_for_columns' => array(
                'modified'
            )
        ),
        'App\Entity\InsOfflineQuoteRequest' => array(
            'primary_key_func' => 'getId',
            'postPersist' => false,
            'postUpdate' => true,
            'dont_log_for_columns' => array(
                'modified'
            )
        ),
        'App\Entity\InsOfflineProposalData' => array(
            'primary_key_func' => 'getId',
            
            'postUpdate' => true,
            'dont_log_for_columns' => array(
                'modified'
            )
        ),
        'App\Entity\InsUcdReuploadDocuments' => array(
            'primary_key_func' => 'getId',
            'postPersist' => false,
            'postUpdate' => true,
            'dont_log_for_columns' => array(
                'modified'
            )
        ),
        'App\Entity\UserGroups' => array(
            'primary_key_func' => 'getId',
            
            'postUpdate' => true,
            'postPersist' => true
        ),
        'App\Entity\InsurancePaymentDetails' => array(
            'primary_key_func' => 'getId',
            
            'postUpdate' => true
        ),
        'App\Entity\Users' => array(
            'primary_key_func' => 'getId',
            'postPersist' => false,
            'postUpdate' => true,
            'dont_log_for_columns' => array(
                'modified_at'
            )
        ),
        'App\Entity\UserAttendence' => array(
            'primary_key_func' => 'getId',
            
            'postUpdate' => true,
            'dont_log_for_columns' => array(
                'updatedAt'
            )
        ),
        'App\Entity\UserWeekOff' => array(
            'primary_key_func' => 'getId',
            
            'postUpdate' => true,
            'dont_log_for_columns' => array(
                'markedAt'
            )
        ),
        'App\Entity\PayoutDealers' => array(
            'primary_key_func' => 'getId',
            
            'postUpdate' => true
        ),
        'App\Entity\PayoutDealerSchemes' => array(
            'primary_key_func' => 'getId',
            
            'postUpdate' => true
        ),
        'App\Entity\PayoutDealerTargets' => array(
            'primary_key_func' => 'getId',
            
            'postUpdate' => true
        ),
        'App\Entity\PayoutErrorLog' => array(
            'primary_key_func' => 'getId',
            
            'postUpdate' => true
        ),
        'App\Entity\PayoutEvent' => array(
            'primary_key_func' => 'getId',
            
            'postUpdate' => true
        ),
        'App\Entity\PayoutLob' => array(
            'primary_key_func' => 'getId',
            
            'postUpdate' => true
        ),
        'App\Entity\PayoutRules' => array(
            'primary_key_func' => 'getId',
            
            'postUpdate' => true
        ),
        'App\Entity\PayoutScheme' => array(
            'primary_key_func' => 'getId',
            
            'postUpdate' => true
        ),
        'App\Entity\PayoutSchemeBenefits' => array(
            'primary_key_func' => 'getId',
            
            'postUpdate' => true
        ),
        'App\Entity\PayoutSchemeRules' => array(
            'primary_key_func' => 'getId',
            
            'postUpdate' => true
        ),
        'App\Entity\PayoutSchemeTargets' => array(
            'primary_key_func' => 'getId',
            
            'postUpdate' => true
        ),
        'App\Entity\PayoutTargets' => array(
            'primary_key_func' => 'getId',
            
            'postUpdate' => true
        ),
        'App\Entity\PayoutTransaction' => array(
            'primary_key_func' => 'getId',
            
            'postUpdate' => true
        ),
        'App\Entity\DealerSfaMappingApi' => array(
            'primary_key_func' => 'getId',
            'postUpdate' => true
        ),
        'App\Entity\TicketFlags' => array(
            'primary_key_func' => 'getId',
            
            'postUpdate' => true
        ),
        'App\Entity\RegionCityMap' => array(
            'primary_key_func' => 'getId',
            
            'postPersist' => true,
            'postUpdate' => true
        ),
        'App\Entity\UserRegionsMapping' => array(
            'primary_key_func' => 'getId',
            
            'postPersist' => true,
            'postUpdate' => true
        ),
        'App\Entity\TicketAllocatedTeams' => array(
            'primary_key_func' => 'getId',
            
            'postPersist' => true,
            'postUpdate' => true
        ),
        'App\Entity\TblInsHealthVisits' => array(
            'primary_key_func' => 'getId',
            'postUpdate' => true,
            'postPersist' => false,
            'dont_log_for_columns' => array(
                'updated_at'
            )
        ),
        'App\Entity\TblInsHealthVisitProposer' => array(
            'primary_key_func' => 'getId',
            'postUpdate' => true,
            'postPersist' => false,
            'dont_log_for_columns' => array(
                'updated_at'
            )
        ),
        'App\Entity\TblInsHealthVisitInsured' => array(
            'primary_key_func' => 'getId',
            'postUpdate' => true,
            'postPersist' => false,
            'dont_log_for_columns' => array(
                'updated_at'
            )
        ),
        'App\Entity\TblInsHealthVisitPaymentDetails' => array(
            'primary_key_func' => 'getId',
            'postUpdate' => true,
            'postPersist' => false,
            'dont_log_for_columns' => array(
                'updated_at'
            )
        ),
        'App\Entity\TblInsHealthVisitCommunication' => array(
            'primary_key_func' => 'getId',
            'postUpdate' => true,
            'postPersist' => false,
            'dont_log_for_columns' => array(
                'updated_at'
            )
        ),
        'App\Entity\PolicyInspectionDetails' => array(
            'primary_key_func' => 'getId',
            'postUpdate' => true,
            'postPersist' => true
        ),
        'App\Entity\AclRolePermissions' => array(
            'primary_key_func' => 'getId',
            'postUpdate' => true,
            'postPersist' => true
        ),
        'App\Entity\TblInsLifeTickets' => array(
            'primary_key_func' => 'getId',
            'postUpdate' => true,
            'postPersist' => true
        ),
        'App\Entity\TblInsLifeTicketsPaymentDetails' => array(
            'primary_key_func' => 'getId',
            'postUpdate' => true,
            'postPersist' => true
        ),
        'App\Entity\TblInsLifeTicketsPremiumDetails' => array(
            'primary_key_func' => 'getId',
            'postUpdate' => true,
            'postPersist' => true
        ),
        'App\Entity\TblInsLifeTicketsProposer' => array(
            'primary_key_func' => 'getId',
            'postUpdate' => true,
            'postPersist' => true
        ),
        'App\Entity\TblInsLifeUploadDocuments' => array(
            'primary_key_func' => 'getId',
            'postUpdate' => true,
            'postPersist' => true
        ),
        'App\Entity\StatusTransition' => array(
            'primary_key_func' => 'getId',
            'postUpdate' => true,
            'postPersist' => true
        )
    );
    private $entityManager = '';
    public function __construct($em) {
        $this->entityManager = $em;
    }
    
    public function postUpdate($eventArgs)
    {        
      $this->auditLog($eventArgs, __FUNCTION__);        
    }
    
    public function preRemove($eventArgs)
    {
        $this->auditLog($eventArgs, self::_ACTION_DELETE);
    }
    
    public function postPersist($eventArgs)
    {
        $this->auditLog($eventArgs, self::_ACTION_INSERT_PRIMARY_KEY);
        $this->auditLog($eventArgs, __FUNCTION__);
    }
    
    public function auditLog($eventArgs, $action){ 
        $entity = $eventArgs->getEntity();
        $entityConfig = @$this->_logClasses[get_class($entity)];
        if ($entityConfig
                && isset($entityConfig['primary_key_func'])
                && (!empty($entityConfig[$action]) || $action == self::_ACTION_INSERT_PRIMARY_KEY || $action == self::_ACTION_DELETE)
                && method_exists($entity, $entityConfig['primary_key_func'])
                ) {
            ## Log only primary key
            
            if($action == self::_ACTION_INSERT_PRIMARY_KEY) {
                $primaryKey = $this->entityManager->getClassMetadata(get_class($entity))->getIdentifierFieldNames()[0];
                $changedColumns = array($primaryKey=>array(
                    0=>'',
                    1=>$entity->{$entityConfig['primary_key_func']}()
                ));
            } elseif($action == self::_ACTION_DELETE){
                $changedColumns = $this->entityManager->getUnitOfWork()->getEntityIdentifier($entity);
            } else{
                $changedColumns = $this->entityManager->getUnitOfWork()->getEntityChangeSet($entity);
            }
            
            $tableName = $this->entityManager->getClassMetadata(get_class($entity))->getTableName();
            $now = new \DateTime('now');
            foreach($changedColumns as $entityColumn => $changedColumn){
                
                if(is_object($changedColumn[0]) && is_a($changedColumn[0], 'DateTime')){
                    $changedColumn[0] = $changedColumn[0]->format('Y-m-d H:i:s');
                }
                if(is_object($changedColumn[1]) && is_a($changedColumn[1], 'DateTime')){
                    $changedColumn[1] = $changedColumn[1]->format('Y-m-d H:i:s');
                }
                if(is_object($changedColumn[0])){
                   $changedColumn[0] = serialize($changedColumn[0]);
                }
                if(is_object($changedColumn[1])){
                   $changedColumn[1] = serialize($changedColumn[1]);
                }
                
                if($changedColumn[0] == $changedColumn[1]){
                    continue;
                }
                
                $mySqlColumn = current($this->entityManager->getClassMetadata(get_class($entity))->getColumnNames(array($entityColumn)));
                
                if(!empty($entityConfig['log_for_columns_only']) && !in_array($mySqlColumn, $entityConfig['log_for_columns_only'])){
                    continue;
                }
                
                if(!empty($entityConfig['dont_log_for_columns']) && in_array($mySqlColumn, $entityConfig['dont_log_for_columns'])){
                    continue;
                }
                
                $auditLogTable = new \App\Entity\AuditLogs();
                $apiRequestKey = $_SERVER['api_request_id']?$_SERVER['api_request_id']:$_SERVER['REQUEST_URI'];
                if(empty($apiRequestKey)){
                    $apiRequestKey = 'No Request Id';
                }
                $auditLogTable->setBunchId($apiRequestKey);
                $auditLogTable->setRefId($entity->{$entityConfig['primary_key_func']}());
                $auditLogTable->setUserId($_SERVER['HTTP_X_AUTH_ID']?$_SERVER['HTTP_X_AUTH_ID']:0);
                $auditLogTable->setTableName($tableName);
                $auditLogTable->setColumnName($mySqlColumn);
                $auditLogTable->setOldValue($changedColumn[0]);
                $auditLogTable->setNewValue($changedColumn[1]);
                $auditLogTable->setDateTime($now);
                $auditLogTable->setAction($action);
                $this->entityManager->persist($auditLogTable);
                $this->entityManager->flush();
            }
        }
    }
}

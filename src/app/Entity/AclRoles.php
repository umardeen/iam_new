<?php
namespace App\Entity;

use App\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * AclRoles
 *
 * @ORM\Table(name="tbl_acl_roles")
 * @ORM\Entity
 */
class AclRoles
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", length=65535, nullable=true)
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="product_id", type="integer", nullable=false)
     */
    private $productId;

    /**
     * @var integer
     *
     * @ORM\Column(name="ticket_type_id", type="integer", nullable=false)
     */
    private $ticketTypeId;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="role_type_id", type="integer", nullable=false)
     */
    private $roleTypeId;

    /**
     * @var integer
     *
     * @ORM\Column(name="aggregator_id", type="integer", nullable=true)
     */
    private $aggregatorId;
	
    /**
     * @var integer
     *
     * @ORM\Column(name="blocked_ips", type="string", nullable=true)
     */
    private $blockedIp;

    /**
     * @var integer
     *
     * @ORM\Column(name="whitelist_ips", type="string", nullable=true)
     */
    private $whitelistIp;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt = '0000-00-00 00:00:00';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified_at", type="datetime", nullable=false)
     */
    private $modifiedAt = 'CURRENT_TIMESTAMP';

    /**
     * @var integer
     *
     * @ORM\Column(name="created_by", type="integer", nullable=false)
     */
    private $createdBy = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="status", type="boolean", nullable=false)
     */
    private $status = '1';
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="auto_active_user", type="boolean", nullable=false)
     */
    private $autoActiveUser = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_deleted", type="boolean", nullable=false)
     */
    private $isDeleted = '0';
   
    /**
     * @var boolean
     *
     * @ORM\Column(name="is_read_only", type="boolean", nullable=false)
     */
    private $isReadOnly = '0';
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="sales_role_id", type="string", nullable=true)
     */
    private $salesRoleId = '';
    /**
     * @var string
     *
     * @ORM\Column(name="mapped_subsorce_id", type="string", nullable=true)
     */
    private $mappedSubsorceId = '';
    
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Set name
     *
     * @param string $name
     *
     * @return AclRoles
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set blockedIp
     *
     * @param string $blockedIp
     *
     * @return AclRoles
     */
    public function setBlockedIp($blockedIp)
    {
        $this->blockedIp = $blockedIp;

        return $this;
    }

    /**
     * Get blockedIp
     *
     * @return string
     */
    public function getBlockedIp()
    {
        return $this->blockedIp;
    }

    /**
     * Set whitelistIp
     *
     * @param string $whitelistIp
     *
     * @return AclRoles
     */
    public function setWhitelistIp($whitelistIp)
    {
        $this->whitelistIp = $whitelistIp;

        return $this;
    }

    /**
     * Get whitelistIp
     *
     * @return string
     */
    public function getWhitelistIp()
    {
        return $this->whitelistIp;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return AclRoles
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set productId
     *
     * @param integer $productId
     *
     * @return AclRoles
     */
    public function setProductId($productId)
    {
        $this->productId = $productId;

        return $this;
    }

     /**
     * Get productId
     *
     * @return integer
     */
    public function getProductId()
    {
        return $this->productId;
    }
    
    /**
     * Get ticketTypeId
     *
     * @return string
     */
    public function getTicketTypeId()
    {
        return $this->ticketTypeId;
    }

    /**
     * Set ticketTypeId
     *
     * @param string $ticketTypeId
     *
     * @return AclRoles
     */
    public function setTicketTypeId($ticketTypeId)
    {
        $this->ticketTypeId = $ticketTypeId;

        return $this;
    }
    
    /**
     * Get roleTypeId
     *
     * @return string
     */
    public function getRoleTypeId()
    {
        return $this->roleTypeId;
    }

    /**
     * Set roleTypeId
     *
     * @param string $roleTypeId
     *
     * @return AclRoles
     */
    public function setRoleTypeId($roleTypeId)
    {
        $this->roleTypeId = $roleTypeId;

        return $this;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return AclRoles
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set modifiedAt
     *
     * @param \DateTime $modifiedAt
     *
     * @return AclRoles
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    /**
     * Get modifiedAt
     *
     * @return \DateTime
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * Set createdBy
     *
     * @param integer $createdBy
     *
     * @return AclRoles
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return integer
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set status
     *
     * @param boolean $status
     *
     * @return AclRoles
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean
     */
    public function getStatus()
    {
        return $this->status;
    }
    
    
    /**
     * Set autoActiveUser
     *
     * @param boolean $autoActiveUser
     *
     * @return AclRoles
     */
    public function setAutoActiveUser($autoActiveUser)
    {
        $this->autoActiveUser = $autoActiveUser;

        return $this;
    }

    /**
     * Get autoActiveUser
     *
     * @return boolean
     */
    public function getAutoActiveUser()
    {
        return $this->autoActiveUser;
    }
    
    
    /**
     * Set isDeleted
     *
     * @param boolean $isDeleted
     *
     * @return AclRoles
     */
    
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }
    
    

    /**
     * Get isDeleted
     *
     * @return boolean
     */
    public function getIsDeleted()
    {
        return $this->isDeleted;
    }
    
    /**
     * Set isReadOnly
     *
     * @param boolean $isReadOnly
     *
     * @return AclRoles
     */
    
    public function setIsReadOnly($isReadOnly)
    {
        $this->isReadOnly = $isReadOnly;

        return $this;
    }
    
    

    /**
     * Get isReadOnly
     *
     * @return boolean
     */
    public function getIsReadOnly()
    {
        return $this->isReadOnly;
    }
    
    
     /**
     * Set salesRoleId
     *
     * @param boolean $salesRoleId
     *
     * @return AclRoles
     */
    
    public function setSalesRoleId($salesRoleId)
    {
        $this->salesRoleId = $salesRoleId;

        return $this;
    }
    
    

    /**
     * Get salesRoleId
     *
     * @return boolean
     */
    public function getSalesRoleId()
    {
        return $this->salesRoleId;
    }
    
    
    /**
     * Set mappedSubsorceId
     *
     * @param boolean $mappedSubsorceId
     *
     * @return AclRoles
     */
    
    public function setMappedSubsorceId($mappedSubsorceId)
    {
        $this->mappedSubsorceId = $mappedSubsorceId;

        return $this;
    }
    
    

    /**
     * Get mappedSubsorceId
     *
     * @return boolean
     */
    public function getMappedSubsorceId()
    {
        return $this->mappedSubsorceId;
    }
    
    
    

    /**
     * Set aggregatorId
     *
     * @param integer $aggregatorId
     *
     * @return AclRoles
     */
    public function setAggregatorId($aggregatorId)
    {
        $this->aggregatorId = $aggregatorId;

        return $this;
    }

    /**
     * Get aggregatorId
     *
     * @return integer
     */
    public function getAggregatorId()
    {
        return $this->aggregatorId;
    }

    /**
     * Get array copy of object
     *
     * @return array
     */
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    public function objectToArray($object) {
       if (!is_object($object) && !is_array($object)) {
           return $object;
       }
       if (is_object($object) && !preg_match("/entity/i", get_class($object))) {
           return $object;
       }
       if (is_object($object) && preg_match("/entity/i", get_class($object))) {
           $object = $object->getArrayCopy();
       }
       return array_map(
                       function ($objList) {
                   return $this->objectToArray($objList);
               }, $object
               );
   }

}

<?php


namespace App\Entity;
use App\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * InsSfaUsers
 *
 * @ORM\Table(name="tbl_ins_sfa_users")
 * @ORM\Entity
 */
class InsSfaUsers
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="integer", nullable=true)
     */
    private $userId;

    /**
     * @var integer
     *
     * @ORM\Column(name="sfa_user_id", type="integer", nullable=true)
     */
    private $sfaUserId;

    /**
     * @var integer
     *
     * @ORM\Column(name="dealer_id", type="integer", nullable=true)
     */
    private $dealerId;

    /**
     * @var integer
     *
     * @ORM\Column(name="sale_role_id", type="integer", nullable=true)
     */
    private $saleRoleId;

    /**
     * @var integer
     *
     * @ORM\Column(name="cash_collection_limit", type="integer", nullable=true)
     */
    private $cashCollectionLimit;

    /**
     * @var integer
     *
     * @ORM\Column(name="pending_collectted_cash", type="integer", nullable=true)
     */
    private $pendingCollecttedCash;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="is_active", type="integer", nullable=true)
     */
    private $isActive = 1;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return InsSfaUsers
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set sfaUserId
     *
     * @param integer $sfaUserId
     *
     * @return InsSfaUsers
     */
    public function setSfaUserId($sfaUserId)
    {
        $this->sfaUserId = $sfaUserId;

        return $this;
    }

    /**
     * Get sfaUserId
     *
     * @return integer
     */
    public function getSfaUserId()
    {
        return $this->sfaUserId;
    }

    /**
     * Set dealerId
     *
     * @param integer $dealerId
     *
     * @return InsSfaUsers
     */
    public function setDealerId($dealerId)
    {
        $this->dealerId = $dealerId;

        return $this;
    }

    /**
     * Get dealerId
     *
     * @return integer
     */
    public function getDealerId()
    {
        return $this->dealerId;
    }

    /**
     * Set saleRoleId
     *
     * @param integer $saleRoleId
     *
     * @return InsSfaUsers
     */
    public function setSaleRoleId($saleRoleId)
    {
        $this->saleRoleId = $saleRoleId;

        return $this;
    }

    /**
     * Get saleRoleId
     *
     * @return integer
     */
    public function getSaleRoleId()
    {
        return $this->saleRoleId;
    }

    /**
     * Set cashCollectionLimit
     *
     * @param integer $cashCollectionLimit
     *
     * @return InsSfaUsers
     */
    public function setCashCollectionLimit($cashCollectionLimit)
    {
        $this->cashCollectionLimit = $cashCollectionLimit;

        return $this;
    }

    /**
     * Get cashCollectionLimit
     *
     * @return integer
     */
    public function getCashCollectionLimit()
    {
        return $this->cashCollectionLimit;
    }

    /**
     * Set pendingCollecttedCash
     *
     * @param integer $pendingCollecttedCash
     *
     * @return InsSfaUsers
     */
    public function setPendingCollecttedCash($pendingCollecttedCash)
    {
        $this->pendingCollecttedCash = $pendingCollecttedCash;

        return $this;
    }

    /**
     * Get pendingCollecttedCash
     *
     * @return integer
     */
    public function getPendingCollecttedCash()
    {
        return $this->pendingCollecttedCash;
    }
    
    /**
     * Set isActive
     *
     * @param integer $isActive
     *
     * @return InsSfaUsers
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return integer
     */
    public function getIsActive()
    {
        return $this->isActive;
    }
}

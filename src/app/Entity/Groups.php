<?php


namespace App\Entity;

use App\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Groups
 *
 * @ORM\Table(name="tbl_groups", uniqueConstraints={@ORM\UniqueConstraint(name="name_UNIQUE", columns={"name"})})
 * @ORM\Entity
 */
class Groups
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=45, nullable=false)
     */
    private $name;
    
    /**
     * @var string
     *
     * @ORM\Column(name="category", type="string", length=255, nullable=false)
     */
    private $category;

    /**
     * @var integer
     *
     * @ORM\Column(name="is_read_only", type="integer", nullable=false)
     */
    private $isReadOnly = '0';
    
    /**
     * @var integer
     *
     * @ORM\Column(name="order_by", type="integer", nullable=true)
     */
    private $order = '0';
    
    /**
     * @var integer
     *
     * @ORM\Column(name="show_on_sfa_detail", type="integer", nullable=true)
     */
    private $showOnSfaDetail = '0';
    
    /**
     * @var integer
     *
     * @ORM\Column(name="sales_role_id", type="integer", nullable=true)
     */
    private $salesRoleId;
    
    /**
     * @var string
     *
     * @ORM\Column(name="id_in_system", type="string", length=255, nullable=false)
     */
    private $idInSystem;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Groups
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * Set category
     *
     * @param string $category
     *
     * @return Groups
     */
    public function setCategory($category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return string
     */
    public function getCategory()
    {
        return $this->category;
    }
    
    /**
     * Set isReadOnly
     *
     * @param string $isReadOnly
     *
     * @return Groups
     */
    public function SetIsReadOnly($isReadOnly)
    {
        $this->isReadOnly = $isReadOnly;

        return $this;
    }

    /**
     * Get isReadOnly
     *
     * @return string
     */
    public function getIsReadOnly()
    {
        return $this->isReadOnly;
    }
    /**
     * Set order
     *
     * @param string $order
     *
     * @return Groups
     */
    public function SetOrder($order)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return string
     */
    public function getOrder()
    {
        return $this->order;
    }
    
    /**
     * Set showOnSfaDetail
     *
     * @param string $showOnSfaDetail
     *
     * @return Groups
     */
    public function setShowOnSfaDetail($showOnSfaDetail)
    {
        $this->showOnSfaDetail = $showOnSfaDetail;

        return $this;
    }

    /**
     * Get order
     *
     * @return string
     */
    public function getShowOnSfaDetail()
    {
        return $this->showOnSfaDetail;
    }
    
    /**
     * Set salesRoleId
     *
     * @param integer $salesRoleId
     *
     * @return Id
     */
    
    public function setSalesRoleId($salesRoleId)
    {
        $this->salesRoleId = $salesRoleId;

        return $this;
    }
    
    

    /**
     * Get salesRoleId
     *
     * @return Id
     */
    public function getSalesRoleId()
    {
        return $this->salesRoleId;
    }
    
    /**
     * Set idInSystem
     *
     * @param integer $idInSystem
     *
     * @return Groups
     */
    
    public function setIdInSystem($idInSystem)
    {
        $this->idInSystem = $idInSystem;

        return $this;
    }
    
    

    /**
     * Get idInSystem
     *
     * @return idInSystem
     */
    public function getIdInSystem()
    {
        return $this->idInSystem;
    }
    
}

<?php
namespace App\Entity;
use App\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * GroupCategories
 *
 * @ORM\Table(name="tbl_group_categories", uniqueConstraints={@ORM\UniqueConstraint(name="code", columns={"code"})}, indexes={@ORM\Index(name="is_active", columns={"is_active"})})
 * @ORM\Entity
 */
class GroupCategories
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=255, nullable=false)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="label", type="string", length=1000, nullable=false)
     */
    private $label;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_active", type="boolean", nullable=false)
     */
    private $isActive = '1';
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="is_multi_select", type="boolean", nullable=false)
     */
    private $isMultiSelect = '1';

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_required", type="boolean", nullable=true)
     */
    private $isRequired = '0';
    
    /**
     * @var integer
     *
     * @ORM\Column(name="order_by", type="integer", length=11, nullable=false)
     */
    private $orderBy;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return GroupCategories
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set label
     *
     * @param string $label
     *
     * @return GroupCategories
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * Get label
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     *
     * @return GroupCategories
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }
    
    /**
     * Set isMultiSelect
     *
     * @param boolean $isMultiSelect
     *
     * @return GroupCategories
     */
    public function setIsMultiSelect($isMultiSelect)
    {
        $this->isMultiSelect = $isMultiSelect;

        return $this;
    }

    /**
     * Get isMultiSelect
     *
     * @return boolean
     */
    public function getIsMultiSelect()
    {
        return $this->isMultiSelect;
    }

    /**
     * Set isRequired
     *
     * @param boolean $isRequired
     *
     * @return GroupCategories
     */
    public function setIsRequired($isRequired)
    {
        $this->isRequired = $isRequired;

        return $this;
    }

    /**
     * Get isRequired
     *
     * @return boolean
     */
    public function getIsRequired()
    {
        return $this->isRequired;
    }
    
    /**
     * Set orderBy
     *
     * @param integer $orderBy
     *
     * @return GroupCategories
     */
    public function setOrderBy($orderBy)
    {
        $this->orderBy = $orderBy;

        return $this;
    }

    /**
     * Get orderBy
     *
     * @return integer
     */
    public function getOrderBy()
    {
        return $this->orderBy;
    }
}

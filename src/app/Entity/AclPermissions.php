<?php


namespace App\Entity;

use App\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * AclPermissions
 *
 * @ORM\Table(name="tbl_acl_permissions", uniqueConstraints={@ORM\UniqueConstraint(name="code_UNIQUE", columns={"code"})})
 * @ORM\Entity
 */
class AclPermissions
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="category_id", type="integer", nullable=false)
     */
    private $categoryId;

    /**
     * @var string
     *
     * @ORM\Column(name="label", type="string", length=45, nullable=false)
     */
    private $label;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=45, nullable=false)
     */
    private $code;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="allow_groups", type="integer", nullable=false)
     */
    private $allowGroups;
    
    /**
     * @var string
     *
     * @ORM\Column(name="allowed_group_category", type="string", length=255, nullable=false)
     */
    private $allowedGroupCategory = '';
    
    /**
     * @var integer
     *
     * @ORM\Column(name="rank", type="integer", nullable=false)
     */
    private $rank;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set categoryId
     *
     * @param integer $categoryId
     *
     * @return AclPermissions
     */
    public function setCategoryId($categoryId)
    {
        $this->categoryId = $categoryId;

        return $this;
    }

    /**
     * Get categoryId
     *
     * @return integer
     */
    public function getCategoryId()
    {
        return $this->categoryId;
    }

    /**
     * Set label
     *
     * @param string $label
     *
     * @return AclPermissions
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * Get label
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return AclPermissions
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }
    
    /**
     * Set allowGroups
     *
     * @param integer $allowGroups
     *
     * @return AclPermissions
     */
    public function setAllowGroups($allowGroups)
    {
        $this->allowGroups = $allowGroups;

        return $this;
    }

    /**
     * Get allowGroups
     *
     * @return integer
     */
    public function getAllowGroups()
    {
        return $this->allowGroups;
    }

    /**
     * Set allowedGroupCategory
     *
     * @param string $allowedGroupCategory
     *
     * @return AclPermissions
     */
    public function setAllowedGroupCategory($allowedGroupCategory)
    {
        $this->allowedGroupCategory = $allowedGroupCategory;

        return $this;
    }

    /**
     * Get allowedGroupCategory
     *
     * @return string
     */
    public function getAllowedGroupCategory()
    {
        return $this->allowedGroupCategory;
    }
    
     /**
     * Set rank
     *
     * @param string $rank
     *
     * @return AclPermissions
     */
    public function setRank($rank)
    {
        $this->rank = $rank;

        return $this;
    }

    /**
     * Get rank
     *
     * @return string
     */
    public function getRank()
    {
        return $this->rank;
    }
}

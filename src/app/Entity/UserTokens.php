<?php
namespace App\Entity;

use App\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="tbl_user_tokens", uniqueConstraints={@ORM\UniqueConstraint(name="id", columns={"id"})}))
 */
class UserTokens
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="integer", length=20)
     */
    protected $user_id;
	
	/**
     * @ORM\Column(type="text")
     */
    protected $access_token;
	
    /**
     * @ORM\Column(name="device_token", type="text", nullable = true)
     */
    protected $device_token;
	
	/**
     * @ORM\Column(type="datetime")
     */
    protected $last_login;
	
	/**
     * @ORM\Column(type="datetime")
     */
    protected $created;
	

    /**
     * Get Access Token
     *
     * @ORM\return string
     */
    public function getAccessToken()
    {
        return $this->access_token;
    }
    
    /**
     * Set Access Token
     *
     */
    public function setAccessToken($token)
    {
        $this->access_token = $token;
    }

    /**
     * Get Device Token
     *
     * @ORM\return string
     */
    public function getDeviceToken()
    {
        return $this->device_token;
    }
    
    /**
     * Set Access Token
     *
     */
    public function setDeviceToken($token)
    {
        $this->device_token = $token;
    }
    
    /**
     * Set Access Token
     *
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }
    
    
    public function getErrors()
    {
       return $this->errors;
    }
    
    /**
     * Get array copy of object
     *
     * @return array
     */
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    /**
     * Get user id
     *
     * @ORM\return integer
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Set created
     *
     */
    public function setCreated()
    {
        $this->created = new \DateTime();
    }
    
    /**
     * Set created
     *
     */
    public function setLastLoginTime()
    {
        $this->last_login = new \DateTime();
    }
    
     /**
     * Get last_login
     * @return \DateTime
     */
    public function getLastLoginTime()
    {
       return $this->last_login;
    }
    

}

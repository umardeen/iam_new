<?php

namespace App;

class ExternalDb {

    public static $_instance = null;
    public static $_container = null;
    public static $_connection = array();

    private function __construct($container) {
        self::$_container = $container;
    }

    private static function _getConnection($dbName) {
        if (!isset(self::$_connection[$dbName])) {
            self::$_connection[$dbName] = null;
        }
        if (is_null(self::$_connection[$dbName])) {
            $settings = self::$_container->get('settings');
            $dbConfig = $settings['doctrine']['connection'][$dbName];
            $metaConfig = \Doctrine\ORM\Tools\Setup::createAnnotationMetadataConfiguration(
                            $settings['doctrine']['meta']['entity_path'], $settings['doctrine']['meta']['auto_generate_proxies'], $settings['doctrine']['meta']['proxy_dir'], !empty($settings['doctrine']['meta']['cache']) ? $settings['doctrine']['meta']['cache'] : NULL, false
            );
            self::$_connection[$dbName] = \Doctrine\ORM\EntityManager::create($dbConfig, $metaConfig);
            self::addCustomFunctions(self::$_connection[$dbName]);
            self::addEventListener(self::$_connection[$dbName]);
            if(!empty($dbConfig['sessionVariables'])){
                self::setSessionVariable(self::$_connection[$dbName], $dbConfig['sessionVariables']);
            }
        }
        return self::$_connection[$dbName];
    }

    private static function setSessionVariable($em, $sessionVariables) {
        if (!is_null($em)) {
            foreach ($sessionVariables as $variableName => $variableValue) {
//                $variableName = 'in_predicate_conversion_threshold';
                $sql = "show VARIABLES LIKE '{$variableName}';";
                $sessionVariableStmt = $em->getConnection()->prepare($sql);
                $sessionVariableStmt->execute();
                $result = $sessionVariableStmt->fetchAll();
                if (!empty($result[0]['Value'])) {
                    $sql = "SET SESSION {$variableName} = {$variableValue};";
                    $sessionVariableStmt = $em->getConnection()->prepare($sql);
                    $sessionVariableStmt->execute();
                //    $sessionVariableStmt->fetchAll();
                }
            }
        }
    }

    private static function addCustomFunctions($em) {
        $em->getConfiguration()->addCustomStringFunction('TIMESTAMPDIFF', 'DoctrineExtensions\Query\Mysql\TimestampDiff');
        $em->getConfiguration()->addCustomStringFunction('GROUP_CONCAT', 'DoctrineExtensions\Query\Mysql\GroupConcat');
        $em->getConfiguration()->addCustomStringFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');
        $em->getConfiguration()->addCustomStringFunction('ConvertTz', 'DoctrineExtensions\Query\Mysql\ConvertTz');
        $em->getConfiguration()->addCustomStringFunction('DateFormat', 'DoctrineExtensions\Query\Mysql\DateFormat');
        $em->getConfiguration()->addCustomStringFunction('DATE_FORMAT', 'DoctrineExtensions\Query\Mysql\DateFormat');
        $em->getConfiguration()->addCustomStringFunction('DateDiff', 'DoctrineExtensions\Query\Mysql\DateDiff');
        $em->getConfiguration()->addCustomStringFunction('TimeDiff', 'DoctrineExtensions\Query\Mysql\TimeDiff');
        $em->getConfiguration()->addCustomStringFunction('Hour', 'DoctrineExtensions\Query\Mysql\Hour');
        $em->getConfiguration()->addCustomStringFunction('Minute', 'DoctrineExtensions\Query\Mysql\Minute');
        $em->getConfiguration()->addCustomStringFunction('Second', 'DoctrineExtensions\Query\Mysql\Second');
        $em->getConfiguration()->addCustomStringFunction('Year', 'DoctrineExtensions\Query\Mysql\Year');
        $em->getConfiguration()->addCustomStringFunction('DateAdd', 'DoctrineExtensions\Query\Mysql\DateAdd');
        $em->getConfiguration()->addCustomStringFunction('Now', 'DoctrineExtensions\Query\Mysql\Now');
        $em->getConfiguration()->addCustomStringFunction('FindInSet', 'DoctrineExtensions\Query\Mysql\FindInSet');
        $em->getConfiguration()->addCustomStringFunction('IfElse', 'DoctrineExtensions\Query\Mysql\IfElse');
    }

    private static function addEventListener($em) {
        $doctrineEventManager = $em->getEventManager();
        $doctrineEventManager->addEventListener(
                array(\Doctrine\ORM\Events::postUpdate, \Doctrine\ORM\Events::postPersist), new \App\Entity\Logger\Logger($em)
        );
    }

    public static function _getInstance($container) {
        if (empty(self::$_instance)) {
            $instanse = new self($container);
            self::$_instance = $instanse;
        }
        return self::$_instance;
    }

    public function getDealerEntity() {
        return self::_getConnection('dealerSlave');
    }

    public function getItmsSlaveEntity() {
        return self::_getConnection('slave');
    }

    public function getLmsSlaveEntity() {
        return self::_getConnection('lmsSlave');
    }
    
    public function getTaskSlaveEntity() {
        return self::_getConnection('taskSlave');
    }

}

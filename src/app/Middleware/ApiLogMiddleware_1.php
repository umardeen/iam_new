<?php

namespace App\Middleware;

use \Psr\Http\Message\ServerRequestInterface;
use \Psr\Http\Message\ResponseInterface;
use App\Model\CommonUtilsModel;
use App\Model\Constant as Constant;

class AspiLogMiddleware {

    public $emObject = '';
    public $mongoObject = '';
    public $logId = '';
    public $configPath = '';
    public $_commonUtilsModel = '';

    public function __construct($appObject) {
        $this->emObject = $appObject->get('em');
        $this->mongoObject = $appObject->get('mongo');
        $this->configPath = $appObject->settings['config_path'];
        $this->_commonUtilsModel = new CommonUtilsModel($appObject);
    }

    /**
     * DatabaseLogMiddleware invokable class
     *
     * @param  \Psr\Http\Message\ServerRequestInterface $request  PSR7 request
     * @param  \Psr\Http\Message\ResponseInterface      $response PSR7 response
     * @param  callable                                 $next     Next middleware
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next) {

        $_SESSION['request_ip_address'] = $ipAddress = $request->getAttribute('ip_address');
        $url = $request->getUri()->getpath();
        $headers = $request->getHeaders();
        $request_url = $request->getUri()->getBasePath() . "/" . $request->getUri()->getPath() . "?" . $request->getUri()->getQuery();
        $parsedBody = $request->getParsedBody();
        $queryParams = $request->getQueryParams();
        $method = $request->getMethod();
        $mongoLogCollection = 'apiLogs_' . $method;
        $requestTicketIndexes = array(
            'requestId' => array('request_id', 'requestId', 'req_id'),
            'ticketId' => array('ticket_id', 'ticketId', 't_id')
        );

        $severConfig = $this->_commonUtilsModel->get_config_data('SERVER_SETTINGS', true);
        $allRequestedParams = (array) $queryParams + (array) $parsedBody;
//        if(($request->getAttribute('route')) && !in_array($request->getAttribute('route')->getName(), $severConfig['avoid_response_checking_for'])){
//            $requestTicketId = $this->getRequestTicketId($requestTicketIndexes, $allRequestedParams);
//            $requestId = $requestTicketId['requestId'];
//            $ticketId = $requestTicketId['ticketId'];
//        }
        $route = $request->getAttribute('route');
        $logInfo = array(
            'ip' => $ipAddress,
            'url' => $url,
            'header' => $headers,
            'request_url' => $request_url,
            'method' => $method,
            'queryParams' => $queryParams,
            'request' => $parsedBody
        );
        $requestTime = microtime(true);
        if ($severConfig['ELK_enable']) {
            $logELK = array(
                'app_group' => 'insurance',
                'app_name' => 'itms',
                'revision_id' => '',
                'log_time' => date('Y-m-d H:i:s.') . gettimeofday()['usec'],
                'level' => '0',
                'status_code' => '',
                'http_code' => '',
                'url' => $request_url,
                'module' => 'API Log',
                'sub_module' => $route ? $route->getName() : 'No Sub Module',
                'request_type' => $method,
                'ref_id' => '',
                'request_data_raw' => array(),
                'request_data' => array(),
                'response_data_raw' => array(),
                'response_data' => array(),
                'process_time' => '',
                'response_time' => '',
                'request_time' => date('Y-m-d H:i:s.') . gettimeofday()['usec'],
                'message' => '',
                'label' => 'Api Log'
            );
            $requestData = (array) $parsedBody + (array) $queryParams;
            $logELK['request_data_raw'] = $requestData;
            $i = 1;
            foreach ($logELK['request_data_raw'] as $key => $value) {
                if (is_array($value)) {
                    $value = json_encode($value);
                }
                $logELK['request_data']['f' . $i++] = $key . ':' . $value;
            }
        }
        //log fatal errors
        $errorLogInfo = $logInfo;
        $errorLogInfo['requestData'] = $request;
        register_shutdown_function([$this, "fatal_handler"], $errorLogInfo);

        //Bypass third-party API log from frontend
        if (($request->getAttribute('route')) && !in_array($request->getAttribute('route')->getName(), array('addUpdateThirdPartyAPILog'))) {
            //save the request into the log
            $logInsert = array(
                'ip' => $ipAddress,
                'headers' => $headers,
//                'ticketId' => $ticketId,
//                'requestId' => $requestId,
                'request_url' => $request_url,
                'queryParams' => $queryParams,
                'url' => $url,
                'method' => $method,
                'request' => $parsedBody,
                'created' => new \MongoDate(strtotime(date('Y-m-d H:i:s')))
            );

//saving the log in mongodb 
            $isRouteByPass = $this->isRouteByPass($request);
            if ($this->mongoObject && $isRouteByPass) {
                if (($request->getAttribute('route')) && !in_array($request->getAttribute('route')->getName(), $severConfig['avoid_response_checking_for'])) {
                    $this->replaceZeroLengthKey($logInsert, '_blank_key');
                }
                $this->mongoObject->{$mongoLogCollection}->insert($logInsert);
                $_SERVER['api_request_id'] = $this->logId = $logInsert['_id'];
                $logELK['ref_id'] = (array) $this->logId;
                if ($logELK['ref_id']['$id']) {
                    $logELK['ref_id'] = $logELK['ref_id']['$id'];
                }
            }

            //call the app
            $response = $next($request, $response);
            if ($severConfig['ELK_enable']) {
                $logELK['http_code'] = $response->getStatusCode();
                $logELK['message'] = $response->getReasonPhrase();
            }
            $body = $response->getBody();
            $body->rewind();
            $avoidResponseFor = (array) $severConfig['avoid_ELK_response_for'];
            if ($severConfig['ELK_enable'] && !in_array($logELK['sub_module'], $avoidResponseFor)) {
                $logELK['response_data_raw'] = $body->getContents();
                $i = 1;
                foreach (json_decode($logELK['response_data_raw'], true) as $key => $value) {
                    if (is_array($value)) {
                        $value = json_encode($value);
                    }
                    $logELK['response_data']['f' . $i++] = $key . ':' . $value;
                }
            }

            if ($response->getStatusCode() != 200) {
                if ($severConfig['ELK_enable']) {
                    $logELK['level'] = '5';
                }
                $body = $response->getBody();
                $body->rewind();
                $updateArray = array(
                    'response' => $body->getContents(),
                    'responseAt' => new \MongoDate(strtotime(date('Y-m-d H:i:s'))),
                    'responseMessage' => $response->getStatusCode() . ' :- ' . $response->getReasonPhrase()
                );
//                if(in_array($method,array('POST','PUT')) && ($request->getAttribute('route')) && !in_array($request->getAttribute('route')->getName(), $severConfig['avoid_response_checking_for'])) {
//                    $requestResponse = json_decode($updateArray['response'], true);
//                    $requestTicketId = $this->getRequestTicketId($requestTicketIndexes, $requestResponse);
//                    $requestId = $requestTicketId['requestId'];
//                    $ticketId = $requestTicketId['ticketId'];
//                    if (!empty($requestId)) {
//                        $updateArray['requestId'] = $requestId;
//                    }
//                    if (!empty($ticketId)) {
//                        $updateArray['ticketId'] = $ticketId;
//                    }
//                }
                $responseData = array(
                    '$set' => $updateArray
                );

                if ($this->mongoObject && $isRouteByPass) {
                    $this->mongoObject->{$mongoLogCollection}->update(array('_id' => $this->logId), $responseData);
                }
            } else {
                $body = $response->getBody();
                $body->rewind();
                $updateArray = array(
                    'response' => $body->getContents(),
                    'responseAt' => new \MongoDate(strtotime(date('Y-m-d H:i:s')))
                );
//                if(in_array($method,array('POST','PUT')) && ($request->getAttribute('route')) && !in_array($request->getAttribute('route')->getName(), $severConfig['avoid_response_checking_for'])) {
//                    $requestResponse = json_decode($updateArray['response'], true);
//                    $requestTicketId = $this->getRequestTicketId($requestTicketIndexes, $requestResponse);
//                    $requestId = $requestTicketId['requestId'];
//                    $ticketId = $requestTicketId['ticketId'];
//
//                    if (!empty($requestId)) {
//                        $updateArray['requestId'] = $requestId;
//                    }
//                    if (!empty($ticketId)) {
//                        $updateArray['ticketId'] = $ticketId;
//                    }
//                }
                //update the log response
                $responseData = array(
                    '$set' => $updateArray
                );
                if ($this->mongoObject && $isRouteByPass) {
                    $this->mongoObject->{$mongoLogCollection}->update(array('_id' => $this->logId), $responseData);
                }
            }

            $processTime = round(microtime(true) - $requestTime, 3) * 1000;
            if ($severConfig['ELK_enable']) {
                $logELK['process_time'] = $processTime;
                $logELK['response_time'] = date('Y-m-d H:i:s.') . gettimeofday()['usec'];
                file_put_contents($this->configPath . App\Model\Constant::INS_ELK_FILE_PATH . 'logs.txt', json_encode($logELK) . PHP_EOL, FILE_APPEND);
            }
        } else {
            $response = $next($request, $response);
        }

        return $response;
    }

    public function fatal_handler($logInfo) {


        // Determine if there was an error and that is why we are about to exit.
        $lastError = error_get_last();
        $errors = array(
            E_ERROR, E_PARSE, E_CORE_ERROR,
            E_CORE_WARNING, E_COMPILE_ERROR,
            E_COMPILE_WARNING, E_STRICT
        );
        if (in_array($lastError['type'], $errors)) {
            //log-info array
            $errorLogInsert = array(
                'api_id' => $_SERVER['api_request_id'],
                'error_code' => (string) $lastError['type'],
                'message' => $lastError['message'],
                'file' => $lastError['file'],
                'line' => (string) $lastError['line'],
                'created' => new \MongoDate(strtotime(date('Y-m-d H:i:s')))
            );
            $isRouteByPass = $this->isRouteByPass($logInfo['requestData']);
            if (isset($logInfo['requestData'])) {
                unset($logInfo['requestData']);
            }
            $errorLogInsert = array_merge($errorLogInsert, $logInfo);

            if ($this->mongoObject && $isRouteByPass) {
                $log = $this->mongoObject->internal_error_log->insert($errorLogInsert);
            } else {
                // Send Error Email
                $errMsg = 'Some Error Occured. Please try later @' . $lastError['file'] . ' : Line - ' . (string) $lastError['line'];

                unset($logInfo['requestData']);

                $this->_commonUtilsModel->itmsErrorLog("", "00", $logInfo, $errMsg, "Error in Master API.");
            }
        }
    }

    public function getRequestTicketId($requestTicketIndexes, $data) {
        static $requestTicketId = array(
            'requestId' => 0,
            'ticketId' => 0
        );

        foreach ($requestTicketIndexes as $type => $indexes) {
            foreach ($indexes as $index) {
                foreach ($data as $dataIndex => $dataRowValue) {
                    if (is_array($dataRowValue)) {
                        $requestTicketId = $this->getRequestTicketId($requestTicketIndexes, $dataRowValue);
                    } elseif (strtolower($index) == $dataIndex && empty($requestTicketId[$type])) {
                        $requestTicketId[$type] = $data[$index];
                    }
                }
            }
        }
        return $requestTicketId;
    }

    public function replaceZeroLengthKey(&$data, $replaceStr) {
        foreach ($data as $key => &$dataRow) {
            if (is_array($dataRow)) {
                $this->replaceZeroLengthKey($dataRow, $replaceStr);
            } elseif (empty(strlen($key))) {
                $data[$replaceStr] = $dataRow;
                unset($data[$key]);
            }
        }
    }

    public function isRouteByPass($request) {
        $methodName = $request->getMethod();
        $routeName = $request->getAttribute('route')->getName();

        // $mgLogRtBypass = $this->_commonUtilsModel->get_config_data('MONGO_LOG_ROUTES_BYPASS',true);
        $severConfig = $this->_commonUtilsModel->get_config_data('SERVER_SETTINGS', true);
        $mgLogRtBypass = (!empty($severConfig['mongo_log_routes_bypass'])) ? $severConfig['mongo_log_routes_bypass'] : [];

        $routeNamesByPass = (isset($mgLogRtBypass['routeNames']) && !empty($mgLogRtBypass['routeNames'])) ? $mgLogRtBypass['routeNames'] : [];
        $methodsByPass = (isset($mgLogRtBypass['methods']) && !empty($mgLogRtBypass['methods'])) ? array_map('strtoupper', $mgLogRtBypass['methods']) : [];

        if (in_array($routeName, $routeNamesByPass) || in_array($methodName, $methodsByPass)) {
            return false;
        } else {
            return true;
        }
    }

}

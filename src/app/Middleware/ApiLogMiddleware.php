<?php

namespace App\Middleware;

use DI\Container;
use Exception;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
//use Slim\Psr7\Response;
use Slim\Routing\RouteContext;

class ApiLogMiddleware implements MiddlewareInterface {

    private $_container;
    private $_commonUtilsModel;
    private $_userTokenModel;

    public function __construct(Container $container) {
        $this->_container = $container;
        $this->_commonUtilsModel = new \App\Model\CommonUtilsModel($this->_container);
        $this->_userTokenModel = new \App\Model\UserTokenModel($this->_container);
    }

    /**
     *
     *
     * @param  ServerRequestInterface  $request PSR-7 request
     * @param  RequestHandlerInterface $handler PSR-15 request handler
     *
     * @return ResponseInterface
     *
     * @throws Exception
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface {
        try {


//            $contentType = $request->getHeaderLine('Content-Type');
//            if (strstr($contentType, 'application/json')) {
                $contents = json_decode($request->getBody()->getContents(), true);
                if (json_last_error() === JSON_ERROR_NONE) {
                    $request = $request->withParsedBody($contents);
                }
//            }
            
            $mongoObj = $this->_commonUtilsModel->getMongoConnection();
            $method = $request->getMethod();
            $mongoCollection = 'api_log_' . $method;
            $mongoConfig = $this->_commonUtilsModel->getMongoConfig();
            $collection = $mongoObj->{$mongoConfig['dbname']}->{$mongoCollection};
            $routeContext = RouteContext::fromRequest($request);
//            $route = $routeContext->getRoute();
//            $routeName = $route->getName();
            $headers = $request->getHeaders();
            $request_url = $request->getUri()->getPath();
            $queryParams = $request->getQueryParams();
            $serverParams = $request->getServerParams();
            $parsedBody = $request->getParsedBody();
            $ipAddress = $serverParams['REMOTE_ADDR'];
            $logInsert = array(
                'ip' => $ipAddress,
                'headers' => $headers,
                'request_url' => $request_url,
                'queryParams' => $queryParams,
                'method' => $method,
                'request' => $parsedBody,
                'created' => new \MongoDB\BSON\UTCDateTime()
            );
            $insertedRow = $collection->insertOne($logInsert);
            $logId = $insertedRow->getInsertedId();

            $response = $handler->handle($request);
            $response->getBody()->rewind();
            $updateArray = array(
                'responseAt' => new \MongoDB\BSON\UTCDateTime(),
                'response' => json_decode($response->getBody()->getContents())
            );
            $responseData = array(
                '$set' => $updateArray
            );
            $collection->updateOne(array('_id' => $logId), $responseData);
            return $response;
        } catch (Exception $ex) {
            die($ex->getMessage());
        }
    }

}

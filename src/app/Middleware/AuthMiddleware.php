<?php

// Application middleware
// e.g: $app->add(new \Slim\Csrf\Guard);

namespace App\Middleware;

use DI\Container;
use DI\DependencyException;
use DI\NotFoundException;
use Exception;
use Google\ApiCore\ApiException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Slim\Psr7\Response;
use Slim\Routing\RouteContext;

/**
 * @property array whiteList
 * @property mixed logger
 * @property string bearer
 * @property int bearerStrLen
 */
class AuthMiddleware implements MiddlewareInterface {

    private $_container;
    private $_commonUtilsModel;
    private $_userTokenModel;

    /**
     * init of the constructor
     * @param Container $container the container
     * @throws DependencyException
     * @throws NotFoundException
     * @throws ApiException
     */
    public function __construct(Container $container) {
        $this->_container = $container;
        $this->_commonUtilsModel = new \App\Model\CommonUtilsModel($this->_container);
        $this->_userTokenModel = new \App\Model\UserTokenModel($this->_container);
    }

    /**
     *
     *
     * @param  ServerRequestInterface  $request PSR-7 request
     * @param  RequestHandlerInterface $handler PSR-15 request handler
     *
     * @return ResponseInterface
     *
     * @throws Exception
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface {
//        die("ssssfsdf");
        try {
            $routeContext = RouteContext::fromRequest($request);
            $route = $routeContext->getRoute();
            $routeName = $route->getName();
            $x_auth_id = $_SERVER['HTTP_X_AUTH_ID'];
            $x_auth_token = $_SERVER['HTTP_X_AUTH_TOKEN'];
            $aclByPassPermissions = $this->_commonUtilsModel->get_config_data('AUTH_PERMISSIONS_BYPASS');
            $notToAssignCronUser = $this->_commonUtilsModel->get_config_data('NOT_TO_ASSIGN_CRON_USER');
            if ($routeName && (!in_array($routeName, $aclByPassPermissions) || !empty($x_auth_id))) {
                $isAuthenticated = $this->_userTokenModel->authenticate($x_auth_id, $x_auth_token);
                $authCheck = true;
                if (!$isAuthenticated['status']) {
                    $authCheck = false;
                }

                if (!$authCheck) {
                    $return = array(
                        "status" => "F",
                        "code" => 4006,
                        "msg" => "Invalid Credentials"
                    );
                    $errorResponse = new Response();
                    $errorResponse->getBody()->write(json_encode($return));
                    return $errorResponse;
                }
            } elseif (($request->getAttribute('route')) && (!in_array($request->getAttribute('route')->getName(), $notToAssignCronUser) && in_array($request->getAttribute('route')->getName(), $aclByPassPermissions) && $authRequired != 1)) {
                $_SERVER['HTTP_X_AUTH_ID'] = App\Model\Constant::CRON_USER_ID;
            }
            $response = $handler->handle($request);
            return $response;
        } catch (Exception $ex) {
            die($ex->getMessage());
        }
    }

}

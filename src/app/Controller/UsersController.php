<?php

namespace App\Controller;

use App\Controller\Root\AbstractController;
use App\Model\UserModel;
use App\Model\UserTokenModel;
use App\Model\CommonUtilsModel;
use App\Constant as Constant;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

final class UsersController extends AbstractController {

    public $userModel;
    public $userTokenModel;
    public $commonUtilsModel;

    public function __construct($c, UserModel $userModel, UserTokenModel $userTokenModel, CommonUtilsModel $commonUtilsModel) {
        parent::__construct($c);
        $this->userModel = $userModel;
        $this->userTokenModel = $userTokenModel;
        $this->commonUtilsModel = $commonUtilsModel;
    }

    /**
     * @api {get} /login Loggedin a user
     * @apiVersion 0.1.0
     * @apiName login
     * @apiGroup User
     *
     * @apiUse AuthHeaders
     *
     * @apiDescription Login a user
     *
     * @apiParam {String}   user_name     		Username.
     * @apiParam {String}   Password       		Password.
     *
     * @apiUse AuthenticationRequired
     * @apiUse InvalidCredentials
     * @apiUse InvalidData
     * @apiUse DatabaseError
     *
     * @apiSuccess {Number}   	user_id                           User id.
     * @apiSuccess {String}   	name                              User  name.
     * @apiSuccess {String}   	user_name                          User username.
     * @apiSuccess {String}   	token                             Generated token.
     *
     * 	@apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     * 	{
     * 	      "user_id": 1,
     *            "first_name": "User 1",
     *            "last_name": "User 1ast name",
     *            "user_name": "user_name",
     *            "token": "$2y$11$mc19PLV8V.xsYTJqNY4eQOC6Rs"
     * 	}
     *
     */
    public function login($request, $response, $args) {
        $result = array(
            'statusCode' => '200',
            'status' => 'F',
            'message' => 'Unable to Login',
            'data' => null
        );
        try {
            $params = $request->getParsedBody();
            $requestFrom = !empty($params['requestFrom']) ? $params['requestFrom'] : "Other";
            $user = $request->getParsedBody();
            if ($this->userModel->validate('login', $user)) {
                $username = $user['user_name'];
                $password = $user['password'];
                $authResult = $this->userModel->authenticateUser($username, $password);
                if (!empty($authResult['status'])) {
                    $userDetails = $authResult['data'];
                    $resultToken = $this->userTokenModel->getExistingActiveToken($userDetails['id']);
                    if ($resultToken == '') {
                        $resultToken = $this->userTokenModel->addToken($userDetails['id']);
                    }
                    if ($resultToken) {
                        $result['statusCode'] = '200';
                        $result['status'] = 'T';
                        $result['data']['user_id'] = $userDetails['id'];
                        $result['data']['token'] = $resultToken;
                        $result['message'] = "User successfully logged in!";
                    } else {
                        $result['statusCode'] = '304';
                        $result['status'] = "F";
                        $result['message'] = "Token Error";
                    }
                } else {
                    $result['statusCode'] = '304';
                    $result['status'] = "F";
                    $result['message'] = $authResult['message'];
                }
            } else {
                $result['errors'] = $this->userModel->getErrors();
                $result['statusCode'] = '304';
                $result['status'] = "F";
                $result['message'] = "Invalid Params";
            }
        }
        catch (Exception $exc) {
            $result['message'] = $exc->getMessage();
            $result['status'] = "F";
            $result['statusCode'] = 500;
        }
        $response->getBody()->write(json_encode($result));
        return $response;
    }

    public function gmailLogin($request, $response, $args) {
        $data = array();
        $codes = array();
        $msg = '';
        $status = 200;
        $userToken = $request->getParsedBody();

        if ($userToken['idtoken']) {

            $requestUrl = Constant::INS_GOOGLE_AUTH_URL . "tokeninfo?id_token=" . $userToken['idtoken'];
            $api_response = $this->userModel->getRequest($requestUrl);
            $responseArr = json_decode($api_response, 1);

            if (isset($responseArr['email'])) {
                $username = $responseArr['email'];
                $password = '';
                $gmailLogin = 'GOOGLESIGNIN';
                $result = $this->userModel->authenticateUser($username, $password, $gmailLogin);

                if (!empty($result['status'])) {
                    $userDetails = $result['data'];
                    $resultToken = $this->userTokenModel->getExistingActiveToken($userDetails['id']);
                    if ($resultToken == '') {
                        $resultToken = $this->userTokenModel->addToken($userDetails['id']);
                    }

                    if ($resultToken) {
                        $data['user_id'] = $userDetails['id'];
                        $data['emp_code'] = $userDetails['empCode'];
                        $data['name'] = $userDetails['name'];
                        $data['user_name'] = $userDetails['userName'];
                        $data['roles'] = $userDetails['roles'];
                        $data['token'] = $resultToken;
                        $data['customerUuid'] = $userDetails['customerUuid'];
                        $data['aggregatorId'] = $userDetails['aggregatorId'];
                        $data['userCapabilities'] = $userDetails['userCapabilities'];
//                        $this->setLoggedInUserInfo($userDetails['id']);
//                        if ($this->_hasPermissions('ENDORSEMENT_LIST')) {
//                            $this->SignInUserOnSupport($data);
//                        }
                        $msg = "User successfully logged in!";
                    } else {
                        $status = 500;
                        $codes = array(112);
                    }
                } else {
                    $status = 401;
                    $codes = array($result['message']);
                }
            } else {
                $codes = array(4006);
                $status = 401;
            }
        } else {
            $codes = array(4006);
            $status = 401;
        }
        //window.open(Constant::CRM_API_URL.'insuranceDashboard');
        return $response->withJson($this->userModel->responseFormat($data, $msg, $codes), $status);
    }

    /**
     * @api {get} /logout Loggedout a user
     * @apiVersion 0.1.0
     * @apiName logout
     * @apiGroup User
     *
     * @apiUse AuthHeaders
     *
     * @apiDescription Login a user
     *
     * @apiUse AuthenticationRequired
     * @apiUse InvalidCredentials
     * @apiUse InvalidData
     * @apiUse DatabaseError
     *
     * @apiSuccess {Array[]}	message			Message Response array
     *
     * 	@apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     * 	{
     * 	  "message": [
     * 	    "Loggedout successfully."
     * 	  ]
     * 	}
     */
    public function logout($request, $response, $args) {

        $result = array(
            'statusCode' => '200',
            'status' => 'F',
            'message' => 'Logout Not Done',
            'data' => null
        );
        try {
            $loginId = $request->getHeaders()['HTTP_X_AUTH_ID'][0];
            $token = $request->getHeaders()['HTTP_X_AUTH_TOKEN'][0];

            if ($loginId && $loginId != Constant::CRON_USER_ID) {
                $this->userModel->unsetLoginUser($loginId);
            }
            unset($_SESSION['User']);
            session_destroy();
            if ($loginId != Constant::CRON_USER_ID) {
                $this->userTokenModel->removeToken($loginId, $token);
            }
            $result['status'] = "T";
            $result['statusCode'] = 200;
            $result['message'] = "Loggedout successfully";
        }
        catch (Exception $exc) {
            $result['message'] = $exc->getMessage();
            $result['status'] = "F";
            $result['statusCode'] = 500;
        }
        $response->getBody()->write(json_encode($result));
        return $response;
    }

    /**
     * @api {get} /users List all the users
     * @apiVersion 0.1.0
     * @apiName getAllUsers
     * @apiGroup User
     *
     * @apiUse AuthHeaders
     *
     * @apiDescription Will be used to list all the users.
     *
     * @apiParam {Number}   pageNo        	Page number.
     * @apiParam {Number}   pageSize   	    Number of records to get.
     * @apiParam {String}   [searchTerm]      Search Term.
     * @apiParam {String}   sortField         Sort Field.
     * @apiParam {String}   sortOrder         Sort Order.
     *
     * @apiUse AuthenticationRequired
     * @apiUse InvalidCredentials
     * @apiUse DatabaseError
     * @apiUse InvalidData
     *
     * @apiSuccess {Number}    	count                                   Current Count of records.
     * @apiSuccess {Number}   	totalCount                              Total Count of records.
     * @apiSuccess {Object[]}	users                                   Users Data Array.
     * @apiSuccess {Number}   	users.id                                User id.
     * @apiSuccess {String}   	users.name                              User name.
     * @apiSuccess {String}   	users.username                          User username.
     * @apiSuccess {String}   	users.password                          User password.
     * @apiSuccess {Boolean}   	users.gender                            User gender.
     * @apiSuccess {String}   	users.mobile                            User mobile.
     * @apiSuccess {Boolean}   	users.status                            User Status.
     * @apiSuccess {Object[]}	users.created                           Created Object.
     * @apiSuccess {Date}   	users.created.date                      Created Date.
     * @apiSuccess {Number}   	users.created.timezone_type             Created Timezone Type.
     * @apiSuccess {String}   	users.created.timezone                  Created Timezone.
     * @apiSuccess {Object[]}	users.modified                          Modified Object.
     * @apiSuccess {Number}   	users.modified.date                     Modified date.
     * @apiSuccess {String}   	users.modified.timezone_type Modified Timezone Type.
     * @apiSuccess {String}   	users.modified.timezone 		Modified Timezone.
     * @apiSuccess {Object[]}       users.role 				Role Object.
     * @apiSuccess {Number}   	users.role.id                           User Role id.
     * @apiSuccess {String}   	users.role.name                         User Role name.
     * @apiSuccess {Boolean}   	users.role.status                       Role status.
     * @apiSuccess {Boolean}   	users.role.is_deleted                   Role deleted status.
     * @apiSuccess {Object[]}   	users.role.created                      Role Created Object.
     * @apiSuccess {Object[]}   	users.role.modified                     Role Modified Object.
     *
     * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     * 	{
     * 	  "count": 5,
     * 	  "totalCount": 5,
     * 	  "users": [
     * 	    {
     * 	      "id": 1,
     *            "role":
     *              {"id": 1,
     *               "name": "admin",
     *               "nameDisp": "Admin",
     *               "status": true,
     *               "is_deleted":false,
     *               "created":{
     *                  "date":"2016-02-25 12:28:56",
     *                  "timezone_type":3,
     *                  "timezone":"UTC"
     *               },
     *               "modified":{
     *               "date":"2016-04-05 05:51:45",
     *               "timezone_type":3,
     *               "timezone":"UTC"
     *               },
     *              },
     * 	      "first_name": "User 1",
     *            "last_name": "User 1",
     * 	      "email": "useremail@gmail.com",
     *            "username": "username",
     *            "password": "$2y$11$mc19PLV8V.xsYTJqNY4eQOC6Rs",
     *            "gender": "2",
     * 	      "mobile": 2147483647,
     * 	      "is_deleted": false,
     * 	      "status": true,
     * 	      "created": {
     * 	        "date": "2016-04-13 05:04:24",
     * 	        "timezone_type": 3,
     * 	        "timezone": "UTC"
     * 	      },
     * 	      "modified": {
     * 	        "date": "2016-04-13 05:04:24",
     * 	        "timezone_type": 3,
     * 	        "timezone": "UTC"
     * 	      }
     * 	    }
     * 	  ]
     * 	} to a member function get( $notificationObj = new NotificationModel();
     *
     */
    public function listAllUsers($request, $response, $args) {
        $result = array(
            'statusCode' => '200',
            'status' => 'F',
            'message' => 'Users Not Fetched',
            'data' => null
        );
        try {
            $loginId = $request->getHeaders()['HTTP_X_AUTH_ID'][0];
            $userData = $this->userModel->listAllUsers($loginId, $request->getQueryParams());
            if (!empty($userData)) {
                foreach ($userData as $key => $value) {
                    $result['data'][$key] = $value;
                }
            }

            $result['status'] = "T";
            $result['statusCode'] = 200;
            $result['message'] = "Users Fetched successfully";
        }
        catch (Exception $exc) {
            $result['message'] = $exc->getMessage();
            $result['status'] = "F";
            $result['statusCode'] = 500;
        }
        $response->getBody()->write(json_encode($result));
        return $response;
    }

    /**
     * @api {get} /users/:id View a user
     * @apiVersion 0.1.0
     * @apiName viewUser
     * @apiGroup User
     *
     * @apiUse AuthHeaders
     *
     * @apiDescription View a user
     *
     * @apiParam {Number}   id        		User Id.
     *
     * @apiUse AuthenticationRequired
     * @apiUse InvalidCredentials
     * @apiUse InvalidData
     * @apiUse DatabaseError
     *
     * @apiSuccess {Number}   	id                                User id.
     * @apiSuccess {String}   	name                         User last name.
     * @apiSuccess {String}   	email                             User email.
     * @apiSuccess {String}   	username                          User username.
     * @apiSuccess {String}   	password                          User password.
     * @apiSuccess {Boolean}   	gender                            User gender.
     * @apiSuccess {String}   	mobile                            User mobile.
     * @apiSuccess {Boolean}   	status                            User Status.
     * @apiSuccess {Object[]}	created                           Created Object.
     * @apiSuccess {Date}   	created.date                      Created Date.
     * @apiSuccess {Number}   	created.timezone_type             Created Timezone Type.
     * @apiSuccess {String}   	created.timezone                  Created Timezone.
     * @apiSuccess {Object[]}	modified                          Modified Object.
     * @apiSuccess {Number}   	modified.date                     Modified date.
     * @apiSuccess {String}   	modified.timezone_type 				Modified Timezone Type.
     * @apiSuccess {String}   	modified.timezone 					Modified Timezone.
     *
     * 	@apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     * 	{
     * 	      "id": 1,
     *            "role":
     *              {"id": 1,
     *               "name": "admin",
     *               "nameDisp": "Admin",
     *               "status": true,
     *               "is_deleted":false,
     *               "created":{
     *                  "date":"2016-02-25 12:28:56",
     *                  "timezone_type":3,
     *                  "timezone":"UTC"
     *               },
     *               "modified":{
     *               "date":"2016-04-05 05:51:45",
     *               "timezone_type":3,
     *               "timezone":"UTC"
     *               },
     *              },
     * 	      "first_name": "User 1",
     *            "last_name": "User 1ast name",
     * 	      "email": "useremail@gmail.com",
     *            "username": "username",
     *            "password": "$2y$11$mc19PLV8V.xsYTJqNY4eQOC6Rs",
     *            "gender": "2",
     * 	      "mobile": 2147483647,
     * 	      "is_deleted": false,
     * 	      "status": true,
     * 	      "created": {
     * 	        "date": "2016-04-13 05:04:24",
     * 	        "timezone_type": 3,
     * 	        "timezone": "UTC"
     * 	      },
     * 	      "modified": {
     * 	        "date": "2016-04-13 05:04:24",
     * 	        "timezone_type": 3,
     * 	        "timezone": "UTC"
     * 	      }
     * 	}
     *
     */
    public function getUser($request, $response, $args) {

        $result = array(
            'statusCode' => '200',
            'status' => 'F',
            'message' => 'User Details Not Fetched',
            'data' => null
        );
        try {
            $params = $request->getParsedBody();
            $result['data'] = $this->userModel->getUser($args['id'], $params);
            $result['status'] = "T";
            $result['statusCode'] = 200;
            $result['message'] = "User Details Fetched Successfully";
        }
        catch (Exception $exc) {
            $result['message'] = $exc->getMessage();
            $result['status'] = "F";
            $result['statusCode'] = 500;
        }
        $response->getBody()->write(json_encode($result));
        return $response;
    }

    /**
     * @api {get} /user:user_id/roleProduct View user roles & products
     * @apiVersion 0.1.0
     * @apiName roleProduct
     * @apiGroup User
     *
     * @apiUse AuthHeaders
     *
     * @apiDescription View a user roles
     *
     * @apiParam {Number}   user_id        		User Id.
     *
     * @apiUse AuthenticationRequired
     * @apiUse InvalidCredentials
     * @apiUse InvalidData
     * @apiUse DatabaseError
     *
     * @apiSuccess {Number}   	id                                User id.
     * @apiSuccess {Object[]}	  roles                             Roles Data Array.
     * @apiSuccess {Object[]}   projects                          Product Data Array.
     *
     * 	@apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     * 	{
      "data": {
      "roles": [
      "1",
      "2"
      ],
      "products": [
      "1",
      "2"
      ],
      "user_id": "8"
      },
      "message": "User Roles & Products List"
      }
     *
     */
    public function getUserRoles($request, $response, $args) {


        $data = array();
        $codes = array();
        $msg = '';
        $status = 200;

        try {
            $data = $this->userModel->getUserRoles($args['id']);
            if (!$data) {
                $codes = array(116);
                $status = 404;
            } else {
                $msg = "User Roles & Products List";
            }
        } catch (Exception $exc) {
            $status = 500;
        }

        return $response->withJson($this->userModel->responseFormat($data, $msg, $codes), $status);
    }

    /**
     * @api {post} /users Add User
     * @apiVersion 0.1.0
     * @apiName addUser
     * @apiGroup User
     *
     * @apiUse AuthHeaders
     *
     * @apiDescription Will be used to add a new user.
     *
     * @apiParam {String}   	name              User Name.
     * @apiParam {String}   	email                   User Email.
     * @apiParam {Number}   	mobile                  User Mobile.
     * @apiParam {Boolean}   	gender     		Gender(Male - 1, Female - 2).
     * @apiParam {String}   	username  		Username for login.
     * @apiParam {String}   	passowrd	    	User Password.
     * @apiParam {Number}   	parent_user_id		Parent User ID.
     * @apiParam {Number}   	role_id			Assigned Role ID.
     * @apiParam {Booelan}   	status			User Status.
     * @apiParam {Booelan}   	is_customer             User is customer or not.
     * @apiUse AuthenticationRequired
     * @apiUse InvalidCredentials
     * @apiUse DatabaseError
     * @apiUse InvalidData
     *
     * @apiSuccess {String}   	id/uuid			User Id/Uuid Inserted.
     * @apiSuccess {String}	message		    	Message Response.
     *
     * @apiSuccessExample {json} Success-Response:
     *  HTTP/1.1 200 OK
     * 	{
     *   "data": {
     *     "id": 123
     *   },
     *   "message": "User has been added successfully."
     *   }
     * 
     * HTTP/1.1 200 OK
     * 	{
     * 	  "status": "200",
     * 	  "uuid": 5f4d7fa0-5d54-11e6-8d12-001e67adace1,
     * 	  "message": [
     * 	    "User has been added successfully."
     * 	  ]
     * 	}
     *
     */
    public function addUser($request, $response, $args) {

        $result = array(
            'statusCode' => '200',
            'status' => 'F',
            'message' => 'User Not Created',
            'data' => null
        );

        try {
            // $result['status'] = "T";
            // $result['data'] = $request->getParsedBody();
            $postData = $request->getParsedBody();
            if ($this->userModel->validate('addUser', $postData)) {
                if (empty($this->userModel->checkDuplicate('userName', $postData['user_name'])) && empty($this->userModel->checkDuplicate('mobile', $postData['mobile']))) {
                    $return = $this->userModel->addUser($postData);
                    $result['status'] = "T";
                    $result['statusCode'] = 200;
                    $result['data'] = $return;
                    $result['message'] = 'User has been added successfully.';
                } else {
                    $result['status'] = "F";
                    $result['statusCode'] = 400;
                    $result['message'] = 'Duplicate User';
                    $result['errors'] = $this->userModel->getErrors();
                }
            } else {
                $result['status'] = "F";
                $result['statusCode'] = 400;
                $result['message'] = 'Invalid User';
                $result['errors'] = $this->userModel->getErrors();
            }
        } catch (Exception $exc) {
            $result['message'] = $exc->getMessage();
            $result['status'] = "F";
            $result['statusCode'] = 500;
        }
        $response->getBody()->write(json_encode($result));
        return $response;
    }

    /**
     * @api {put} /users/:id Edit User
     * @apiVersion 0.1.0
     * @apiName editUser
     * @apiGroup User
     *
     * @apiHeader {String} x-auth-id User unique user id.
     * @apiHeader {String} x-auth-token Users unique access-token.
     *
     * @apiDescription Will be used to edit the details of a user.
     *
     * @apiParam {String}   	first_name              User First Name.
     * @apiParam {String}   	last_name               User Last Name.
     * @apiParam {String}   	email                   User Email.
     * @apiParam {Number}   	mobile                  User Mobile.
     * @apiParam {Boolean}   	gender     		Gender(Male - 1, Female - 2).
     * @apiParam {String}   	username  		Username for login.
     * @apiParam {String}   	passowrd	    	User Password.
     * @apiParam {Number}   	role_id	    		User Role Id.
     * @apiParam {Booelan}   	status			User Status.
     * @apiParam {Booelan}   	is_customer             User is customer or not.
     *
     * @apiUse AuthenticationRequired
     * @apiUse InvalidCredentials
     * @apiUse DatabaseError
     * @apiUse InvalidData
     *
     * @apiSuccess {String}   	id/uuid			User Id/Uuid Inserted.
     * @apiSuccess {Object[]}	message		    	Message Response Array.
     *
     * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     * 	{
     * 	  "status": "200",
     * 	  "message": [
     * 	    "User has been updated successfully."
     * 	  ]
     * 	}
     *
     */
    public function updateUser($request, $response, $args) {
        $result = array(
            'statusCode' => '200',
            'status' => 'F',
            'message' => 'User Not Updated',
            'data' => null
        );

        try {
            $postData = $request->getParsedBody();
            if ($this->userModel->validate('editUser', $postData) && $this->_hasPermissions('EDIT_USER')) {

                if (empty($this->userModel->checkDuplicate('userName', $postData['userName'], $args['id'])) && empty($this->userModel->checkDuplicate('mobile', $postData['mobile'], $args['id']))) {
                    $return = $this->userModel->updateUser($postData, $args['id']);
                    $result['status'] = "T";
                    $result['statusCode'] = 200;
                    $result['data'] = $return;
                    $result['message'] = 'User has been updated successfully.';
                } else {
                    $result['status'] = "F";
                    $result['statusCode'] = 400;
                    $result['message'] = 'Duplicate User';
                    $result['errors'] = $this->userModel->getErrors();
                }
            } else {
                $result['status'] = "F";
                $result['statusCode'] = 400;
                $result['message'] = 'Invalid User';
                $result['errors'] = $this->userModel->getErrors();
            }
        } catch (Exception $exc) {
            $result['message'] = $exc->getMessage();
            $result['status'] = "F";
            $result['statusCode'] = 500;
        }
        $response->getBody()->write(json_encode($result));
        return $response;
    }

    /**
     * @api {delete} /users/:id Delete a user
     * @apiVersion 0.1.0
     * @apiName deleteUser
     * @apiGroup User
     *
     * @apiUse AuthHeaders
     *
     * @apiDescription Delete a user
     *
     * @apiParam {Number}   id  User Id.
     *
     * @apiUse AuthenticationRequired
     * @apiUse InvalidCredentials
     * @apiUse InvalidData
     * @apiUse DatabaseError
     *
     * @apiSuccess {Array[]}	message			Message Response array
     *
     * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     * 	{
     * 	  "message": [
     * 	    "User has been deleted successfully"
     * 	  ]
     * 	}
     */
    public function deleteUser($request, $response, $args) {
        $result = array(
            'statusCode' => '200',
            'status' => 'F',
            'message' => 'User Not Deleted',
            'data' => null
        );
        try {
            if ($this->_hasPermissions('DELETE_USER')) {
                $userData = $this->userModel->deleteUser($args['id']);
            }
            $result['data'] = $data;
            $result['status'] = 'T';
            $result['statusCode'] = 200;
            $result['message'] = 'User Deleted Successfully';
        }
        catch (Exception $exc) {
            $result['message'] = $exc->getMessage();
            $result['status'] = "F";
            $result['statusCode'] = 500;
        }
        $response->getBody()->write(json_encode($result));
        return $response;
    }

    public function forgotPassword($request, $response, $args) {
        
    }

    /**
     * @api {get} /users/parent Get parents list
     * @apiVersion 0.1.0
     * @apiName User Parents
     * @apiGroup User
     *
     * @apiUse AuthHeaders
     *
     * @apiUse AuthenticationRequired
     * @apiUse InvalidCredentials
     * @apiUse InvalidData
     * @apiUse DatabaseError
     *
     * @apiSuccess {Number}   	id                                User id.
     * @apiSuccess {Object[]}	  name                             Roles Data Array.
     *
     * 	@apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     * 	{
      "data": [
      {
      "id": 1,
      "name": "Test"
      },
      {
      "id": 18,
      "name": "Deepak Gupta"
      },
      {
      "id": 69,
      "name": "Ram Ji tiwari"
      },
      {
      "id": 70,
      "name": "Ghazala"
      },
      {
      "id": 97,
      "name": "test7"
      }
      ],
      "message": "Parent Users List"
      }
     *
     */
    public function getUserParents($request, $response, $args) {

        $result = array(
            'statusCode' => '200',
            'status' => 'F',
            'message' => 'Parents Not Fetched',
            'data' => null
        );
        try {
            $result['data'] = $this->userModel->getUserParents($request->getParsedBody());
            $result['status'] = 'T';
            $result['statusCode'] = 200;
            $result['message'] = 'Password Updated Successfully';
        }
        catch (Exception $exc) {
            $result['message'] = $exc->getMessage();
            $result['status'] = "F";
            $result['statusCode'] = 500;
        }
        $response->getBody()->write(json_encode($result));
        return $response;
    }
    

    public function changePassword($request, $response, $args) {
        $result = array(
            'statusCode' => '200',
            'status' => 'F',
            'message' => 'Password Not Changed',
            'data' => null
        );
        try {
            $data['id'] = $this->userModel->changePassword($request->getParsedBody());
            $result['data'] = $data;
            $result['status'] = 'T';
            $result['statusCode'] = 200;
            $result['message'] = 'Password Updated Successfully';
        }
        catch (Exception $exc) {
            $result['message'] = $exc->getMessage();
            $result['status'] = "F";
            $result['statusCode'] = 500;
        }
        $response->getBody()->write(json_encode($result));
        return $response;
    }

    /**
     * @name getPermissions
     * @author Umardeen
     * @description returns logged in user's permissions
     */
    public function getPermissions($request, $response, $args) {
        $result = array(
            'statusCode' => '200',
            'status' => 'F',
            'message' => 'No Permissions Found',
            'data' => null
        );
        try {
            $params = $request->getParsedBody();
            $requestFrom = !empty($params['requestSource']) ? $params['requestSource'] : "Other";
            $result['data'] = $this->getUserCapabilities();
            $result['data']['roleInfo']['heirarchy'] = $this->getUserHeirarchy();
            if ($_SERVER['HTTP_X_AUTH_ID'] && $requestFrom == 'support') {
                $mapping = $this->userModel->getSupportUserMapping($_SERVER['HTTP_X_AUTH_ID']);
                $result['data']['roleInfo']['userMapping'] = $mapping;
            }
            $result['status'] = 'T';
            $result['statusCode'] = 200;
            $result['message'] = 'Permission Info Fetched Successfully';
        }
        catch (Exception $exc) {
            $result['message'] = $exc->getMessage();
            $result['status'] = "F";
            $result['statusCode'] = 500;
        }
        $response->getBody()->write(json_encode($result));
        return $response;
    }

    /**
     * @name addUserToGroups
     * @author Umardeen
     * @description returns status
     */
    public function addUserToGroups($request, $response, $args) {
        $result = array(
            'statusCode' => '200',
            'status' => 'F',
            'message' => 'User Not Added to Group',
            'data' => null
        );
        try {
            $params = $request->getParsedBody();
            foreach ($params['users'] as $user) {
                $this->userModel->addUserGroups($user['id'], $params['teams'], $user['is_head'] ? 1 : 0);
            }
            $result['status'] = 'T';
            $result['statusCode'] = 200;
            $result['message'] = 'User Added to Group Successfully';
        }
        catch (Exception $exc) {
            $result['message'] = $exc->getMessage();
            $result['status'] = "F";
            $result['statusCode'] = 500;
        }
        $response->getBody()->write(json_encode($result));
        return $response;
    }

    /**
     * @name updateHeadInTeam
     * @author Umardeen
     * @description returns status
     */
    public function updateHeadInTeam($request, $response, $args) {
        $params = $request->getParams();
        $this->userModel->addUserGroups($params['id'], array($params['team_group_id']), $params['is_head'] ? 1 : 0);
        $returnData = array('message' => 'Added Successfully', 'success' => 1);
        $response->getBody()->write(json_encode($returnData));
        return $response;
    }

//    public function signInUserOnSupport($userDetails) {
//        $isSupportLoginEnabled = $this->commonUtilsModel->get_config_data('IS_SUPPORT_LOGIN_ENABLED');
//        if ($isSupportLoginEnabled) {
//            $permissions = $this->getUserCapabilities();
//            $postData = [];
//            $postData['userId'] = $userDetails['user_id'];
//            $postData['accessToken'] = $userDetails['token'];
//            $postData['permissionData'] = json_encode($permissions);
//            $requestUrl = Constant::SUPPORT_SYSTEM_API_URL . 'oauth2/save/user';
//            $api_response = $this->commonUtilsModel->curlRequest($requestUrl, $postData, '', 'POST');
//        }
//        return true;
//    }

    public function getDealerAgentMapping($request, $response, $args) {
        $params = $request->getParams();
        $returnData = $this->userModel->getDealerAgentMapping($params['user_id']);
        $response->getBody()->write(json_encode($returnData));
        return $response;
    }

    public function alllogout($request, $response, $args) {
        $result = array(
            'statusCode' => '200',
            'status' => 'F',
            'message' => 'All Logout Not Done',
            'data' => null
        );
        try {
            $params = $request->getParsedBody();
            $requestFrom = !empty($params['requestFrom']) ? $params['requestFrom'] : "Other";
            $msg = '';
            $success = true;
            $loginId = $request->getHeaders()['HTTP_X_AUTH_ID'][0];
            $token = $request->getHeaders()['HTTP_X_AUTH_TOKEN'][0];
            $headers = ['x-auth-token:' . $token, 'x-auth-id:' . $loginId];
            if ($loginId && $loginId != Constant::CRON_USER_ID) {
                $this->userModel->unsetLoginUser($loginId);
            }
            unset($_SESSION['User']);
            session_destroy();
            if ($loginId != Constant::CRON_USER_ID) {
                $this->userTokenModel->removeAllToken($loginId);
                if ($requestFrom != 'support') {
                    $this->supportLogout($loginId, $headers);
                }
            }
            $result['status'] = "T";
            $result['statusCode'] = 200;
            $result['message'] = "All Loggedout Successfully";
        }
        catch (Exception $exc) {
            $result['message'] = $exc->getMessage();
            $result['status'] = "F";
            $result['statusCode'] = 500;
        }
        $response->getBody()->write(json_encode($result));
        return $response;
    }

    public function supportLogout($loginId, $headers) {
        $requestUrl = Constant::SUPPORT_SYSTEM_API_URL . "oauth2/logout";
        $response = $this->commonUtilsModel->getRequest($requestUrl, $headers);
        return $response;
    }

    public function migrateUsers($request, $response, $args) {

        $errors = [];
        $msg = '';
        $data = [];
        $status = 200;

        try {
            $postData = $request->getParsedBody();
            $isInternalEmail = $this->userModel->checkUserEmailDomain($postData['email']);
//            if($isInternalEmail){
//                $msg="Only External user is Allowed";
//                $success = false;
//                return $response->withJson(array('success'=>$success,'message'=>$msg,'data' => $data,'errors'=>$errors));
//            }
            if (!empty($postData) && isset($postData['filter_by_dealer']) && isset($postData['filter_id'])) {
                $userRole = !empty($postData['user_role']) ? trim($postData['user_role']) : '';
                if (!$isInternalEmail || !$userRole) {
                    $userRole = Constant::ROLE_NAME_GUEST;
                }
                $source = !empty($postData['user_source']) ? trim($postData['user_source']) : '';
                if ($userRole != '') {
                    $userId = isset($request->getHeaders()['HTTP_X_AUTH_ID'][0]) ? $request->getHeaders()['HTTP_X_AUTH_ID'][0] : '';
                    $accessToken = isset($request->getHeaders()['HTTP_X_AUTH_TOKEN'][0]) ? $request->getHeaders()['HTTP_X_AUTH_TOKEN'][0] : '';
                    $roleId = '';
                    $roleDetail = $this->userModel->getRoleDetailByName($userRole);
                    if (empty($roleDetail)) {
                        $postData = [];
                        $postData['product'] = Constant::INS_PRODUCT_ID;
                        $postData['productId'] = Constant::INS_PRODUCT_ID;
                        $postData['roleTypeId'] = Constant::SUPPORT_MANAGER_ROLE_TYPE_ID;
                        $postData['name'] = $userRole;
                        $postData['ticketTypeId'] = 7;
                        $postData['isRoot'] = 0;
                        $postData['created_by'] = $userId;
                        $header = ['x-auth-token:' . $accessToken, 'x-auth-id:' . $userId];
                        $requestUrl = Constant::CRM_API_URL . 'roles';
                        $api_response = $this->commonUtilsModel->curlRequest($requestUrl, $postData, $header, 'POST');
                        $apiResponse = json_decode($api_response);
                        if (!empty($apiResponse->data->id)) {
                            $roleId = $apiResponse->data->id;
                        }
                    } else {
                        $roleId = $roleDetail->getId();
                    }
                    if ($roleId != '') {
                        $groupId = $this->userModel->getGroupIdsToTag($source);
                        if ($groupId != '') {
                            $password = $this->getRandomPassword(10);
                            $userData = [];
                            $userData['name'] = !empty($postData['name']) ? $postData['name'] : '';
                            $userData['user_name'] = !empty($postData['email']) ? $postData['email'] : '';
                            $userData['password'] = $password;
                            $userData['userType'] = $source;
                            $userData['empCode'] = !empty($postData['user_ref_id']) ? $postData['user_ref_id'] : NULL;
                            $userData['status'] = 1;
                            $userData['gender'] = !empty($postData['gender']) ? $postData['gender'] : '';
                            $userData['email'] = !empty($postData['email']) ? $postData['email'] : '';
                            $userData['mobile'] = !empty($postData['mobile']) ? $postData['mobile'] : '';
                            $userData['parent_user_id'] = !empty($postData['parent_user_id']) ? $postData['parent_user_id'] : '';

                            $header = ['x-auth-token:' . $accessToken, 'x-auth-id:' . $userId];
                            $requestUrl = Constant::CRM_API_URL . 'users';
                            $requestType = 'POST';
                            $userDetail = $this->userModel->getUserByEmailId($userData['email']);
                            if ($userDetail && $userDetail->getId()) {
                                $requestUrl .= '/' . $userDetail->getId();
                                $requestType = 'PUT';
                                $userData['autoUpdate'] = 1;
                                $userData['allowedAllocation'] = $userDetail->getAllowedAllocation();
                                $userData['ranking'] = $userDetail->getRanking();
                                $userData['empCode'] = $userDetail->getEmpCode();
                                $userData['userType'] = $userDetail->getUserType();
                                $userData['showRegionTicketOnly'] = $userDetail->getShowRegionTicketOnly();
                                $userData['subSources'] = $userDetail->getSubSources();
                                $userData['user_name'] = $userDetail->getUserName();
                                if (!$userData['parent_user_id']) {
                                    $userData['parent_user_id'] = $userDetail->getParentUserId();
                                }
                            } else {
                                $userData['role_ids'] = $roleId;
                                $userData['group_ids'] = array($groupId, Constant::EXTERNAL_USER_GROUP_ID);
                            }
                            $api_response = $this->commonUtilsModel->curlRequest($requestUrl, $userData, $header, $requestType);
                            $apiResponse = json_decode($api_response);
                            if (!empty($apiResponse->data->id)) {
                                if ($source == 'POS') {
                                    $mappingId = $this->userModel->addSupportUserMapping($apiResponse->data->id, $postData['filter_by_dealer'], $postData['filter_id']);
                                    if ($mappingId > 0) {
                                        $data['username'] = $postData['email'];
                                        $data['password'] = $password;
                                        $msg = "User Created Successfully with userID " . $apiResponse->data->id;
                                        $success = true;
                                    } else {
                                        $msg = "Something went wrong with Support Mapping";
                                        $success = false;
                                    }
                                } else {
                                    $data['username'] = $postData['email'];
                                    $data['password'] = $password;
                                    $msg = "User Created Successfully with userID " . $apiResponse->data->id;
                                    $success = true;
                                }
                            } else {
                                $msg = "User Not Created.";
                                if (!empty($apiResponse->error)) {
                                    $errors = $apiResponse->error;
                                }
                                $success = false;
                            }
                        } else {
                            $msg = "Something went wrong with groupId";
                            $success = false;
                        }
                    } else {
                        $msg = "Something went wrong with roleId";
                        $success = false;
                    }
                } else {
                    $msg = "User Role Name can not be blank.";
                    $success = false;
                }
            } else {
                $msg = "All Mandatory fields are required.";
                $success = false;
            }
        } catch (Exception $exc) {
            $msg = 'Some Error Occured while creating user. Please try later @' . __FILE__ . ' : Line - ' . __LINE__;
            $success = false;
        }
        $returnData = array('status' => $success ? "T" : "F", 'message' => $msg, 'data' => $data, 'errors' => $errors);
        $response->getBody()->write(json_encode($returnData));
        return $response;
    }

    public function getRandomPassword($length_of_string) {
        $str_result = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        return substr(str_shuffle($str_result), 0, $length_of_string);
    }

    //Dealer Info Logic Removed
    public function saveUserCredential($request, $response, $args) {
        $result = array(
            'statusCode' => '200',
            'status' => 'F',
            'message' => 'Credential Not Saved',
            'data' => null
        );
        try {
            $params = $request->getParsedBody();
            if (!empty($params['token']) && !empty($params['email'])) {
                $token = $params['token'];
                $email = $params['email'];
                $credentialToken = $this->commonUtilsModel->get_config_data('CREDENTIAL_SET_TOKEN', true);
                if ($credentialToken['token'] == $params['token']) {
                    $userDetail = $this->userModel->getUserByEmailId($email);
                    if (empty($userDetail)) {
                        $result['status'] = "F";
                        $result['statusCode'] = 304;
                        $result['message'] = "User not found for email provided";
                    } else {
                        $password = $this->getRandomPassword(10);
                        $data['username'] = $userDetail->getUserName();
                        $data['password'] = $password;
                        $result['status'] = "T";
                        $result['statusCode'] = 200;
                        $result['message'] = "User Already Exist with userID " . $userDetail->getId();
                        $userDetail->setPassword($password);
                        $this->userModel->saveUserUpdatedPassword($userDetail);
                    }
                } else {
                    $result['status'] = "F";
                    $result['statusCode'] = 304;
                    $result['message'] = "Token Mismatch Detected.";
                }
            } else {
                $result['status'] = "F";
                $result['statusCode'] = 304;
                $result['message'] = "Email and Token is Required.";
            }
        }
        catch (Exception $exc) {
            $result['message'] = $exc->getMessage();
            $result['status'] = "F";
            $result['statusCode'] = 500;
        }
        $response->getBody()->write(json_encode($result));
        return $response;
    }

    public function getParentUser(Request $request, Response $response, $args) {
        $result = array(
            'statusCode' => '200',
            'status' => 'F',
            'message' => 'No Parent Found',
            'data' => null
        );
        try {
            $userId = $args['userId'];
            $return = $this->userModel->getParentUser($userId);
            $result = $return;
        }
        catch (Exception $exc) {
            $result['message'] = $exc->getMessage();
            $result['status'] = "F";
            $result['statusCode'] = 500;
        }
        $response->getBody()->write(json_encode($result));
        return $response;
    }

}

<?php

namespace App\Controller\Root;

use App\Model\Constant as Constant;

abstract class AbstractController {

    protected $_loggedInUserRow;
    protected $_container;
    private $_userCapabilities;
    private $_userHeirarchy;
    private $_userRoleTypeId = 0;
    private $_userRoleTicketTypeId = 0;
    private $_ownTeamGroupId = 0;

    public function __construct($container) {
        
        $this->_container = $container;
        if ($_SERVER['HTTP_X_AUTH_ID']) {
            $this->setLoggedInUserInfo($_SERVER['HTTP_X_AUTH_ID']);
        }
        \App\Model\UserModel::$_CONTROLLER_OBJ = $this;
    }

    protected function setLoggedInUserInfo($userId) {
        $userModel = new \App\Model\UserModel($this->_container);
        $this->setLoggedInUserRow($userModel->getUserById($userId));
        if ($this->_loggedInUserRow) {
            $permissionData = $userModel->getPermissionsByUserId($userId);
            $childRoleData = array();
            $userModel->getChildUsersOfUser($userId, $childRoleData);
            $this->setUserHeirarchy($childRoleData);
            $this->setUserCapabilities($permissionData['permissions']);
            $this->setRoleTypeId($permissionData['roleTypeId']);
            $this->setRoleTicketTypeId($permissionData['permissions']['roleInfo']['ticketTypeId']);
            if ($permissionData['permissions']['roleInfo']['ownTeamGroupId']) {
                $this->setOwnTeamGroupId($permissionData['permissions']['roleInfo']['ownTeamGroupId']);
            }
        }
    }

    protected function setLoggedInUserRow($userRow) {
        \App\Model\UserModel::$_LOGGED_IN_USER = $this->_loggedInUserRow = $userRow;
    }

    protected function setUserHeirarchy($heirarchy) {
        $this->_userHeirarchy = \App\Model\UserModel::$_USER_HEIRARCHY = $heirarchy;
    }

    protected function getUserHeirarchy() {
        return $this->_userHeirarchy;
    }

    protected function setUserCapabilities($permissions) {
        $this->_userCapabilities = \App\Model\UserModel::$_USER_CAPABILIES = $permissions;
    }

    protected function getUserCapabilities() {
        return $this->_userCapabilities;
    }

    protected function setRoleTypeId($roleTypeId) {
        $this->_userRoleTypeId = \App\Model\UserModel::$_ROLE_TYPE_ID = $roleTypeId;
    }

    protected function getRoleTypeId() {
        return $this->_userRoleTypeId;
    }

    protected function setRoleTicketTypeId($roleTicketTypeId) {
        $this->_userRoleTicketTypeId = \App\Model\UserModel::$_ROLE_TICKET_TYPE_ID = $roleTicketTypeId;
    }

    protected function getRoleTicketTypeId() {
        return $this->_userRoleTicketTypeId;
    }

    protected function setOwnTeamGroupId($ownTeamGroupId) {
        $this->_ownTeamGroupId = \App\Model\UserModel::$_OWN_TEAM_GROUP_ID = $ownTeamGroupId;
    }

    protected function getOwnTeamGroupId() {
        return $this->_ownTeamGroupId;
    }

    public function _hasPermissions($permissions) {
        $hasPermission = false;
        if ($this->_loggedInUserRow && $this->_loggedInUserRow->getIsSuperAdmin()) {
            return true;
        }
        if (is_string($permissions)) {
            $permissions = array($permissions);
        }
        foreach ($permissions as $permission) {
            if (!empty($this->_userCapabilities[$permission])) {
                $hasPermission = true;
                break;
            }
        }

        return $hasPermission;
    }

    public function isLeapYear($year) {
        $leapYear = ($year % 4) == 0 && ($year % 100) != 0;
        return $leapYear;
    }

}

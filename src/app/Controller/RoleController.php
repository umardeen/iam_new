<?php
namespace App\Controller;

use App\Controller\Root\AbstractController;
use App\Model\RoleModel;

final class RoleController extends AbstractController
{
    private $roleModel;

    public function __construct($c,RoleModel $roleModel)
    {
        parent::__construct($c);
        $this->roleModel = $roleModel;
    }

/**
 * @api {get} /roles List all the roles
 * @apiVersion 0.1.0
 * @apiName getAllRoles
 * @apiGroup Role
 * 
 * @apiUse AuthHeaders
 * 
 * @apiDescription Will be used to list all the roles.
 *
 * @apiParam {Number}   pageNo            Page number.
 * @apiParam {Number}   pageSize   	  Number of records to get.
 * @apiParam {String}   [searchTerm]      Search Term.
 * @apiParam {String}   sortField         Sort Field.
 * @apiParam {String}   sortOrder         Sort Order.
 *
 * @apiUse AuthenticationRequired
 * @apiUse InvalidCredentials
 * @apiUse DatabaseError
 * @apiUse InvalidData
 * 
 * @apiSuccess {Number}    	count                                   Current Count of records.
 * @apiSuccess {Number}   	totalCount                              Total Count of records.
 * @apiSuccess {Object[]}	roles                                   Roles Data Array.
 * @apiSuccess {Number}   	roles.id                                Role id.
 * @apiSuccess {String}   	roles.name                              Role name.
 * @apiSuccess {Boolean}   	roles.is_deleted                        Role Deleted Status.
 * @apiSuccess {Boolean}   	roles.status                            Role Status.
 * @apiSuccess {Object[]}	roles.created                           Created Object.
 * @apiSuccess {Date}   	roles.created.date                      Created Date.
 * @apiSuccess {Number}   	roles.created.timezone_type             Created Timezone Type.
 * @apiSuccess {String}   	roles.created.timezone                  Created Timezone.
 * @apiSuccess {Object[]}	roles.modified                          Modified Object.
 * @apiSuccess {Number}   	roles.modified.date                     Modified date.
 * @apiSuccess {String}   	roles.modified.timezone_type Modified Timezone Type.
 * @apiSuccess {String}   	roles.modified.timezone 		Modified Timezone.
 * 
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *	{
 *	  "count": 5,
 *	  "totalCount": 5,
 *	  "roles": [
 *	    {
 *	      "id": 1,
 *            "name": "Platinum",
 *            "is_deleted": false,
 *	      "status": true,
 *	      "created": {
 *	        "date": "2016-04-13 05:04:24",
 *	        "timezone_type": 3,
 *	        "timezone": "UTC"
 *	      },
 *	      "modified": {
 *	        "date": "2016-04-13 05:04:24",
 *	        "timezone_type": 3,
 *	        "timezone": "UTC"
 *	      }
 *	    }
 *	  ]
 *	}
 * 
 */
    public function listAllRoles($request, $response, $args) {
        $result = array(
            'statusCode' => '200',
            'status' => 'F',
            'message' => 'Groups Not Listed',
            'data' => null
        );
        try {
            $return = $this->roleModel->listAllRoles($request->getParsedBody());
            $result = $return;
        }
        catch (Exception $exc) {
            $result['message'] = $exc->getMessage();
            $result['status'] = "F";
            $result['statusCode'] = 500;
        }
        $response->getBody()->write(json_encode($result));
        return $response;
    }
    
    public function getAllRoles($request, $response, $args) {
        
        $result = array(
            'statusCode' => '200',
            'status' => 'F',
            'message' => 'Groups Not Listed',
            'data' => null
        );
        try {
            $return = $this->roleModel->getAllRoles($request->getParsedBody());
            $result = $return;
        }
        catch (Exception $exc) {
            $result['message'] = $exc->getMessage();
            $result['status'] = "F";
            $result['statusCode'] = 500;
        }
        $response->getBody()->write(json_encode($result));
        return $response;
    }
    
    
/**
 * @api {get} /roles/:id View a role
 * @apiVersion 0.1.0
 * @apiName viewRole
 * @apiGroup Role
 * 
 * @apiUse AuthHeaders
 * 
 * @apiDescription View a role
 *
 * @apiParam {Number}   id        Role Id.
 *
 * @apiUse AuthenticationRequired
 * @apiUse InvalidCredentials
 * @apiUse InvalidData
 * @apiUse DatabaseError
 * 
 * @apiSuccess {Number}   	id                                Role id.
 * @apiSuccess {String}   	name                              Role name.
 * @apiSuccess {Boolean}   	is_deleted                        Role Deleted Status.
 * @apiSuccess {Boolean}   	status                            Role Status.
 * @apiSuccess {Object[]}	created                           Created Object.
 * @apiSuccess {Date}   	created.date                      Created Date.
 * @apiSuccess {Number}   	created.timezone_type             Created Timezone Type.
 * @apiSuccess {String}   	created.timezone                  Created Timezone.
 * @apiSuccess {Object[]}	modified                          Modified Object.
 * @apiSuccess {Number}   	modified.date                     Modified date.
 * @apiSuccess {String}   	modified.timezone_type Modified Timezone Type.
 * @apiSuccess {String}   	modified.timezone 		  Modified Timezone.
 *
 *	@apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
*	    {
               "data": {
                 "id": 1,
                 "name": "Admin",
                 "description": "This admin role is for RSA Service.",
                 "productId": 3,
                 "createdAt": {
                   "date": "2016-06-06 04:09:31.000000",
                   "timezone_type": 3,
                   "timezone": "UTC"
                 },
                 "modifiedAt": {
                   "date": "2016-07-01 04:49:50.000000",
                   "timezone_type": 3,
                   "timezone": "UTC"
                 },
                 "createdBy": 1,
                 "status": true,
                 "isDeleted": ""
               },
               "message": "Role Details"
           }
 * 
 */
    public function getRole($request, $response, $args)
    {
        $result = array(
            'statusCode' => '404',
            'status' => 'F',
            'message' => 'Group Not Found',
            'data' => null
        );
        try {
            $return = $this->roleModel->getRole($args['id']);
            $result = $return;
        }
        catch (Exception $exc) {
            $result['message'] = $exc->getMessage();
            $result['status'] = "F";
            $result['statusCode'] = 500;
        }
        $response->getBody()->write(json_encode($result));
        return $response;
        
    }
	
/**
 * @api {post} /roles Add Role
 * @apiVersion 0.1.0
 * @apiName addole
 * @apiGroup Role
 * 
 * @apiUse AuthHeaders
 * 
 * @apiDescription Will be used to add a new role.
 *
 * @apiParam {String}   	name                    Role Name.
 * @apiParam {number}    product_id          Selected Product IDs .
 * @apiParam {string}    description          Role Discription.
 * @apiUse AuthenticationRequired
 * @apiUse InvalidCredentials
 * @apiUse DatabaseError
 * @apiUse InvalidData
 * 
 * @apiSuccess {Number}   	id			Role Id Inserted.
 * @apiSuccess {String}	        message		    	Message Response.
 * 
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *	{
 *	  "id": 7,
 *	  "message": [
 *	    "Role has been added successfully."
 *	  ]
 *	}
 *
 */      
    public function addRole($request, $response, $args)
    {
        $result = array(
            'statusCode' => '200',
            'status' => 'F',
            'message' => 'Role Not Saved',
            'data' => null
        );
        try {
            $postData = $request->getParsedBody();
            if(!$this->_loggedInUserRow->getIsSuperAdmin() && $this->_hasPermissions('ROLE_CREATE')) {
                $postData['roleTypeId'] = $this->getRoleTypeId();
            }
            if($this->roleModel->validate('addRole', $postData)) {
                if(!empty($postData['name'])) {
                    if(empty($this->roleModel->checkDuplicate($postData['name'],'',$postData['product']))) {
                        $return =  $this->roleModel->addRole($request->getParsedBody());
                        $result = $return;
                    }else {
                        $result['statusCode'] = '400';
                        $result['status'] = "F";
                        $result['message'] = "Duplicate Role";
                        $result['errors'] = $this->roleModel->getErrors();
                    }
                }
                else {
                    $result['statusCode'] = '400';
                    $result['status'] = "F";
                    $result['message'] = "Name not found";
                    $result['errors'] = $this->roleModel->getErrors();
                }
            }
            else {
                $result['statusCode'] = '400';
                $result['status'] = "F";
                $result['message'] = "Invalid Role";
                $result['errors'] = $this->roleModel->getErrors();
            }
        }
        catch (Exception $exc) {
            $result['message'] = $exc->getMessage();
            $result['status'] = "F";
            $result['statusCode'] = 500;
        }
        $response->getBody()->write(json_encode($result));
        return $response;
    }

/**
 * @api {put} /roles Edit Role
 * @apiVersion 0.1.0
 * @apiName editole
 * @apiGroup Role
 * 
 * @apiUse AuthHeaders
 * 
 * @apiDescription Will be used to update a role.
 *
 * @apiParam {String}       name                    Role Name.
 * @apiParam {number}    product_id          Selected Product IDs .
 * @apiParam {string}    description          Role Discription.
 * @apiParam {number}    created_by          Created By.
 * @apiParam {permission_ids[]}    permission_ids          Selected Permission Object.
 * @apiUse AuthenticationRequired
 * @apiUse InvalidCredentials
 * @apiUse DatabaseError
 * @apiUse InvalidData
 * 
 * @apiSuccess {String}	        message		    	Message Response.
 * 
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *	{
 *	  "message": [
 *	    "Role has been updated successfully."
 *	  ]
 *	}
 *
 */ 
    public function updateRole($request, $response, $args)
    {
        $result = array(
            'statusCode' => '200',
            'status' => 'F',
            'message' => 'Role Not Updated',
            'data' => null
        );
        try {
            $postData = $request->getParsedBody();
            if($this->roleModel->validate('editRole', $postData) && $this->_hasPermissions('ROLE_EDIT')){
                if(!empty($postData['id']))
                {
                    if(empty($this->roleModel->checkDuplicate($postData['name'],$postData['id'],$postData['productId'])))
                    {
                        $return =  $this->roleModel->editRole($request->getParsedBody());
                        $result = $return;
                    }else{
                        $result['statusCode'] = '400';
                        $result['status'] = "F";
                        $result['message'] = "Duplicate Role";
                        $result['errors'] = $this->roleModel->getErrors();
                        // $codes = array(119);
                        // $status = 400;
                    }
                }
                else{
                    $result['statusCode'] = '400';
                    $result['status'] = "F";
                    $result['message'] = "Id not found";
                    $result['errors'] = $this->roleModel->getErrors();
                }
            }else{
                $result['statusCode'] = '400';
                $result['status'] = "F";
                $result['message'] = "Invalid Role";
                $result['errors'] = $this->roleModel->getErrors();
            }
        } catch (Exception $exc) {
            $result['message'] = $exc->getMessage();
            $result['status'] = "F";
            $result['statusCode'] = 500;
        }
        $response->getBody()->write(json_encode($result));
        return $response;

        //return $response->withJson($this->roleModel->responseFormat($data ,$msg ,$codes),$status);
    }

    /**
     * @api {delete} /roles/:id Delete a role
     * @apiVersion 0.1.0
     * @apiName deleteRole
     * @apiGroup Role
     *
     * @apiUse AuthHeaders
     *
     * @apiDescription Delete a role
     *
     * @apiParam {Number}   id  Role Id.
     *
     * @apiUse AuthenticationRequired
     * @apiUse InvalidCredentials
     * @apiUse InvalidData
     * @apiUse DatabaseError
     *
     * @apiSuccess {Array[]}    message         Message Response array
     *
     * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *  {
     *    "message": [
     *      "Role has been deleted successfully"
     *    ]
     *  }
     */

    public function deleteRole($request, $response, $args) {
        $result = array(
            'statusCode' => '200',
            'status' => 'F',
            'message' => 'Role Not Updated',
            'data' => null
        );
        try {
            $return = $this->roleModel->deleteRole($args['id']);
            $result = $return;
        } catch (Exception $exc) {
            $result['message'] = $exc->getMessage();
            $result['status'] = "F";
            $result['statusCode'] = 500;
        }
        $response->getBody()->write(json_encode($result));
        return $response;
        // $data = array();
        // $codes = array();
        // $msg = '';
        // $status = 200;
        // try {
        //     $data = $this->roleModel->deleteRole($args['id']);
        //     if (!$data) {
        //         $codes = array(116);
        //         $status = 404;
        //     } else {
        //         $msg =  "Role has been deleted successfully.";
        //     }
        // } catch (Exception $exc) {
        //     $status = 500;
        // }

        // return $response->withJson($this->roleModel->responseFormat($data ,$msg ,$codes),$status);
        
    }

    
    /**
     * @author Umardeen
     * @name getPermissions
     * @description Retrieves the All capabilities
     * @param $request, $response, $args
     * @return details array
     * 
     */
    public function getPermissions($request, $response, $args) {
        $result = array(
            'statusCode' => '200',
            'status' => 'F',
            'message' => 'Unable to fetch role permissions',
            'data' => null
        );
        try {
            $return = $this->roleModel->getPermissionWithCategory($args);
            $result = $return;
        } catch (Exception $exc) {
            $result['message'] = $exc->getMessage();
            $result['status'] = "F";
            $result['statusCode'] = 500;
        }
        $response->getBody()->write(json_encode($result));
        return $response;
    }
    
    /**
     * @author Umardeen
     * @name updatePermissions
     * @description update capabilities
     * @return details array
     * 
     */
    public function updatePermissions($request, $response, $args) {
        $result = array(
            'statusCode' => '200',
            'status' => 'F',
            'message' => 'Unable to update role permissions',
            'data' => null
        );
        try {
            $data = $request->getParsedBody();
            $return = $this->roleModel->updatePermissions($data);
            $result = $return;
        } catch (Exception $exc) {
            $result['message'] = $exc->getMessage();
            $result['status'] = "F";
            $result['statusCode'] = 500;
        }
        $response->getBody()->write(json_encode($result));
        return $response;
    }
    
}

<?php

namespace App\Controller;

use App\Controller\Root\AbstractController;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class AuthController extends AbstractController {

    public function me(Request $request, Response $response, $args) {
        return $response->withJSON(['data' => $args]);
    }

}

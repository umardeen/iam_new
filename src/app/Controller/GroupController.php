<?php
namespace App\Controller;

use App\Model\GroupModel;
use Psr\Log\LoggerInterface;

final class GroupController
{
    private $groupModel;
    
    public function __construct(GroupModel $groupModel)
    {
        $this->groupModel = $groupModel;
    }
    
    /**
     * @author Umardeen
     * @name saveGroup
     * @description Save Groups
     * @return details array
     * 
     */
    public function saveGroup($request, $response, $args){
        $result = array(
            'statusCode' => '200',
            'status' => 'F',
            'message' => 'Group Not Saved',
            'data' => null
        );
        try {
            $params = $request->getParsedBody();
            $return = $this->groupModel->saveGroup($params);
            $result = $return;
        }
        catch (Exception $exc) {
            $result['message'] = $exc->getMessage();
            $result['status'] = "F";
            $result['statusCode'] = 500;
        }
        $response->getBody()->write(json_encode($result));
        return $response;
    }
    
    /**
     * @author Umardeen
     * @name updateGroup
     * @description Update Groups
     * @return details array
     * 
     */
    public function updateGroup($request, $response, $args){
        $result = array(
            'statusCode' => '201',
            'status' => 'F',
            'message' => 'Group Not Updated',
            'data' => null
        );
        try {
            $params = $request->getParsedBody();
            $return = $this->groupModel->updateGroup($params);
            $result = $return;
        }
        catch (Exception $exc) {
            $result['message'] = $exc->getMessage();
            $result['status'] = "F";
            $result['statusCode'] = 500;
        }
        $response->getBody()->write(json_encode($result));
        return $response;
    }
    
    /**
     * @author Umardeen
     * @name updateGroup
     * @description Update Groups
     * @return details array
     * 
     */
    public function deleteGroup($request, $response, $args){
        $result = array(
            'statusCode' => '201',
            'status' => 'F',
            'message' => 'Group Not Deleted',
            'data' => null
        );
        try {
            $params = $args;
            $return = $this->groupModel->deleteGroup($params);
            $result = $return;
        }
        catch (Exception $exc) {
            $result['message'] = $exc->getMessage();
            $result['status'] = "F";
            $result['statusCode'] = 500;
        }
        $response->getBody()->write(json_encode($result));
        return $response;
    }
    /**
     * @author Umardeen
     * @name saveGroup
     * @description Save Groups
     * @return details array
     * 
     */
    public function getAllGroups($request, $response, $args){
        $result = array(
            'statusCode' => '200',
            'status' => 'F',
            'message' => 'Groups Not Listed',
            'data' => null
        );
        try {
            $return = $this->groupModel->getAllGroups($request->getParsedBody()['data']);
            $result = $return;
        }
        catch (Exception $exc) {
            $result['message'] = $exc->getMessage();
            $result['status'] = "F";
            $result['statusCode'] = 500;
        }
        $response->getBody()->write(json_encode($result));
        return $response;
    }
}

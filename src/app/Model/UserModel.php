<?php

namespace App\Model;

use App\Model\Root\AbstractModel;
use App\Entity\Users;
use App\Entity\UserGroups;
use App\Entity\Groups;
use App\Entity\AclRoleUser;
use App\Entity\UserRegionsMapping;
use App\Entity\UserLocations;
use App\Entity\UserProducts;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Cake\Validation\Validator;
use App\Constant as Constant;

/**
 * Class Model
 * @package App
 */
class UserModel extends AbstractModel {

    private $errors = array();

    /**
     * @author Bhanu
     * @since 03/June/2016
     * @description Validate Login inputs
     * @param string $type
     * @return boolean
     */
    public function validate($type, $data = array()) {
        $validator = new Validator();
        switch ($type) {
            case 'login':
                $validator->requirePresence('user_name', 'user_name is required')
                        ->notEmptyString('user_name', 'user_name is required');
                $validator->requirePresence('password', 'password is required')
                        ->notEmptyString('password', 'password is required');
//                $validator = v::key('user_name', v::notEmpty())
//                        ->key('password', v::notEmpty());
                $errors = $validator->validate($data);
                break;
           case 'addUser':
                $validator->requirePresence('user_name', 'user_name is required')
                    ->notEmptyString('user_name', 'user_name is required');
                $validator->requirePresence('name', 'name is required')
                    ->notEmptyString('name', 'name is required');
                $validator->requirePresence('email', 'email is required')
                    ->notEmptyString('email', 'email is required');
                $validator->requirePresence('mobile', 'mobile is required')
                    ->notEmptyString('mobile', 'mobile is required');
                // $validator->add('title', 'custom', [
                //     'rule' => function ($value, $context) use ($extra) {
                //         // Custom logic that returns true/false
                //     },
                //     'message' => 'The title is not valid'
                // ]);
            //    $validator = v::key('user_name', v::notEmpty())
            //            ->key('password', v::notEmpty())
            //            ->key('name', v::notEmpty())
            //            //->key('email', v::regex('/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/'))
            //            ->key('mobile', v::notEmpty()->regex('/^(\+\d{1,3}[- ]?)?\d{10}$/'));
                $errors = $validator->validate($data);
                break;
           case 'editUser':
            $validator->requirePresence('user_name', 'user_name is required')
            ->notEmptyString('user_name', 'user_name is required');
            $validator->requirePresence('name', 'name is required')
                ->notEmptyString('name', 'name is required');
            $validator->requirePresence('email', 'email is required')
                ->notEmptyString('email', 'email is required');
            $validator->requirePresence('mobile', 'mobile is required')
                ->notEmptyString('mobile', 'mobile is required');
               //$validator = v::key('mobile', v::notEmpty()->regex('/^(\+\d{1,3}[- ]?)?\d{10}$/'));
               //key('userName', v::notEmpty()->regex('/^[a-z0-9._]*$/i'))
               //->key('name', v::notEmpty())
               //->key('email', v::regex('/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/'))
               //->key('mobile', v::notEmpty()->regex('/^(\+\d{1,3}[- ]?)?\d{10}$/'));
               $errors = $validator->validate($data);
               break;
            default:
                break;
        }
        $this->errors = $errors;
        return count($errors) ? false : true;
    }

    public function getErrors() {
        return $this->errors;
    }

    /**
     * @author Bhanu
     * @since 03/June/2016
     * @description Authenticate user login
     * @param $user_name
     * @param $password
     * @return boolean
     */
    public function authenticateUser($username, $password, $gmailLoginFlag = '') {
        $user = $this->entityManager->getRepository('App\Entity\Users')->findOneBy(
                array('userName' => $username, 'status' => 1, 'isDeleted' => 0)
        );
        if (empty($user)) {
            $user = $this->entityManager->getRepository('App\Entity\Users')->findOneBy(
                    array('mobile' => $username, 'status' => 1, 'isDeleted' => 0)
            );
        }

        $return_data = array('status' => false, 'message' => 'Invalid Username/Password');
        if ($user) {
            if (!$user->getStatus()) {
                $return_data['message'] = 'User Not Active';
            } else {

                $userData = $user->objectToArray($user);
//                $qb = $this->entityManager->getRepository('App\Entity\AclRoleUser')->createQueryBuilder('UR');
//                $rolesData = $qb->select('UR.roleId as role_id', 'R.name as role_name', 'R.productId as product_id', 'R.ticketTypeId as product_type')
//                        ->innerJoin('App\Entity\AclRoles', 'R', 'WITH', 'R.id = UR.roleId AND R.status = 1 AND UR.userId = :user_id')
//                        ->setParameter('user_id', $userData['id'])
//                        ->orderBy('R.id')
//                        ->getQuery()
//                        ->getResult();
//
//                if (!$rolesData) {
//                    $rolesData = $qb->select('UR.roleId as role_id', 'G.name as role_name', 'G.productId as product_id', 'G.ticketTypeId as product_type')
//                            ->innerJoin('App\Entity\AclRoles', 'G', 'WITH', 'G.id = UR.roleId AND G.id = :id AND UR.userId = :user_id')
//                            ->setParameter('user_id', $userData['id'])
//                            ->setParameter('id', 13)
//                            ->orderBy('G.id')
//                            ->getQuery()
//                            ->getResult();
//                }
//
//                if ($rolesData) {
//                    $userData['roles'] = $rolesData;
//                }
                $isPasswordValidated = true;
                if (empty($gmailLoginFlag) || $gmailLoginFlag !== 'GOOGLESIGNIN') {
                    $isPasswordValidated = CommonUtilsModel::validatePassword($password, $userData['password']);
                }
                if ($isPasswordValidated) {
                    $return_data['status'] = true;
                    $return_data['message'] = 'Successfully Login';
                    $return_data['data'] = $userData;
//                    $restrictionIpCheck = UserTokenModel::checkRestrictionIP($userData['id'], $_SESSION['request_ip_address']);
//                    if ($restrictionIpCheck['status']) {
//                        $return_data['status'] = true;
//                        $return_data['message'] = 'Successfully Login';
//                        $return_data['data'] = $userData;
//                    } else {
//                        $return_data = array('status' => false, 'message' => 'IP is not allowed');
//                    }
                }
//                if (!empty($gmailLoginFlag) && $gmailLoginFlag == 'GOOGLESIGNIN') {
//                    $restrictionIpCheck = UserTokenModel::checkRestrictionIP($userData['id'], $_SESSION['request_ip_address']);
//                    if ($restrictionIpCheck['status']) {
//                        $return_data['status'] = true;
//                        $return_data['message'] = 'Successfull Login';
//                        $return_data['data'] = $userData;
//                    } else {
//                        $return_data = array('status' => false, 'message' => 4090);
//                    }
//                } else {
//                    if (CommonUtilsModel::validatePassword($password, $userData['password'])) {
//                        $restrictionIpCheck = UserTokenModel::checkRestrictionIP($userData['id'], $_SESSION['request_ip_address']);
//                        if ($restrictionIpCheck['status']) {
//                            $return_data['status'] = true;
//                            $return_data['message'] = 'Successfull Login';
//                            $return_data['data'] = $userData;
//                        } else {
//                            $return_data = array('status' => false, 'message' => 4090);
//                        }
//                    }
//                }
                if ($user) {
                    $user->setIsLogin(1);
                    $this->entityManager->persist($user);
                    $this->entityManager->flush();
                }
            }
        }
        return $return_data;
    }

    public function getAllUsersQuery($loginId, $requestParams) {

        if (!empty($requestParams['sortField'])) {
            $sortField = 'u.' . $requestParams['sortField'];
        } else {
            $sortField = 'u.id';
        }
        if (!empty($requestParams['sortOrder'])) {
            $sortOrder = $requestParams['sortOrder'];
        } else {
            $sortOrder = 'ASC';
        }
        $query = $this->entityManager->getRepository('App\Entity\Users')
                ->createQueryBuilder('u')
                ->innerJoin('App\Entity\AclRoleUser', 'UR', 'WITH', 'u.id = UR.userId')
                ->innerJoin('App\Entity\AclRoles', 'R', 'WITH', 'R.id = UR.roleId')
//                ->innerJoin('App\Entity\Product', 'P', 'WITH', 'P.id = 1 AND P.id = R.productId')
                ->addOrderBy($sortField, $sortOrder);
        $query->select('u.subSources', 'R.id as roleId', 'u.empCode', 'u.userType', 'u.accessOwnTeamOnly', 'u.ownTeamGroupId', 'u.createdAt', 'u.customerUuid', 'u.email', 'u.gender'
                , 'u.id', 'u.mobile', 'u.status', "u.name", "CONCAT(u.name,' (',R.name,')') as nameAndRole", 'u.userName', 'R.ticketTypeId', 'R.name as roleName', 'usrGrp.isHead', 'u.isLogin', 'R.productId');
//        $query->where('P.id = 1');
        $query->andWhere('u.isDeleted = 0');
//        if ($requestParams['removeAllowedAllocation']) {
//            $query->andWhere('u.allowedAllocation != 1');
//        }
//        if ($requestParams['allowedAllocation']) {
//            $query->andWhere('u.allowedAllocation = 1');
//        }
        if ($requestParams['userType']) {
            $query->andWhere('u.userType = :userType')
                    ->setParameter('userType', $requestParams['userType']);
        }
//        $requestParams['roleId'] = 1;
        if (!empty($requestParams['roleId'])) {
            $requestParams['roleId'] = (array) $requestParams['roleId'];
            $query->andWhere("R.id in (:roleIds)")
                    ->setParameter('roleIds', $requestParams['roleId']);
        }

//        if (!empty($requestParams['searchTerm'])) {
//            $query->andWhere('(u.name like :keyword OR '
//                            . 'u.email like :keyword OR '
//                            . 'u.userName like :keyword OR '
//                            . 'u.mobile like :keyword OR R.name like :keyword)')
//                    ->setParameter('keyword', '%' . $requestParams['searchTerm'] . '%');
//        }
//        if (!empty($requestParams['searchText'])) {
//            $query->andWhere('(u.name like :keyword OR u.userName like :keyword )')
//                    ->setParameter('keyword', '%' . $requestParams['searchText'] . '%');
//        }
//        if (!empty($requestParams['productType']) && in_array($requestParams['productType'], array(Constant::INS_UCD_PRODUCT_TYPE,Constant::INS_OFF_PRODUCT_TYPE,Constant::INS_GCLOUCD_OFF_PRODUCT_TYPE))) {
//            $query->andWhere('R.id IN ('.Constant::UCD_EXECUTIVE_AND_LEAD_ROLE_IDS.')'); 
//        }
        if (!empty($requestParams['getOnlyActiveUsers'])) {
            $query->andWhere("u.status = 1");
        }
        if (self::$_LOGGED_IN_USER && !self::$_LOGGED_IN_USER->getIsSuperAdmin()) {

            $query->andWhere("u.isSuperAdmin != 1");

            if (self::$_OWN_TEAM_GROUP_ID) {
                $teams = array($_SERVER['HTTP_X_AUTH_ID']);
                $teamUsers = $this->entityManager->getRepository('App\Entity\UserGroups')->createQueryBuilder('userGrp');
                $teamUsers->select('userGrp.userId')
                        ->where('userGrp.groupId = :groupId')
                        ->setParameter('groupId', self::$_OWN_TEAM_GROUP_ID);
                $results = $teamUsers->getQuery()->getResult();
                foreach ($results as $teamUser) {
                    $teams[] = $teamUser['userId'];
                }
                $query->andWhere("u.id IN (:teamUserIds)")
                        ->setParameter('teamUserIds', $teams);
            }
//            $groupIds = array(0);
//            if(!empty(self::$_USER_CAPABILIES[$requestParams['permission']]['groups'])){
//                $groupIds = array_keys(self::$_USER_CAPABILIES[$requestParams['permission']]['groups']);
//            }
            $groupIds = $this->getGroupIdsByPermission($requestParams['permission']);
            $query->join('App\Entity\UserGroups', 'usrGrp', 'WITH', "usrGrp.groupId IN (:groupIds) AND usrGrp.userId = u.id")
                    ->andWhere('usrGrp.groupId IN (:groupIds)')
                    ->setParameter('groupIds', $groupIds);
        } else {
            $query->leftJoin('App\Entity\UserGroups', 'usrGrp', 'WITH',
                    "usrGrp.userId = u.id");
        }

        if (!empty($requestParams['category'])) {
            $query->join('App\Entity\Groups', 'grp', 'WITH',
                            "usrGrp.groupId = grp.id")
                    ->andWhere('grp.category IN (:grpCats)')
                    ->setParameter('grpCats', (array) $requestParams['category']);
            ;
        }


        if (!empty($requestParams['teamGroupId'])) {
            $query->andWhere('usrGrp.groupId = :teamGroupId')
                    ->setParameter('teamGroupId', $requestParams['teamGroupId']);
        }

//        if (self::$_ROLE_TICKET_TYPE_ID == 1) {
//
//            $allowedDealersResuls = $this->entityManager->getRepository('App\Entity\DealerUsers')
//                    ->createQueryBuilder('loggedInDealGrp')
//                    ->select('loggedInDealGrp.dealerId')
//                    ->where("loggedInDealGrp.userId = {$_SERVER['HTTP_X_AUTH_ID']}")
//                    ->getQuery()
//                    ->getResult();
//            $users = $dealers = array();
//            foreach ($allowedDealersResuls as $allowedDealersResult) {
//                $dealers[] = $allowedDealersResult['dealerId'];
//            }
//            if (!empty($dealers)) {
//                $allowedUserResults = $this->entityManager->getRepository('App\Entity\DealerUsers')
//                        ->createQueryBuilder('userDealer')
//                        ->select('userDealer.userId')
//                        ->where("userDealer.dealerId IN (:dealers)")
//                        ->setParameter('dealers', $dealers)
//                        ->getQuery()
//                        ->getResult();
//                foreach ($allowedUserResults as $allowedUserResult) {
//                    $users[] = $allowedUserResult['userId'];
//                }
//            }
//            if (!empty($requestParams['salesTeamsUsers'])) {
//                $users = array_intersect(array_keys($requestParams['salesTeamsUsers']), $users);
//            }
//            if (empty($users)) {
//                $users = array(0);
//            }
//            $query->andWhere('u.id IN (:userIds)')
//                    ->setParameter('userIds', $users);
//        } elseif (!empty($requestParams['salesTeamsUsers'])) {
//            $query->andWhere('u.id IN (:teamUsers)')
//                    ->setParameter('teamUsers', array_keys($requestParams['salesTeamsUsers']));
//        }

        $query->groupBy('u.id');

        return $query;
    }

    /**
     * @author Bhanu
     * @since 03/June/2016
     * @description List all the users according to options
     * @param array|null $options
     * @return array of users
     */
    public function listAllUsers($loginId, $requestParams) {
        if (empty($requestParams['permission'])) {
            $requestParams['permission'] = 'USER_LIST_PAGE';
        }
        $query = $this->getAllUsersQuery($loginId, $requestParams);
        $limit = $requestParams['pageSize'];
        $page = $requestParams['pageNo'];
        $start = ($requestParams['pageNo'] - 1) * $requestParams['pageSize'];
        // $total = count($query->getQuery()->getResult());
        $query->setFirstResult($start)->setMaxResults($limit);
        $paginator = new \Doctrine\ORM\Tools\Pagination\Paginator($query);
        $result = $query->getQuery()->getResult();
//        $users = CommonUtilsModel::paginate($query,$page,$limit);
        $resultSet = array();

        $resultSet = $result;
        return $resultSet;
    }

    /**
     * @author Bhanu
     * @since 03/June/2016
     * @name getUser
     * @description Retrieves the User Details
     * @param id
     * @return user details array
     *
     */
    public function getUser($id, $params = array()) {
        $return = array(
            'statusCode' => 200,
            'status' => 'F',
            'message' => 'No User Found',
            'data' => null
        );
        $qb = $this->entityManager->getRepository('App\Entity\Users')->createQueryBuilder('U');
        $userData = $qb->select('U.bucketSize', 'U.ranking', 'U.userType', 'U.empCode', 'U.allowedAllocation', 'U.showRegionTicketOnly', 'U.subSources', 'U.accessOwnTeamOnly', 'U.ownTeamGroupId', 'U.id', 'U.parentUserId', 'U.userName', 'U.name', 'U.email', 'U.mobile',
                        'U.gender', 'U.customerUuid', 'U.status', 'U.isDeleted', 'R.productId', 'U.aggregatorId')
                ->addSelect("GROUP_CONCAT(DISTINCT UR.roleId  SEPARATOR ', ') AS role_id")
                ->innerJoin('App\Entity\AclRoleUser', 'UR', 'WITH', 'U.id = UR.userId AND UR.userId = :user_id')
                ->innerJoin('App\Entity\AclRoles', 'R', 'WITH', 'R.id = UR.roleId')
                ->setParameter('user_id', $id)
                ->orderBy('U.id')
                ->getQuery()
                ->getResult();
        // die($qb->getQuery()->getSQL());
        if ($userData) {
            $userData[0]['allowedAllocation'] = $userData[0]['allowedAllocation'] ? true : false;
            $userData[0]['showRegionTicketOnly'] = $userData[0]['showRegionTicketOnly'] ? true : false;
            $userData[0]['subSources'] = $userData[0]['subSources'] ? explode(',', $userData[0]['subSources']) : array();
            if ($userData[0]['ownTeamGroupId']) {
                $group = $this->entityManager->getRepository('App\Entity\Groups')->findOneBy(
                        array('id' => $userData[0]['ownTeamGroupId'])
                );
                $userData[0]['teamName'] = $group ? $group->getName() : '';
            }
            if (!empty($params['userRegions'])) {
                $userData[0]['userRegions'] = array();
                $userRegions = $this->getUserRegions($userData[0]['id']);
                foreach ($userRegions as $userRegionRow) {
                    $userData[0]['userRegions'][] = $userRegionRow->getRegionId();
                }
            }

            $return['statusCode'] = '200';
            $return['status'] = 'T';
            $return['message'] = 'User Fetched Successfully';
            $return['data'] = $userData;        
        }
        return $return;
    }

    public function getUserLocations($userId) {
        return $this->entityManager->getRepository('App\Entity\UserLocations')
                        ->createQueryBuilder('usrLcn')
                        ->select('usrLcn.locationId', 'grp.name', 'grp.idInSystem')
                        ->innerJoin('App\Entity\Groups', 'grp', 'WITH', 'grp.id = usrLcn.locationId AND grp.category = :groupCategory')
                        ->where('usrLcn.userId=:userId')
                        ->setParameter('userId', $userId)
                        ->setParameter('groupCategory', 'LMS_LOCATIONS')
                        ->getQuery()
                        ->getResult();
    }

    public function getUserProducts($userId) {
        return $this->entityManager->getRepository('App\Entity\UserProducts')
                        ->createQueryBuilder('usrPrd')
                        ->select('usrPrd.productId', 'grp.name', 'grp.idInSystem')
                        ->innerJoin('App\Entity\Groups', 'grp', 'WITH', 'grp.id = usrPrd.productId AND grp.category = :groupCategory')
                        ->where('usrPrd.userId=:userId')
                        ->setParameter('userId', $userId)
                        ->setParameter('groupCategory', 'LMS_PRODUCT_TYPES')
                        ->getQuery()
                        ->getResult();
    }

    /**
     * @author Bhanu
     * @name checkDuplicate
     * @description Check the posted data exists
     * @param field
     * @param value
     * @param id
     * @return user details array
     */
    public function checkDuplicate($field, $value, $id = '') {
        $qb = $this->entityManager->getRepository('App\Entity\Users')->createQueryBuilder('c');
        if (isset($id) && $id > 0) { //->andWhere('c.status=1')
            $data = $qb->where('c.id !=:id')
                            ->andWhere('c.' . $field . '=:param')
                            ->andWhere('c.isDeleted=0')
                            ->setParameter('id', $id)
                            ->setParameter('param', $value)
                            ->getQuery()->getResult();
        } else {
            $data = $qb->where('c.' . $field . '=:param')
                            ->andWhere('c.isDeleted=0')
                            ->setParameter('param', $value)
                            ->getQuery()->getResult();
        }
        $data = array_map(
                function ($userData) {
                    return $userData->getArrayCopy();
                }, $data
        );
        return $data;
    }

    /**
     * @author Bhanu
     * @since 02/June/2016
     * @name addUser
     * @description Saves the user
     * @param string|null $slug
     * @return null|Id
     */
    public function addUser($data) {

        $this->entityManager->getConnection()->beginTransaction();
        try {
            $userData = new Users();
            if ($data['role_ids'] != Constant::ROLE_ID_GUEST) {
                $isInternalEmail = $this->checkUserEmailDomain($data['email']);
                if (!$isInternalEmail) {
                    $data['role_ids'] = Constant::ROLE_ID_GUEST;
                }
            }
            if (isset($data['parent_user_id'])) {
                $userData->setParentUserId($data['parent_user_id']);
            }
            $userData->setEmpCode($data['empCode']);
            $userData->setUserName($data['user_name']);
            $userData->setPassword($data['password']);
            $userData->setName((isset($data['name'])) ? $data['name'] : '');
            $userData->setStatus((isset($data['status'])) ? $data['status'] : 0);
            $userData->setGender((isset($data['gender'])) ? $data['gender'] : 0);
            $userData->setEmail($data['email']);
            $userData->setMobile($data['mobile']);
            $userData->setCreatedBy($data['created_by']);
            $userData->setModifiedBy($data['created_by']);
            $now = new \DateTime("now");
            $userData->setCreatedAt($now);
            $userData->setModifiedAt($now);
            $userData->setStatus(1);

            $this->entityManager->persist($userData);
            $this->entityManager->flush();
            $userId = $userData->getId();
            if (isset($data['role_ids']) && $data['role_ids']) {
                $this->updateUserRoles($userId, $data['role_ids']);
            } else {
                $this->updateUserRoles($userId, array(13));
            }
            if (isset($data['group_ids']) && $data['group_ids']) {
                $this->updateUserGroups($userId, $data['group_ids']);
            }
            $this->entityManager->getConnection()->commit();
        } catch (Exception $ex) {
            $this->entityManager->getConnection()->rollback();
        }

        $user_data['id'] = $userId;
        return $user_data;
    }

    /**
     * @author Umardeen
     * @since 08/March/2018
     * @description Updates the user Groups
     * @param array $groups
     * @param Number $userId
     */
    public function updateUserGroups($userId, $groups) {
        $deleteQuery = $this->entityManager->createQueryBuilder()
                ->delete('App\Entity\UserGroups', 'usrGrp')
                ->where('usrGrp.userId = :userId')
                ->andWhere('usrGrp.groupId NOT IN (:groupIds)')
                ->setParameter('userId', $userId)
                ->setParameter('groupIds', array_values($groups))
                ->getQuery();
        $deleteQuery->execute();

        foreach ($groups as $groupId) {
            $userGroupRow = $this->entityManager->getRepository('App\Entity\UserGroups')
                    ->findOneBy(array(
                'userId' => $userId,
                'groupId' => $groupId
            ));
            if (!empty($userGroupRow)) {
                continue;
            }
            $userGroupTable = new UserGroups();
            $userGroupTable->setUserId($userId);
            $userGroupTable->setGroupId($groupId);
            $this->entityManager->persist($userGroupTable);
            $this->entityManager->flush();
        }
    }

    /**
     * @author Umardeen
     * @since 08/March/2018
     * @description Adds the user Groups
     * @param array $groups
     * @param Number $userId
     */
    public function addUserGroups($userId, $groups, $isHead = 2) {
        foreach ($groups as $groupId) {
            $userRoleRow = $this->entityManager
                    ->getRepository('App\Entity\UserGroups')
                    ->findOneBy(array(
                'userId' => $userId,
                'groupId' => $groupId
                    )
            );
            if (!$userRoleRow) {
                $isHead = $isHead == 2 ? 0 : $isHead;
                $userGroupTable = new UserGroups();
                $userGroupTable->setUserId($userId);
                $userGroupTable->setGroupId($groupId);
                $userGroupTable->setIsHead($isHead);
                $this->entityManager->persist($userGroupTable);
                $this->entityManager->flush();
            } else {
                if ($isHead != 2) {
                    $userRoleRow->setIsHead($isHead);
                    $this->entityManager->persist($userRoleRow);
                    $this->entityManager->flush();
                }
            }
        }
    }

    /**
     * @author Bhanu
     * @since 03/June/2016
     * @description Updates the user details
     * @param array $data
     * @return Number|id
     */
    public function updateUser($data, $id) {
        // print_r($data);die();
        $this->entityManager->getConnection()->beginTransaction();
        try {
            $userData = $this->entityManager->getRepository('App\Entity\Users')->findOneBy(
                    array('id' => $id)
            );
            if (!$userData) {
                return null;
            }

            if (isset($data['userName']) && !empty($data['userName'])) {
                $userData->setUserName($data['userName']);
            }
            if (isset($data['password']) && !empty($data['password'])) {
                $userData->setPassword($data['password']);
            }
            if (isset($data['name']) && !empty($data['name'])) {
                $userData->setName($data['name']);
            }
            if (!$data['autoUpdate']) {
                $userData->setEmpCode($data['empCode']);
            }

            if (isset($data['email']) && !empty($data['email'])) {
                $userData->setEmail($data['email']);
            }
            $userData->setMobile($data['mobile']);
            $userData->setParentUserId($data['parentUserId']);
            if (!$data['autoUpdate']) {
                $userData->setStatus((isset($data['status'])) ? $data['status'] : 0);
                $userData->setGender((isset($data['gender'])) ? $data['gender'] : 0);
            }
            $userData->setModifiedBy($data['modified_by']);
            $userData->setModifiedAt((new \DateTime("now")));
            if (self::$_CONTROLLER_OBJ && self::$_CONTROLLER_OBJ->_hasPermissions('CHANGE_USER_STATUS')) {
                $userData->setStatus(1);
            }
            $this->entityManager->persist($userData);
            $this->entityManager->flush();

            if ($id != $_SERVER['HTTP_X_AUTH_ID'] && !empty($data['role_ids'])) {
                $this->updateUserRoles($id, $data['role_ids']);
            }
            if (!$data['autoUpdate'] && isset($data['group_ids']) && $data['group_ids']) {
                $this->updateUserGroups($id, $data['group_ids']);
            }
            $this->entityManager->getConnection()->commit();
        } catch (Exception $ex) {
            $this->entityManager->getConnection()->rollback();
        }

        return $user_data['id'] = $id;
    }

    /**
     * @author Bhanu
     * @since 06/June/2016
     * @description Updates the user roles
     * @param array $data
     * @return Number|id
     */
    public function updateUserRoles($userId, $roles = null) {
        try {
//            $roles = $roles;
//            $userId = $userId;

            $user_roles = $this->entityManager
                    ->getRepository('App\Entity\AclRoleUser')
                    ->findBy(array(
                'userId' => $userId
                    )
            );
            foreach ($user_roles as $userRole) {
                $this->entityManager->remove($userRole);
            }
            if ($roles) {
                if (is_array($roles)) {
                    foreach ($roles as $role) {
                        $userRole = new AclRoleUser();
                        $userRole->setRoleId($role);
                        $userRole->setUserId($userId);
                        $userRole->setCreatedAt((new \DateTime("now")));
                        $userRole->setModifiedAt((new \DateTime("now")));
                        $this->entityManager->persist($userRole);
                        $this->entityManager->flush();
                    }
                } else {
                    $userRole = new AclRoleUser();
                    $userRole->setRoleId($roles);
                    $userRole->setUserId($userId);
                    $userRole->setCreatedAt((new \DateTime("now")));
                    $userRole->setModifiedAt((new \DateTime("now")));
                    $this->entityManager->persist($userRole);
                    $this->entityManager->flush();
                }
            }

            return true;
        } catch (Exception $exec) {
            return false;
        }
    }

    /**
     * @author Bhanu
     * @since 03/June/2016
     * @name getUser
     * @description Retrieves the User Details
     * @param id
     * @return user details array
     * 
     */
    public function getUserRoles($userId) {

        $qb = $this->entityManager->getRepository('App\Entity\AclRoleUser')->createQueryBuilder('UR');
        $rolesData = $qb->select('UR.id', 'UR.roleId as role_id', 'UR.userId as user_id', 'R.productId as product_id')
                ->innerJoin('App\Entity\AclRoles', 'R', 'WITH', 'R.id = UR.roleId AND UR.userId = :user_id')
                ->setParameter('user_id', $userId)
                ->orderBy('UR.id')
                ->getQuery()
                ->getResult();

        $userRoles = array();
        if ($rolesData) {
            foreach ($rolesData as $role) {
                $userRoles['roles'][] = $role['role_id'];
                $userRoles['ticketTypes'][] = $role['product_id'];
                $userRoles['user_id'] = $role['user_id'];
            }
        }
        return $userRoles;
    }

    /**
     * @author Bhanu
     * @since 03/June/2016
     * @description Deletes a user
     * @param array $id
     * @return null|boolean
     */
    public function deleteUser($id) {
        if (self::$_LOGGED_IN_USER && !self::$_LOGGED_IN_USER->getIsSuperAdmin()) {
//            $groupIds = array(0);
//            if(!empty(self::$_USER_CAPABILIES['USER_LIST_PAGE']['groups'])){
//                $groupIds = array_keys(self::$_USER_CAPABILIES['USER_LIST_PAGE']['groups']);
//            }
            $groupIds = $this->getGroupIdsByPermission('USER_LIST_PAGE');
            $query = $this->entityManager->getRepository('App\Entity\Users')
                    ->createQueryBuilder('u')
                    ->join('App\Entity\UserGroups', 'usrGrp', 'WITH', "u.id = {$id} AND usrGrp.userId = u.id")
                    ->andWhere('usrGrp.groupId IN (:groupIds)')
                    ->andWhere("u.id = {$id}")
                    ->setParameter('groupIds', $groupIds);
            $userRows = $query->getQuery()->getResult();

            if (empty($userRows)) {
                return null;
            }
        }
        $userData = $this->entityManager->getRepository('App\Entity\Users')->findOneBy(
                array('id' => $id)
        );
        if (!$userData) {
            return null;
        }
        $userData->setStatus(0);
        $userData->setIsDeleted(1);
        $this->entityManager->persist($userData);
        $this->entityManager->flush();
        return true;
    }

    /**
     * @author Bhanu
     * @since 03/June/2016
     * @description User list
     * @param array $params
     * @return array
     */
    public function getUserParents($params) {
        $q = $this->entityManager
                ->getRepository('App\Entity\Users')
                ->createQueryBuilder('u');
        $q->select('u.id', "CONCAT(u.name,' - ',COALESCE(u.email,'')) as name")
                ->where('u.status =:status')
                ->setParameter('status', 1);

        if ($params['childUserId']) {
            $q->andWhere('u.id !=:childUserId')
                    ->setParameter('childUserId', $params['childUserId']);
        }

        $q->andWhere('u.email IS NOT NULL');
        $q->andWhere('u.name IS NOT NULL');

        if ($params['userId']) {
            $q->andWhere('u.id =:userId')
                    ->setParameter('userId', $params['userId']);
        }

        if ($params['search']) {
            $q->andWhere('(u.name like :search OR u.email like :search)')
                    ->setParameter('search', "{$params['search']}%");
        }
        $userData = $q->getQuery()->getResult();

        if ($userData) {
            return $userData;
        }
        return array();
    }

    /**
     * @author Bhanu
     * @since 23/June/2016
     * @description Get user id either id or customer_uuid
     * @param array $id
     * @return $userid
     */
    public function getUserId($id, $customer_id = '') {
        $q = $this->entityManager->getRepository('App\Entity\Users')->createQueryBuilder('u');
        $q->select('u.id');
        if ($customer_id == 1) {
            $q->where('u.customerUuid = :customer_uuid ')->setParameter(':customer_uuid', $id);
        } else {
            $q->where('u.id = :id')->setParameter(':id', $id);
        }
        $userData = $q->getQuery()->getResult();

        if (!$userData) {
            return null;
        }

        return $userData[0]['id'];
    }

    public function getUserByField($fieldValue) {
        $q = $this->entityManager->getRepository('App\Entity\Users')->createQueryBuilder('u');
        $userData = $q->select('u.id', 'u.name', 'u.customerUuid');
        foreach ($fieldValue as $key => $value) {
            $q->andWhere('u.' . $key . '=:' . $key);
        }
        $q->andWhere('u.isDeleted=0');
        foreach ($fieldValue as $key => $value) {
            $q->setParameter($key, $value);
        }
        $result = $q->getQuery()->getResult();
        return $result;
    }

    /**
     * @author Ruchi
     * @since 05/August/2016
     * @description Updates the user password upon forgot password
     * @param array $data
     * @return Number|id
     */
    public function changePassword($data) {
        $mobile = isset($data['mobile']['phone']) ? $data['mobile']['phone'] : $data['mobile'];
        $userData = $this->entityManager->getRepository('App\Entity\Users')->findOneBy(
                array('mobile' => $mobile)
        );
        if (!$userData) {
            return null;
        }
        if (isset($data['new_password']) && !empty($data['new_password'])) {
            $userData->setPassword($data['new_password']);
        }
        $userData->setModifiedAt((new \DateTime("now")));
        $this->entityManager->persist($userData);
        $this->entityManager->flush();
        // $permission = $this->updateUserRoles($id,$data['roleIds']);
        return array('mobile' => $mobile);
    }

    /**
     * @author Umardeen
     * @param $userId
     * @return array of user Permission
     */
    public function getPermissionsByUserId($userId) {
        $qb = $this->returnExDbCon()->getItmsSlaveEntity()->getRepository('App\Entity\AclRoleUser')
                ->createQueryBuilder('roleUser')
                ->select('grp.category as groupCategory', 'grp.order as groupRank', 'role.name as roleName', "grp.name as groupName", "grp.idInSystem", "perm.code", "permGrp.groupId", "role.roleTypeId", "roleUser.roleId", "role.ticketTypeId")
                ->join('App\Entity\AclRoles', 'role', 'WITH', "roleUser.userId = '{$userId}' AND roleUser.roleId = role.id")
                ->leftJoin('App\Entity\AclRolePermissions', 'rolePerm', 'WITH', "rolePerm.roleId = roleUser.roleId")
                ->leftJoin('App\Entity\AclPermissions', 'perm', 'WITH', 'rolePerm.permissionId = perm.id')
                ->leftJoin('App\Entity\PermissionGroups', 'permGrp', 'WITH', 'rolePerm.id = permGrp.permissionId')
                ->leftJoin('App\Entity\Groups', 'grp', 'WITH', 'grp.id = permGrp.groupId')
                ->where("roleUser.userId = '{$userId}'")
        ;
        $results = $qb->getQuery()->getResult();
        $data = array(
            'permissions' => array(),
            'roleTypeId' => 0
        );
        $data['permissions']['roleInfo']['isSuperAdmin'] = self::$_LOGGED_IN_USER->getIsSuperAdmin();
        $data['permissions']['roleInfo']['isLoggedIn'] = self::$_LOGGED_IN_USER->getIsLogin();
        $data['permissions']['roleInfo']['roleTypeId'] = $results[0]['roleTypeId'];
        $data['permissions']['roleInfo']['roleId'] = $results[0]['roleId'];
        $data['permissions']['roleInfo']['ticketTypeId'] = $results[0]['ticketTypeId'];
        $data['permissions']['roleInfo']['roleName'] = $results[0]['roleName'];
        $groups = $this->getGroupsByUserId($userId);
        $data['permissions']['roleInfo']['userGroups'] = array(0);
        foreach ($groups as $group) {
            $data['permissions']['roleInfo']['userGroups'][] = $group['groupId'];
        }
        if (empty($results)) {
            return $data;
        }
        $userRow = $this->getUserById($userId);
        $data['permissions']['roleInfo']['ranking'] = $userRow->getRanking();
        $data['permissions']['roleInfo']['bucketSize'] = $userRow->getBucketSize();
        $data['permissions']['roleInfo']['userMobile'] = $userRow->getMobile();
        $data['permissions']['roleInfo']['userName'] = $userRow->getName();
        $data['permissions']['roleInfo']['userEmail'] = $userRow->getEmail();
        $data['permissions']['roleInfo']['empCode'] = $userRow->getEmpCode();
        $data['permissions']['roleInfo']['userType'] = $userRow->getUserType();

        $data['roleTypeId'] = $results[0]['roleTypeId'];
        foreach ($results as $result) {
            if ($result['groupId']) {
                $data['permissions'][$result['code']]['groups'][$result['groupId']]['id'] = $result['groupId'];
                $data['permissions'][$result['code']]['groups'][$result['groupId']]['label'] = $result['groupName'];
                $data['permissions'][$result['code']]['groups'][$result['groupId']]['groupRank'] = $result['groupRank'];
                $data['permissions'][$result['code']]['groups'][$result['groupId']]['category'] = $result['groupCategory'];
                if ($result['idInSystem']) {
                    $data['permissions'][$result['code']]['groups'][$result['groupId']]['idInSystem'] = $result['idInSystem'];
                }
            } elseif ($result['code']) {
                $data['permissions'][$result['code']] = 1;
            }
        }
        return $data;
    }

    /**
     * @author Umardeen
     * @param $parentUserId
     * @return array of user heirarchy
     */
    public function getChildUsersOfUser($parentUserId, &$data = array()) {
        if (!is_array($data)) {
            $data = array();
        }
        $qb = $this->returnExDbCon()->getItmsSlaveEntity()->getRepository('App\Entity\Users')
                ->createQueryBuilder('user')
                ->select('user.userType', "roleUser.roleId", 'role.name as roleName',
                        'user.id as userId', 'user.empCode', 'user.bucketSize', 'user.ranking', 'user.name', 'user.email')
                ->leftjoin('App\Entity\AclRoleUser', 'roleUser', 'WITH', "roleUser.userId = user.id")
                ->leftjoin('App\Entity\AclRoles', 'role', 'WITH', "roleUser.roleId = role.id")
                ->where("user.parentUserId = :parentUserId")
                ->setParameter('parentUserId', $parentUserId);
        $results = $qb->getQuery()->getResult();
        if (!empty($results)) {
            $data[$parentUserId]['children'] = array();
            foreach ($results as $result) {
                $data[$parentUserId]['children'][$result['userId']] = $result;
                if (!isset($data[$parentUserId]['children'][$result['userId']]['children'])) {
//                    $data[$parentUserId]['children'][$result['userId']]['children'] = array();
                    $this->getChildUsersOfUser($result['userId'],
                            $data[$parentUserId]['children']);
                }
            }
        }
    }

    /**
     * @author Umardeen
     * @param $id
     * @return array of user Permission
     */
    public function getUserById($id) {
        return $this->entityManager->getRepository('App\Entity\Users')->findOneBy(
                        array('id' => $id)
        );
    }

    /**
     * @author Umardeen
     * @param $emailId
     * @return array of user Permission
     */
    public function getUserByEmailId($emailId) {
        return $this->entityManager->getRepository('App\Entity\Users')->findOneBy(
                        array('email' => $emailId)
        );
    }

    /**
     * @author Umardeen
     * @param $userId
     * @return User list
     */
    public function getSfaRelatedUsers($userId) {
        $relatedUsers = array($userId);
        $relatedPolicies = array(0);
        $query = $this->returnExDbCon()->getItmsSlaveEntity()->getRepository('App\Entity\PolicyTeamUsers')
                ->createQueryBuilder('plcTeam')
                ->select('plcTeam.policyId')
                ->where("plcTeam.userId = '{$userId}'")
                ->groupBy('plcTeam.policyId');

        $results = $query->getQuery()->getResult();

        foreach ($results as $result) {
            $relatedPolicies[] = $result['policyId'];
        }
        $queryUser = $this->returnExDbCon()->getItmsSlaveEntity()->getRepository('App\Entity\PolicyTeamUsers')
                ->createQueryBuilder('plcTeam')
                ->select('plcTeam.userId')
                ->where('plcTeam.policyId IN (:policyIds)')
                ->setparameter('policyIds', $relatedPolicies)
                ->groupBy('plcTeam.userId');
        $results = $queryUser->getQuery()->getResult();

        foreach ($results as $result) {
            $relatedUsers[] = $result['userId'];
        }

        $query = $this->entityManager->getRepository('App\Entity\Users')
                ->createQueryBuilder('user')
                ->select('user.id', 'role.name as roleName', 'user.email', 'user.name')
                ->join('App\Entity\AclRoleUser', 'roleUser', 'WITH', 'roleUser.userId IN (:userIds) AND roleUser.userId = user.id')
                ->join('App\Entity\AclRoles', 'role', 'WITH', 'role.id = roleUser.roleId')
                ->where('user.id IN (:userIds)')
                ->setparameter('userIds', $relatedUsers)
                ->orderBy('role.name')
                ->groupBy('user.id');
        ;
        return $query->getQuery()->getResult();
    }

    /**
     * @author Ankita
     * @param $id
     * @return array of user Groups
     */
    public function getGroupsByUserId($id) {
        $query = $this->entityManager->getRepository('App\Entity\UserGroups')
                ->createQueryBuilder('grp')
                ->select('grp.userId', 'grp.groupId')
                ->where('grp.userId = :id')
                ->setparameter('id', $id);
        return $query->getQuery()->getResult();
    }

    public function unsetLoginUser($uid) {
        $userData = $this->getUserById($uid);
        $userData->setIsLogin(0);
        $this->entityManager->persist($userData);
        $this->entityManager->flush();
    }

    public function getRoleDetailByName($name) {
        $data = [];
        $roleDetail = $this->entityManager->getRepository('App\Entity\AclRoles')->findOneBy(
                array('name' => $name)
        );
        if (!empty($roleDetail)) {
            $data = $roleDetail;
        }
        return $data;
    }

    public function getGroupIdsToTag($groupName) {
        $groupId = '';
        $data = [];
        $groupDetail = $this->entityManager->getRepository('App\Entity\Groups')->findOneBy(
                array('name' => $groupName, 'category' => 'ROLE_PERMISSIONS')
        );
        if (empty($groupDetail)) {
            $groups = new Groups();
            $groups->setName($groupName);
            $groups->setCategory('ROLE_PERMISSIONS');
            $groups->SetIsReadOnly(1);
            $this->entityManager->persist($groups);
            $this->entityManager->flush();
            $groupId = $groups->getId();
        } else {
            $groupId = $groupDetail->getId();
        }
        return $groupId;
    }

    public function getDealerAgentMapping($userId) {
        $returnData = array(
            'status' => 'T',
            'data' => array()
        );
        $query = $this->returnExDbCon()->getItmsSlaveEntity()
                ->getRepository('App\Entity\SupportUserMapping')
                ->createQueryBuilder('usrMpg')
                ->select('usrMpg.itmsUserId', 'usrMpg.dealerId', 'usrMpg.sourceCreatorId');
        if ($userId) {
            $query->where("usrMpg.itmsUserId = :userId")
                    ->setParameter('userId', $userId);
        }
        $results = $query->getQuery()->getResult();
        if (!empty($results)) {
            $returnData['data'] = $results;
        }
        return $returnData;
    }

    public function addSupportUserMapping($userId, $filterByDealer, $filterId) {
        $supportUserMapping = new \App\Entity\SupportUserMapping();
        $supportUserMapping->setItmsUserId($userId);
        if ($filterByDealer) {
            $supportUserMapping->setDealerId($filterId);
        } else {
            $supportUserMapping->setSourceCreatorId($filterId);
        }
        $this->entityManager->persist($supportUserMapping);
        $this->entityManager->flush();
        return $supportUserMapping->getId();
    }

    public function getSupportUserMapping($userId) {
        $userMappingDetails = [];
        $userMapping = $this->entityManager->getRepository('App\Entity\SupportUserMapping')
                ->findOneBy(array(
            'itmsUserId' => $userId
        ));
        if (!empty($userMapping)) {
            if ($userMapping->getDealerId() > 0) {
                $userMappingDetails['key'] = 'dealer_id';
                $userMappingDetails['value'] = $userMapping->getDealerId();
            } elseif ($userMapping->getSourceCreatorId() > 0) {
                $userMappingDetails['key'] = 'source_creator_id';
                $userMappingDetails['value'] = $userMapping->getSourceCreatorId();
            }
        }
        return $userMappingDetails;
    }

    public function getUserByEmailIdAndMobile($emailId, $mobile) {
        $dataArray = [];
        $userdata = $this->entityManager->getRepository('App\Entity\Users')->findOneBy(
                array('email' => $emailId, 'mobile' => $mobile)
        );
        if ($userdata) {
            $dataArray = $userdata;
        }
        return $dataArray;
    }

    public function getDealerDetailsFromEmail($emailId) {
        $dataArray = [];
        $dealerData = $this->entityManager->getRepository('App\Entity\DealerInfo')->findOneBy(
                array('email' => $emailId, 'dealerType' => 1)
        );
        if ($dealerData) {
            $dataArray = $dealerData;
        }
        return $dataArray;
    }

    public function saveUserUpdatedPassword($userDetail) {
        $this->entityManager->persist($userDetail);
        $this->entityManager->flush();
    }

    public function checkUserEmailDomain($mailId) {
        $domain = '';
        $allowedDomains = explode(',', Constant::INTERNAL_ALLOWED_EMAILS_DOMAINS);
        if (filter_var($mailId, FILTER_VALIDATE_EMAIL)) {
            $exploadedArray = explode('@', $mailId);
            $domain = array_pop($exploadedArray);
        }
        if (in_array($domain, $allowedDomains)) {
            return true;
        } else {
            return false;
        }
    }

    public function getParentUser($userId) {
        $return = array(
            'statusCode' => 200,
            'status' => 'F',
            'message' => 'No Parent Found',
            'data' => null
        );
        $query = $this->returnExDbCon()->getItmsSlaveEntity()
                ->getRepository('App\Entity\Users')
                ->createQueryBuilder('usr')
                ->select('usr.parentUserId', 'usr.email', 'usr.mobile', 'usr.name', 'prntUsr.name as parentName', 'prntUsr.email as parentEmail', 'prntUsr.mobile as parentMobile');
        $query->join('App\Entity\Users', 'prntUsr', 'WITH', 'prntUsr.id=usr.parentUserId');
        $query->where("usr.id = :userId");
        $query->andWhere("usr.parentUserId != 0");
        $query->setParameter('userId', $userId);
        $query->setMaxResults(1);
        $results = $query->getQuery()->getResult();
        if (!empty($results)) {
            $return['statusCode'] = '200';
            $return['status'] = 'T';
            $return['message'] = 'Parent User Fetched Successfully';
            $return['data'] = $results[0];
        }
        return $return;
    }

}

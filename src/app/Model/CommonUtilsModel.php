<?php

namespace App\Model;

use App\Model\Root\AbstractModel;
use Doctrine\ORM\Tools\Pagination\Paginator;
use \App\Constant as Constant;

/**
 * Class CommonUtilsModel
 * @package App
 */
class CommonUtilsModel extends AbstractModel {

    private static $mongoObj;

    public function __construct($c) {
        parent::__construct($c);
        self::$mongoObj = $this->getMongoConnection();
    }

    /**
     * Paginator Helper
     *
     * Pass through a query object, current page & limit
     * the offset is calculated from the page and limit
     * returns an `Paginator` instance, which you can call the following on:
     *
     *     $paginator->getIterator()->count() # Total fetched (ie: `5` posts)
     *     $paginator->count() # Count of ALL posts (ie: `20` posts)
     *     $paginator->getIterator() # ArrayIterator
     *
     * @param Doctrine\ORM\Query $dql   DQL Query Object
     * @param integer            $page  Current page (defaults to 1)
     * @param integer            $limit The total number per page (defaults to 10)
     *
     * @return \Doctrine\ORM\Tools\Pagination\Paginator
     */
    public function paginate($dql, $page = 1, $limit = 6) {
        $start = ($page - 1) * $limit;
        $paginator = new Paginator($dql);
        $paginator->getQuery()
                ->setFirstResult($start) // Offset
                ->setMaxResults($limit); // Limit
        return $paginator;
    }

    /**
     * @author Agam
     * @name generatePassword
     * @since 10/March/2016
     * @description generate the password
     * @param string|null $password
     * @param string|null $cost
     * @return string|password
     */
    public static function generatePassword($password, $cost = 11) {
        $salt = substr(base64_encode(openssl_random_pseudo_bytes(17)), 0, 22);
        $salt = str_replace('+', '.', $salt);
        $param = '$' . implode('$', array("2y", str_pad($cost, 2, "0", STR_PAD_LEFT), $salt));
        return crypt($password, $param);
    }

    /**
     * @author Agam
     * @name validatePassword
     * @since 10/March/2016
     * @description validate the password
     * @param string|null $password
     * @param string|null $hash
     * @return boolean|true
     */
    public static function validatePassword($password, $hash) {
        return crypt($password, $hash) == $hash;
    }

    public function get_config_data($config_name, $returnAsArray = false) {
        try {
            $requestUrl = Constant::ITMS_API_URL . "get_config_data/{$config_name}";
            $headers = $this->getMasterHeaders();
            $response = $this->getRequest($requestUrl, $headers);
            if ($response) {
                $config = json_decode($response, $returnAsArray);
                return !$returnAsArray ? $config->data->config_values : $config['data']['config_values'];
            }
            return array();
        } catch (Exception $exc) {
            return array();
        }
    }

    public function get_multi_config_data($config_name, $returnAsArray = false) {
        try {
            $data = $this->returnExDbCon()->getItmsSlaveEntity()->getRepository('App\Entity\Configuration')
                    ->findBy(array(
                'config_name' => $config_name,
                'is_deleted' => '0',
                'status' => '1'));
            $configs = array();
            foreach ($data as $dataRow) {
                $configs[$dataRow->getConfigName()] = json_decode($dataRow->getConfigValues(), $returnAsArray);
            }
            return $configs;
        } catch (Exception $exc) {
            return array();
        }
    }

    /**
     * Logged all the third party API calls
     *
     * @author Shweta
     * @return variantId, variantName, modelId, makeId, makeName, modelName
     */
    public function loggedApiCalls($param, $logId = 0) {
        $resArr = array();
        try {
            $mongoCollection = 'third_party_api_log_' . $param['method'];
            $mongoConfig = $this->getMongoConfig();
            $collection = self::$mongoObj->{$mongoConfig['dbname']}->{$mongoCollection};
            if (!$logId) {
                $logInsert = array(
                    'ip' => $_SESSION['request_ip_address'],
                    'headers' => $param['headers'],
                    'request_url' => $param['request_url'],
                    'queryParams' => $param['queryParams'],
                    'method' => $param['method'],
                    'created' => new \MongoDB\BSON\UTCDateTime()
//                    'response' => $param['response']
                );
                if (!empty($_SERVER['api_request_id'])) {
                    $logInsert['bunch_id'] = $_SERVER['api_request_id'];
                }
                if (self::$mongoObj) {
                    $insertedRow = $collection->insertOne($logInsert);
                    return $insertedRow->getInsertedId();
                }
            } else {
                $updateArray = array(
                    'responseAt' => new \MongoDB\BSON\UTCDateTime(),
                    'response' => $param['response']
                );
                if (self::$mongoObj) {
                    $responseData = array(
                        '$set' => $updateArray
                    );
                    $collection->updateOne(array('_id' => $logId), $responseData);
                }
            }
            return true;
        } catch (Exception $exc) {
            
        }
    }

    /**
     * @name encrypt with key
     * @author Agam
     * @since 09/01/2017
     */
    function encryptString($string, $key) {
        $encrypt_method = "AES-256-CBC";
        //pls set your unique hashing key
        $secret_iv = $secret_key = $key;
        // hash
        $key = hash('sha256', $secret_key);
        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hash('sha256', $secret_iv), 0, 16);
        $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
        return base64_encode($output);
    }

    /**
     * @name Decrypt with key
     * @author Agam
     * @since 09/01/2017
     */
    function decryptString($string, $key) {
        $encrypt_method = "AES-256-CBC";
        //pls set your unique hashing key
        $secret_iv = $secret_key = $key;
        // hash
        $key = hash('sha256', $secret_key);
        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hash('sha256', $secret_iv), 0, 16);
        var_dump(openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv));
        exit;
        return openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
    }

    public function itmsErrorLog($action, $ticket_id, $params, $error_msg, $subject = '') {

        $error_json = is_array($params) ? json_encode($params) : $params;
        $extra_data = json_encode(array('action' => $action));

        $apiURL = '/send_mail';
        $data['template_variable']['title'] = $subject ? $subject : 'ITMS Connection error.';
        $data['template_variable']['error_message'] = $error_msg;
        $data['template_variable']['mongo_api_id'] = is_array($_SERVER['api_request_id']) ? json_decode($_SERVER['api_request_id']) : $_SERVER['api_request_id'];
        $data['template_variable']['ticket_id'] = $ticket_id;
        $data['template_variable']['error_json'] = $error_json;
        $data['template_variable']['extra_data'] = $extra_data;
        $data['to'] = array('amit.chaudhary@insurancedekho.com' => 'Amit Chaudhary');
        $actionsArr = CommonUtilsModel::get_config_data('ACTIONS_FOR_ERROR_MAIL');

        $additionalMailArr = [];
        if (in_array($action, $actionsArr)) {
            $additionalEmailsObj = json_encode(CommonUtilsModel::get_config_data('ADDITIONAL_MAIL_IDS_FOR_ERRORS'));
            $additionalMailArr = json_decode($additionalEmailsObj, 1);
            if (is_array($additionalMailArr)) {
                $data['to'] = array_merge($data['to'], $additionalMailArr);
            }
        }

        $finalData['template_name'] = 'ITMS_ERROR_REPORT_INS';
        $finalData['to'] = json_encode($data['to']);
        $finalData['template_variable'] = json_encode($data['template_variable']);
        $finalData['subject_variable'] = $finalData['template_variable'];
        $finalData['reference_type'] = 'itms error';
        $finalData['reference_id'] = $ticket_id;

        $this->sendCurlPost($apiURL, $finalData);
    }

}

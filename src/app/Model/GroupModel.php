<?php

namespace App\Model;

use App\Entity\Groups;
use App\Entity\UserRegionsMapping;
use App\Model\Root\AbstractModel;

/**
 * Class Model
 * @package App
 */
class GroupModel extends AbstractModel {

    private $errors = array();

    /**
     * @author Umardeen
     * @name getAllGroups
     * @since 06/March/2018
     * @description Returns all Groups
     * @return array of groups
     */
    public function getAllGroups($filter) {
        $return = array(
            'statusCode' => 200,
            'status' => 'F',
            'message' => 'No Groups Found',
            'data' => null
        );
        $query = $this->returnExDbCon()->getItmsSlaveEntity()->getRepository('App\Entity\Groups')
                ->createQueryBuilder("grp")
                ->select("grp.idInSystem", "grp.name", "grp.id", "grp.category", "groupCate.isMultiSelect as categoryMultiSelect", "groupCate.isRequired as categoryRequired", "groupCate.label as categoryName", "grp.isReadOnly", "0 as deleted", "grp.order")
        ;
        $query->join('App\Entity\GroupCategories', 'groupCate', 'WITH', "groupCate.isActive = 1 AND groupCate.code = grp.category");

        if (!empty($filter['categories']) && is_array($filter['categories'])) {
            $query->andWhere("grp.category IN (:categories)")
                    ->setParameter(':categories', $filter['categories']);
        }

        if (!empty($filter['permissionId'])) {
            $query->join('App\Entity\PermissionGroups', 'permGroup', 'WITH', "permGroup.groupId = grp.id")
                    ->join('App\Entity\AclRolePermissions', 'rolePermission', 'WITH', "rolePermission.permissionId = {$filter['permissionId']} AND permGroup.permissionId = rolePermission.id")
                    ->andWhere("rolePermission.permissionId = {$filter['permissionId']}")
                    ->andWhere("rolePermission.roleId = {$filter['roleId']}");
        }
        // print_r($filter['userId']);
        if (!empty($filter['userId'])) {
            if (in_array("REGION", $filter['categories'])) {
                $query->join('App\Entity\UserRegionsMapping', 'usrRegionMap', 'With', "grp.id=usrRegionMap.regionId")
                        ->andWhere("usrRegionMap.userId = {$filter['userId']}")
                        ->andWhere("usrRegionMap.status =1");
            } else {
                $query->join('App\Entity\UserGroups', 'usrGrp', 'With', "usrGrp.userId = {$filter['userId']} AND usrGrp.groupId = grp.id")
                        ->andWhere("usrGrp.userId = {$filter['userId']}");
            }
        }

        $query->orderBy("groupCate.orderBy")
                ->orderBy("grp.order")
                ->groupBy("grp.id");
        $groupRows = $query->getQuery()->getResult();

        if (empty($groupRows)) {
            $return['statusCode'] = 404;
            $return['status'] = 'F';
            $return['message'] = 'No Group found';
        } else {
            $return['statusCode'] = '200';
            $return['status'] = 'T';
            $return['message'] = 'Groups Listed Successfully';
            $return['data'] = $groupRows;
        }
        return $return;
    }

    /**
     * @author Umardeen
     * @name saveGroup
     * @since 06/March/2018
     * @description Creates New Groups
     * @param  $data
     * @return array of status
     */
    public function saveGroup($data) {
        $return = array(
            'statusCode' => 200,
            'status' => 'F',
            'message' => 'Group Not Saved',
            'data' => null
        );
        if (!$data['name']) {
            $return['status']= 'F';
            $return['statusCode'] = 304;
            $return['message'] = 'Group Name is required';
        }
        $groupRow = $this->entityManager->getRepository('App\Entity\Groups')->findOneBy(
                array(
                    'name' => $data['name'],
                    'category' => strtoupper($data['categoryName'])
                )
        );

        if ($groupRow) {
            $return['status']= 'F';
            $return['statusCode'] = 304;
            $return['message'] = 'Group already exists';
        } else {
            $groupRow = new Groups();
            $groupRow->setName($data['name']);
            $groupRow->setCategory($data['categoryName']);
            $this->entityManager->persist($groupRow);
            $this->entityManager->flush();
            $return['status']= 'T';
            $return['data']['id'] = $groupRow->getId();
            $return['statusCode'] = 200;
            $return['message'] = 'Group Created Successfully.';
        }
        return $return;
    }

    /**
     * @author Umardeen
     * @name updateGroup
     * @since 06/March/2018
     * @description Update Groups
     * @param  $data
     * @return array of status
     */
    public function updateGroup($data) {
        $return = array(
            'statusCode' => 201,
            'status' => 'F',
            'message' => 'Group Not Updated',
            'data' => null
        );        
        $query = $this->entityManager->getRepository('App\Entity\Groups')
                ->createQueryBuilder("grp")
                ->select("grp.id")
                ->where("grp.name = '{$data['name']}'")
                ->andWhere("grp.name != '{$data['id']}'")
                ->andWhere("grp.category = '{$data['category']}'")
                ->setMaxResults(1);
        $groupRows = $query->getQuery()->getResult();
        if (!empty($groupRows)) {
            $return['status']= 'F';
            $return['statusCode'] = 304;
            $return['message'] = 'Duplicate group name';
        } else {
            $groupRow = $this->entityManager->getRepository('App\Entity\Groups')->findOneBy(
                    array(
                        'id' => $data['id']
                    )
            );
            if ($groupRow && !$groupRow->getIsReadOnly()) {
                $groupRow->setName($data['name']);
                $this->entityManager->persist($groupRow);
                $this->entityManager->flush();
                $return['data']['id'] = $groupRow->getId();
            }
            $return['status']= 'T';
            $return['statusCode'] = 200;
            $return['message'] = 'Group Updated Successfully.';
        }
        return $return;
    }

    /**
     * @author Umardeen
     * @name updateGroup
     * @since 06/March/2018
     * @description Update Groups
     * @param  $data
     * @return array of status
     */
    public function getGroupsByPermissionCode($code) {
        $return = array(
            'status' => 'T',
            'data' => array()
        );
        $permissionRow = $this->entityManager->getRepository('App\Entity\AclPermissions')
                ->findOneBy(array(
            'code' => $code,
            'allowGroups' => 1
        ));

        if (empty($permissionRow)) {
            $return['status'] = 'F';
            $return['msg'] = 'No Group allowed for this permission';
            return $return;
        }
        $groups = $permissionRow->getAllowedGroupCategory();
        if (empty($groups)) {
            $return['status'] = 'F';
            $return['msg'] = 'No Group in this permission';
            return $return;
        }
        $groupCategories = explode(',', $groups);
        $groupRowset = $this->entityManager->getRepository('App\Entity\Groups')->findBy(
                array(
                    'category' => $groupCategories
                )
        );
        foreach ($groupRowset as $groupRow) {
            $return['data'][$groupRow->getId()] = array(
                'id' => $groupRow->getId(),
                'label' => $groupRow->getName(),
                'groupRank' => $groupRow->getOrder(),
                'idInSystem' => $groupRow->getIdInSystem(),
                'category' => $groupRow->getCategory()
            );
        }
        return $return;
    }

    /**
     * @author Umardeen
     * @name deleteGroup
     * @since 06/March/2018
     * @description Delete Groups
     * @param  $data
     * @return array of status
     */
    public function deleteGroup($data) {
        $return = array(
            'statusCode' => 200,
            'status' => 'F',
            'message' => 'Group Not Deleted',
            'data' => null
        );
        $groupRow = $this->entityManager->getRepository('App\Entity\Groups')->findOneBy(
                array(
                    'id' => $data['id']
                )
        );
        if ($groupRow && !$groupRow->getIsReadOnly()) {
            $this->entityManager->remove($groupRow);
            $this->entityManager->flush();
        }
        $return['status'] = 'T';
        $return['statusCode'] = 200;
        $return['message'] = 'Group Deleted Successfully';
        return $return;
    }

}

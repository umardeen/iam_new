<?php

namespace App\Model\Root;

//use Doctrine\ORM\EntityManager;
use App\Constant as Constant;
use Aws\S3\S3Client;
//use Aws\S3\Exception\S3Exception;
//use Dompdf\Dompdf;

abstract class AbstractModel {

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    static $_CONTROLLER_OBJ;
    static $_USER_CAPABILIES = array();
    static $_USER_HEIRARCHY = array();
    static $_LOGGED_IN_USER;
    static $_ROLE_TYPE_ID = 0;
    static $_ROLE_TICKET_TYPE_ID = 0;
    static $_OWN_TEAM_GROUP_ID = 0;
    static $_CURRENT_TICKET_ROW = null;
    static $_CURRENT_UCD_ROW = null;
    static $_CURRENT_ONLINE_QUOTE_ROW = null;
    static $_CURRENT_DEALER_ROW = null;
    static $_CURRENT_VEHICLE_ROW = null;
    private $exDbCon = null;
    protected $entityManager = null;
    protected $mongoManager = null;
    protected $configPath;
    protected $configPathShared;
    public $_errmessage = array();

    public function __construct($container) {
        $this->entityManager = $container->get('em');
        $this->exDbCon = $container->get('exDbCon');
        $this->mongoManager = $container->get('mongo');
        $this->container = $container;
        $this->configPath = $container->settings['config_path'];
        $this->configPathShared = $container->settings['config_path_shared'];
    }

    public function getMongoManager() {
        return $this->mongoManager;
    }
   
    public function getMongoConnection() {
        return $this->mongoManager['connection'];
    }
    
    public function getMongoConfig() {
        return $this->mongoManager['config'];
    }

    public function returnExDbCon() {
        return $this->exDbCon;
    }

    public function returnEntityManager() {
        return $this->entityManager;
    }

    public function returnConfigPath() {
        return $this->configPath;
    }

    public function replaceBlankToNull(&$dataValue) {
        if (is_array($dataValue)) {
            foreach ($dataValue as &$value) {
                $this->replaceBlankToNull($value);
            }
        } elseif ($dataValue === '') {
            $dataValue = null;
        }
    }

    public function replaceNullToBlank(&$dataValue) {
        if (is_array($dataValue)) {
            foreach ($dataValue as &$value) {
                $this->replaceNullToBlank($value);
            }
        } elseif (is_null($dataValue)) {
            $dataValue = '';
        }
    }

    public function changeFieldsToInteger($fields = array(), &$dataValue) {
        foreach ($dataValue as $key => &$value) {
            if (!is_array($value)) {
                if (in_array($key, $fields)) {
                    $value = (int) $value;
                    $value = $value ? $value : 0;
                }
            } elseif (isset($fields[$key]) && is_array($fields[$key])) {
                $this->changeFieldsToInteger($fields[$key], $value);
            }
        }
    }

    /*     * ***********************EVENTS CREATION METHODS****************************** */

    public function responseFormat($postData = array(), $message = '', $codes = array(), $customMsg = array()) {
        $errmessage = array();
        $errmessage[24] = array('msg' => 'Something went wrong. Please try again.', 'desc' => 'Something went wrong. Please try again.');
        $errmessage[26] = array('msg' => 'Username/Mobile already exists.', 'desc' => 'Username/Mobile already exists.');
        $errmessage[35] = array('msg' => 'Firstname must be present', 'desc' => 'Firstname must be present');
        $errmessage[36] = array('msg' => 'Email must be present', 'desc' => 'Email must be present');
        $errmessage[37] = array('msg' => 'Mobile must be present', 'desc' => 'Mobile must be present');
        $errmessage[101] = array('msg' => 'Username must be present', 'desc' => 'Username must be present');
        $errmessage[102] = array('msg' => 'Username must be valid', 'desc' => 'Username must be valid');
        $errmessage[103] = array('msg' => 'Password must be present', 'desc' => 'Password must be present');
        $errmessage[114] = array('msg' => 'Invalid Mobile No', 'desc' => 'Invalid Mobile No');
        $errmessage[9114] = array('msg' => 'User is not registered, Please register.', 'desc' => 'User is not registered, Please register.');
        $errmessage[115] = array('msg' => 'Invalid Username/Password', 'desc' => 'Invalid Username/Password');
        $errmessage[116] = array('msg' => 'Invaild Request', 'desc' => 'Invaild Request');
        $errmessage[119] = array('msg' => 'Role with same name already exists.', 'desc' => 'Role with same name already exists.');
        $errmessage[120] = array('msg' => 'Role name must be present', 'desc' => 'Role name must be present');
        $errmessage[121] = array('msg' => 'Role name must be valid', 'desc' => 'Role name must be valid');
        $errmessage[122] = array('msg' => 'Role permission must be present', 'desc' => 'Role permission must be present');
        $errmessage[129] = array('msg' => 'User does not exist.', 'desc' => 'User does not exist.');
        $errmessage[130] = array('msg' => 'Role type is required.', 'desc' => 'Role type is required.');
        $errmessage[602] = array('msg' => 'Name already exists.', 'desc' => 'Name already exists.');
        $errmessage[301] = array('msg' => 'Invalid OTP', 'desc' => 'Invalid OTP');
        $errmessage[3301] = array('msg' => 'OTP could not be sent, Please try again.', 'desc' => 'OTP couldnot be sent, Please try again.');
        $errmessage[302] = array('msg' => 'Required parameters not passed', 'desc' => 'Required parameters not passed');
        $errmessage[4006] = array('msg' => 'Invalid Credentials', 'desc' => 'Invalid Credentials');
        $errmessage[4003] = array('msg' => 'Unauthorized access', 'desc' => 'Unauthorized access');
        $errmessage[6000] = array('msg' => 'Invalid User Id or Invalid Token.', 'desc' => 'Invalid User Id or Invalid Token.');
        if (is_array($this->_errmessage) && !empty($this->_errmessage)) {
            $errmessage += $this->_errmessage;
        }
        if (is_array($postData)) {
            array_walk_recursive($postData, function (&$item, $key) {
                if (empty($item) || $item == null) {
                    $item = utf8_encode($item);
                }
            });
        }

        if (!empty($codes)) {
            $codes = array_unique($codes);
            $errorArr = array();
            foreach ($codes as $k => $val) {
                // if(is_numeric($val)) {
                $errorArr[$k]['code'] = $val;
                $errorArr[$k]['message'] = (!empty($errmessage[$val]['msg'])) ? $errmessage[$val]['msg'] : $customMsg[$val]['msg'];
                $errorArr[$k]['description'] = (!empty($errmessage[$val]['desc'])) ? $errmessage[$val]['desc'] : $customMsg[$val]['desc'];
                if (!empty($customMsg[$val]['displayMessage'])) {
                    $errorArr[$k]['displayMessage'] = $customMsg[$val]['displayMessage'];
                }
                if (!empty($customMsg[$val]['message'])) {
                    $errorArr[$k]['message'] = $customMsg[$val]['message'];
                }
                // }
            }
            $format["error"] = $errorArr;
            if (!empty($postData)) {
                $format["data"] = $postData;
                $format["message"] = $message;
            }
        } else {
            $format = array(
                "data" => $postData,
                "message" => $message,
            );
        }
        return $format;
    }

    public function getRequest($url, $headers = array(), $auth = false) {
        /*
         * Start Logging here
         */
        $getUrl = explode('?', $url);
        $param = array();
        $param['method'] = 'GET';
        $param['request_url'] = $getUrl[0];
        // $param['queryParams'] = $data;//json_encode($data);
        if ($getUrl[1]) {
            parse_str(parse_url($url, PHP_URL_QUERY), $array);
            $getUrlJson = $array ? json_encode($array) : $getUrl[1];
        }
        $headers[] = 'x-api-id:' . ($_SERVER['api_request_id'] ? $_SERVER['api_request_id'] : '');
        $param['queryParams'] = $getUrlJson ? $getUrlJson : '';
        $param['headers'] = json_encode($headers);
//        $param['response'] = $dataStr;//json_encode($result);
        //Logging
        $commonUtilsModel = new \App\Model\CommonUtilsModel($this->container);
        $logInsert = $commonUtilsModel->loggedApiCalls($param);
        $ch = curl_init($url);
        if (empty($headers))
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Expect:'));
        else
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        if (!empty($auth)) {
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($ch, CURLOPT_USERPWD, $auth);
        }

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $dataStr = curl_exec($ch);
//        $header = curl_getinfo($ch);
//        $error = curl_errno($ch);
//        $errormsg = curl_error($ch);
        curl_close($ch);
        $param['response'] = $dataStr; //json_encode($result);
        //Logging
        $commonUtilsModel = new \App\Model\CommonUtilsModel($this->container);
        $commonUtilsModel->loggedApiCalls($param, $logInsert);
        /*         * * End of logging here *** */
        return $dataStr;
    }

    /**
     * @author Bhanu
     * @since Aug/2016
     * @name curlRequest
     * @description CURL API Request for GET, POST and PUT
     */
    public function curlRequest($url, $data = array(), $header = array(), $method = "POST", $ref = '') {
        try {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            if ($data) {
                $getUrl = explode('?', $url);
                $param = array();
                $param['method'] = !empty($method) ? $method : 'POST';
                $param['request_url'] = $getUrl[0];
                $param['queryParams'] = $data; //json_encode($data);
                $header[] = 'x-api-id:' . ($_SERVER['api_request_id'] ? $_SERVER['api_request_id'] : '');
                $param['headers'] = json_encode($header);
//                $param['response'] = $result;//json_encode($result);
                $commonUtilsModel = new \App\Model\CommonUtilsModel($this->container);
                $logInsert = $commonUtilsModel->loggedApiCalls($param);

                //condition for PUT and POST method
                if ($method == "PUT" && $ref == 'ENCODE') {
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
                    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
                } elseif ($method == "PUT") {
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
                    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
                } elseif ($method == 'POST' && ($ref == 'LMS' || $ref == 'ENC_DEC' || $ref == 'ENC_DEC_AR')) {
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
                    curl_setopt($ch, CURLOPT_POST, true);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
                    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
                } else {
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
                    curl_setopt($ch, CURLOPT_POST, true);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
                    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
                }

                //Condition for AUTH API only
                if ($header) {
                    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
                }

                //Tell cURL that it should only spend 20 seconds
                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);

                //A given cURL operation should only take 90 seconds max.
                curl_setopt($ch, CURLOPT_TIMEOUT, 90);

                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $result = curl_exec($ch);
                curl_close($ch);

                /*
                 * Start Logging here
                 */
//                $getUrl = explode('?', $url);
//                $param = array();
//                $param['method'] = !empty($method)?$method:'POST';
//                $param['request_url'] = $getUrl[0];
//                $param['queryParams'] = $data;//json_encode($data);
//                $param['headers'] = json_encode($header);
                $param['response'] = $result; //json_encode($result);
                //Logging
                $commonUtilsModel = new \App\Model\CommonUtilsModel($this->container);
                $commonUtilsModel->loggedApiCalls($param, $logInsert['_id']);
                /*                 * * End of logging here *** */
                //$time_elapsed_secs = microtime(true) - $start;
                return $result;
            }
        } catch (Exception $ex) {
            
        }
    }

    /**
     * @name converToTimezone
     * @description This Method will convert from one timezone to other
     * @param $fromTz, $toTz, $time, $format
     * @return Converted TimeStamp
     */
    public function converToTimezone($fromTz = 'GMT', $toTz = 'GMT', $time = '', $format = 'Y-m-d H:i:s') {
        if (!$time)
            $time = new \DateTime('now');
        $date = new DateTime($time, new DateTimeZone($fromTz));
        $date->setTimezone(new DateTimeZone($toTz));
        return $date->format($format);
    }

    /**
     * @author Bhanu
     * @name getS3ClientObject
     */
    public function getS3ClientObject() {
        try {
            $s3 = S3Client::factory(array(
                        'version' => 'latest',
                        'credentials' => [
                            'key' => Constant::GIRNAR_INS_AWS_ACCESS_KEY,
                            'secret' => Constant::GIRNAR_INS_AWS_SECRET_KEY,
                        ],
                        'region' => 'ap-south-1'
            ));
            return $s3;
        } catch (\Exception $ee) {
            return false;
        }
    }

    /**
     * @author Bhanu
     * @name getS3ClientObjectURL
     */
    public function getS3ClientObjectURL($objectKey, $minutes) {
        try {
            $s3 = $this->getS3ClientObject();

            $cmd = $s3->getCommand('GetObject', [
                'Bucket' => Constant::GIRNAR_INS_S3_BUCKET,
                'Key' => $objectKey
            ]);

            $request = $s3->createPresignedRequest($cmd, '+' . $minutes . 'minutes');

            return (string) $request->getUri();
        } catch (\Exception $ee) {
            return false;
        }
    }

    /**
     * @author Bhanu
     * @name getS3ObjectPublicURL
     */
    public function getS3ObjectPublicURL($bucketName, $objectKey) {
        $s3Object = $this->getS3ClientObject();

        $resultS3 = $s3Object->getObjectUrl($bucketName, $objectKey);
        if ($resultS3) {
            return $resultS3;
        } else {
            return false;
        }
    }

    /**
     * @author Bhanu
     * @name putObjectToS3Bucket
     */
    public function putObjectToS3BucketFromURL($bucketName, $objectKey, $url, $tagingData) {
        try {
            $s3Object = $this->getS3ClientObject();
            $headers = get_headers($url);
            if (stripos($headers[0], "200 OK")) {

                if (!file_exists('/tmp/tmpfile')) {
                    mkdir('/tmp/tmpfile');
                }
                // Create temp file
                $tempFilePath = '/tmp/tmpfile/' . basename($url);
                $tempFile = fopen($tempFilePath, "w") or die("Error: Unable to open file.");
                $fileContents = file_get_contents($url);
                $tempFile = file_put_contents($tempFilePath, $fileContents);

                $result = $s3Object->putObject([
                    'Bucket' => $bucketName,
                    'Key' => $objectKey,
                    'SourceFile' => $tempFilePath,
                    'Tagging' => $tagingData,
                    'StorageClass' => 'REDUCED_REDUNDANCY',
                    'ACL' => 'public-read'
                ]);

                //Delete tmp dir
                if (is_dir('/tmp/tmpfile') && !empty($result['ObjectURL'])) {
                    $deletedDirFlag = $this->delete_directory('/tmp/tmpfile');
                }

                return $result['ObjectURL'];
            } else {
                return false;
            }
        } catch (Aws\S3\Exception\S3Exception $e) {
            echo "There was an error uploading the file.\n";
        }
    }

    /**
     * @author Ankita
     * For delete s3 object
     */
    function deleteS3Object($bucket, $keyname) {
        $s3 = $this->getS3ClientObject();
        $delete = $s3->deleteObject([
            'Bucket' => $bucket,
            'Key' => $keyname
        ]);
        return true;
    }

    function delete_directory($dirname) {
        if (is_dir($dirname))
            $dir_handle = opendir($dirname);
        if (!$dir_handle)
            return false;
        while ($file = readdir($dir_handle)) {
            if ($file != "." && $file != "..") {
                if (!is_dir($dirname . "/" . $file)) {
                    unlink($dirname . "/" . $file);
                } else {
                    $this->delete_directory($dirname . '/' . $file);
                    rmdir($dirname . '/' . $file);
                }
            }
        }
        closedir($dir_handle);
        if (is_dir($dirname)) {
            rmdir($dirname);
        }
        return true;
    }

    public function getThirdPartyEncDecAuthToken($callbackfunction, $param) {
        $postData = array(
            'username' => Constant::ENC_DEC_USER,
            'password' => Constant::ENC_DEC_PASSWORD,
        );
        $header = ['Content-Type:application/json'];
        $requestUrl = Constant::ENC_DEC_API_URL . 'user/authenticate';
        $response = $this->curlRequest($requestUrl, $postData, $header, 'POST', 'ENC_DEC');
        $arrayResponse = json_decode($response);
        if (empty($arrayResponse->errors) && !empty($arrayResponse->data->token)) {
            $configRow = $this->entityManager->getRepository('App\Entity\Configuration')->findOneBy(
                    array('config_name' => ENC_DEC_AUTH_TOKEN)
            );
            $configRow->setConfigValues($arrayResponse->data->token);
            $this->returnEntityManager()->persist($configRow);
            $this->returnEntityManager()->flush();
            return $this->$callbackfunction($param);
        } else {
            return '';
        }
    }

    public function getThirdPartyEncrypted($input) {
        if (!is_array($input) && $input == '') {
            return $input;
        }
        $configModel = new \App\Model\ConfigurationModel($this->container);
        $configArr = array('ENC_DEC_ENABLE', 'ENC_DEC_AUTH_TOKEN');
        $configValueArr = $configModel->getMultipleConfigData($configArr, 1);
        if (!$configValueArr['ENC_DEC_ENABLE']) {
            return $input;
        }
        $authToken = trim($configValueArr['ENC_DEC_AUTH_TOKEN']);
        $requestUrl = Constant::ENC_DEC_API_URL . 'cipher/encrypt';
        if (is_array($input)) {
            $postData = [];
            foreach ($input as $k => $v) {
                array_push($postData, $v);
            }
        } else {
            $postData = array($input);
        }

        $header = ['Content-Type:application/json', 'authorization:' . $authToken];
        $response = $this->curlRequest($requestUrl, $postData, $header, 'POST', 'ENC_DEC_AR');
        $arrayResponse = json_decode($response);
        if (!empty($arrayResponse->data)) {
            $this->saveEncryptionToLog($postData, $arrayResponse->data);
            if (is_array($input)) {
                return $arrayResponse->data;
            } else {
                return $arrayResponse->data['0'];
            }
        } else {
            if (!empty($arrayResponse->errors)) {
                foreach ($arrayResponse->errors as $k => $v) {
                    if ($v->message == 'Authentication error. Valid token required.') {
                        return $this->getThirdPartyEncDecAuthToken('getThirdPartyEncrypted', $input);
                    }
                }
            }
            return '';
        }
    }

    public function getThirdPartyDecrypted($input) {
        $configModel = new \App\Model\ConfigurationModel($this->container);
        $configArr = array('ENC_DEC_ENABLE', 'ENC_DEC_AUTH_TOKEN');
        $configValueArr = $configModel->getMultipleConfigData($configArr, 1);
        if (!$configValueArr['ENC_DEC_ENABLE']) {
            if (!is_array($input)) {
                return $this->getDecryptedValueFromLog($input);
            }
            return $input;
        }
        $authToken = trim($configValueArr['ENC_DEC_AUTH_TOKEN']);
        $requestUrl = Constant::ENC_DEC_API_URL . 'cipher/decrypt';
        if (is_array($input)) {
            $postData = [];
            foreach ($input as $k => $v) {
                array_push($postData, array("id" => $v));
            }
        } else {
            $postData = [array('id' => $input)];
        }

        $header = ['Content-Type:application/json', 'authorization:' . $authToken];
        $response = $this->curlRequest($requestUrl, $postData, $header, 'POST', 'ENC_DEC_AR');

        $arrayResponse = json_decode($response);
        if (!empty($arrayResponse->data)) {
            if (is_array($input)) {
                return $arrayResponse->data;
            } else {
                return $arrayResponse->data['0'];
            }
        } else {
            if (!empty($arrayResponse->errors)) {
                foreach ($arrayResponse->errors as $k => $v) {
                    if ($v->message == 'Authentication error. Valid token required.') {
                        return $this->getThirdPartyEncDecAuthToken('getThirdPartyDecrypted', $input);
                    }
                }
            }
            return '';
        }
    }

    public function maskPhoneNumber($number) {
        $configModel = new \App\Model\ConfigurationModel($this->container);
        $configArr = array('ENC_DEC_ENABLE');
        $configValueArr = $configModel->getMultipleConfigData($configArr, 1);
        if (empty($number) || !$configValueArr['ENC_DEC_ENABLE']) {
            return $number;
        }
        $mask_number = substr($number, 0, 3) . str_repeat("#", strlen($number) - 5) . substr($number, -2);
        return $mask_number;
    }

    function maskEmailId($email) {
        $configModel = new \App\Model\ConfigurationModel($this->container);
        $configArr = array('ENC_DEC_ENABLE');
        $configValueArr = $configModel->getMultipleConfigData($configArr, 1);
        if (empty($email) || !$configValueArr['ENC_DEC_ENABLE']) {
            return $email;
        }
        $em = explode("@", $email);
        $name = implode(array_slice($em, 0, count($em) - 1), '@');
        $len = floor(strlen($name) / 2);
        return substr($name, 0, $len) . str_repeat('#', strlen($em[0]) - $len) . "@" . end($em);
    }

    public function getSfaUserByMobile($mobileNo, $sfaUserIds) {
        $query = $this->entityManager
                ->getRepository('App\Entity\AclRoles')->createQueryBuilder('aclRole')
                ->select('aclRole.salesRoleId, aclRole.name as roleName, user.name,user.mobile,user.email')
                ->innerJoin('App\Entity\AclRoleUser', 'roleUser', 'WITH', 'roleUser.roleId = aclRole.id')
                ->innerJoin('App\Entity\Users', 'user', 'WITH', 'user.id = roleUser.userId')
                ->where('aclRole.salesRoleId IN (:salesRoleId)')
                ->andWhere('user.mobile = :mobile')
                ->setParameter('salesRoleId', $sfaUserIds)
                ->setParameter('mobile', $mobileNo);
        $results = $query->getQuery()->getResult();
        if (!empty($results[0])) {
            return $results[0];
        }
        return false;
    }

    public function getPermissionValues($permission) {
        $permissionInfo = array();
        if (!self::$_LOGGED_IN_USER || !self::$_LOGGED_IN_USER->getIsSuperAdmin()) {
            foreach (self::$_USER_CAPABILIES[$permission]['groups'] as $row) {
                $permissionInfo['groups'][$row['category']][$row['id']] = $row;
            }
//            $permissionInfo = self::$_USER_CAPABILIES[$permission];
            if (empty($permissionInfo)) {
                $returnData['status'] = 'F';
                $returnData['msg'] = 'Permission Denied';
                return $returnData;
            }
        } else {
            $permissionGroup = \App\Model\GroupModel::getGroupsByPermissionCode($permission);
            $permissionInfo = array();
            if ($permissionGroup['status'] == 'F') {
                $returnData['status'] = 'F';
                $returnData['msg'] = $permissionGroup['msg'];
                return $returnData;
            }
            foreach ($permissionGroup['data'] as $row) {
                $permissionInfo['groups'][$row['category']][$row['id']] = $row;
            }
        }
        return $permissionInfo;
    }

    public function getGroupIdsByPermission($permission) {
        $groupIds = array(0);
        if (!empty(self::$_USER_CAPABILIES[$permission]['groups']) &&
                is_array(self::$_USER_CAPABILIES[$permission]['groups'])) {
            $groupIds = array();
            foreach (self::$_USER_CAPABILIES[$permission]['groups'] as $groupRow) {
                $groupIds[] = $groupRow['id'];
            }
        }
        return $groupIds;
    }

    public function getLoggedInUserId() {
        $userId = self::$_LOGGED_IN_USER && self::$_LOGGED_IN_USER->getId() ? self::$_LOGGED_IN_USER->getId() : Constant::CRON_USER_ID;
        return $userId;
    }

    public function getLoggedInHeaders() {
        $allHeaders = $this->container ? $this->container->get('request')->getHeaders() : array();
        $userId = isset($allHeaders['HTTP_X_AUTH_ID'][0]) ? $allHeaders['HTTP_X_AUTH_ID'][0] : '';
        $accessToken = isset($allHeaders['HTTP_X_AUTH_TOKEN'][0]) ? $allHeaders['HTTP_X_AUTH_TOKEN'][0] : '';
        $headers = ['x-auth-token:' . $accessToken, 'x-auth-id:' . $userId];
        return $headers;
    }
    
    public function getMasterHeaders() {
        $userId = constant::ITMS_MASTER_AUTH_ID;
        $accessToken = constant::ITMS_MASTER_AUTH_TOKEN;
        $headers = ['x-auth-token:' . $accessToken, 'x-auth-id:' . $userId];
        return $headers;
    }

}

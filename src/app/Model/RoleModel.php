<?php

namespace App\Model;

use App\Model\Root\AbstractModel;
use App\Entity\AclRolePermission;
use App\Entity\AclRolePermissions;
use App\Entity\PermissionGroups;
use App\Entity\AclRoles;

use \Respect\Validation\Validator as v;
use App\Constant as Constant;
/**

 * Class Model
 * @package App
 */
class RoleModel extends AbstractModel
{
    /**
    * @author Bhanu
    * @name listAllRoles
    * @since 27/June/2016
    * @description List all the roles according to requested parameters
    * @param array|null $options
    * @return array of roles
    */
    public function listAllRoles($requestParams)
    {
        $return = array(
            'statusCode' => 200,
            'status' => 'F',
            'message' => 'No Roles Found',
            'data' => null
        );

        if(!empty($requestParams['sortField'])){
            $sortField = 'r.'.$requestParams['sortField'];
        }else{
            $sortField = 'r.id';
        }
        if(!empty($requestParams['sortOrder'])){
            $sortOrder = $requestParams['sortOrder'];
        }else{
            $sortOrder = 'ASC';
        }
        //check if limit is in request params or not
        $limit = isset($requestParams['pageSize']) ? $requestParams['pageSize'] : '';
        if($limit)
            $start = ($requestParams['pageNo']-1)*$requestParams['pageSize'];
        
        $query = $this->entityManager->getRepository('App\Entity\AclRoles')
            ->createQueryBuilder('r')
            ->addOrderBy($sortField, $sortOrder)
            ->select(
                'r.id','r.name as name','r.description as description',
                    'r.productId as product_id','r.mappedSubsorceId as subsource',
//                'tt.name as ticket_name','p.name as product_name',
                    'r.createdAt','r.modifiedAt',
                'r.status','u.name as created_by','r.isReadOnly'
            )
//            ->leftJoin('App\Entity\TicketType', 'tt', 'WITH', 'r.ticketTypeId = tt.id')
//            ->leftJoin('App\Entity\Product', 'p', 'WITH', 'r.productId = p.id')
            ->leftJoin('App\Entity\Users', 'u', 'WITH', 'r.createdBy = u.id')
            ->andWhere('r.isDeleted = 0')
            ->andWhere('r.productId=1');
        //if admin role then bypass else use productTypes Conditions
//        if(!self::$_LOGGED_IN_USER->getIsSuperAdmin())
//        {
//            $query->andWhere('r.ticketTypeId IN (:productTypeIds)')
//            ->setParameter(':productTypeIds', $_SESSION['user']['productTypes']);
//		}
        if(isset($requestParams['listing']) && $requestParams['listing']=='true')
        { 
            $query->andWhere('r.id!='.Constant::ROLE_ID_GUEST);
        }
        if(!empty($requestParams['searchTerm']))
        {
            $query->andWhere('r.name like :keyword')
            ->setParameter('keyword', '%' . $requestParams['searchTerm'] . '%');
        }
         
        $total = count($query->getQuery()->getResult());
        if($limit)
            $query->setFirstResult($start)->setMaxResults($limit);
        $results = array();
        $result = $query->getQuery()->getResult();
        if (empty($result)) {
            $return['statusCode'] = 404;
            $return['status'] = 'F';
            $return['message'] = 'No Roles found';
        } else {
            $return['statusCode'] = '200';
            $return['status'] = 'T';
            $return['message'] = 'Roles Listed Successfully';
            $return['data'] = $result;
        }
        return $return;
    }
    
    /**
    * @author Bhanu
    * @name checkDuplicate
    * @description Check the posted data exists 
    * @param name
    * @param role_id
    * @return role details array
    * 
    */
    public function checkDuplicate($name,$role_id='',$productId)
    {    
        $qb   =  $this->entityManager->getRepository('App\Entity\AclRoles')->createQueryBuilder('r');
        if(isset($role_id) && $role_id > 0)
        {
            // condtion true while adding a capability
            $data =  $qb->where('r.id !=:id')
                        ->andWhere('r.name=:name')
                        ->andWhere('r.productId=:productId')
                        ->setParameter('id', $role_id)
                        ->setParameter('name', $name)
                        ->setParameter('productId', $productId)
                        ->getQuery()->getResult();
        }
        else
        {
            // condtion true while editing a capability
            $data =  $qb->where('r.name=:name')
                        ->andWhere('r.productId=:productId')
                        ->setParameter('name', $name)
                        ->setParameter('productId', $productId)
                        ->getQuery()->getResult();
        }
        $data = array_map(
            function ($roleData) {
                 return $roleData->getArrayCopy();
            },
            $data
        );
            
        return $data;
    }
    /**
    * @author Bhanu
    * @since 16/June/2016
    * @name getRoles
    * @description Retrieves the Role Details by Id
    * @param id
    * @return role details array
    * 
    */
    public function getRole($id)
    {
        $return = array(
            'statusCode' => 404,
            'status' => 'F',
            'message' => 'Role Not Found',
            'data' => null
        );
        $role = $this->entityManager->getRepository('App\Entity\AclRoles')->findOneBy(
            array('id' => $id, 'isDeleted' => 0)
        );
	    if ($role) {
            $role = $role->objectToArray($role);
            if($role['mappedSubsorceId']) {
                $subScidarr = explode(',', $role['mappedSubsorceId']);
                $modelData = array();
                foreach($subScidarr as $subId) {
                    $modelData[] = array('id'=>$subId);
                }
                $modelData = json_encode($modelData);
            }
             $role['modelData'] = $modelData;
            foreach($role_products as $p){
                $role['product'][] = $p->getProduct()->getId();
            }
            $return['statusCode'] = '200';
            $return['status'] = 'T';
            $return['message'] = 'Role Found Successfully';
            $return['data'] = $role;
        }
        return $return;
    }
    
    /**
     * @author Bhanu
     * @since 04/March/2016
     * @name addRole
     * @description Saves the role
     * @param $data
     * @return Inserted Id
     */
    public function addRole($data)
    {   
        $return = array(
            'statusCode' => 200,
            'status' => 'F',
            'message' => 'Role Not Saved',
            'data' => null
        );
        if(!self::$_LOGGED_IN_USER->getIsSuperAdmin()){
            $data['roleTypeId'] = self::$_ROLE_TYPE_ID;
        }
        
        if(!$data['roleTypeId']){
            $data['roleTypeId'] = Constant::INS_BUSINESS_TYPE_UCD;
        }
        $role = new AclRoles();
        $role->setName(ucfirst($data['name']));
        $role->setDescription($data['description']);
        $role->setCreatedBy($data['created_by']);
        $role->setProductId($data['product']);
        $role->setTicketTypeId($data['ticketTypeId']);
        $role->setRoleTypeId($data['roleTypeId']);
        $role->setWhitelistIp($data['whitelistIp']);
        $role->setBlockedIp($data['blockedIp']);
        $role->setStatus(1);
        if(isset($data['autoActiveUser'])){
            $role->setAutoActiveUser($data['autoActiveUser']?1:0);
        }
        if(isset($data['modelData'])) {
          $mapperId = array();
          foreach($data['modelData'] as $subSorceId) {
             $mapperId[] =  $subSorceId['id'];
          } 
          $mapperIdStr = implode(',', $mapperId);
          $role->setMappedSubsorceId($mapperIdStr);
        }
        $now = new \DateTime("now");
        $role->setCreatedAt($now);
        $role->setModifiedAt($now);
        //$permission_ids = $data['permission_ids'];
        $this->entityManager->persist($role);
        $this->entityManager->flush();
        $role_id = $role->getId();
        $return['status']= 'T';
        $return['data']['id'] = $role_id;
        $return['statusCode'] = 200;
        $return['message'] = 'Role Created Successfully.';
        //$permission = $this->rolePermission($permission_ids,$role_id);
        return $return;
    }

    /**
    * @author Bhanu
    * @since 06/June/2016
    * @description Updates the role permissions
    * @param array $data
    * @return Number|id
    */
    public function rolePermission($permission_ids,$role_id)
    {
        try{
          $resources = $permission_ids;
          $role_id = $role_id;

          $role_permissions = $this->entityManager
              ->getRepository('App\Entity\AclRolePermission')
              ->findBy(array(
                'roleId' => $role_id
              )
           );
           foreach ($role_permissions as $rolePer) {
              $this->entityManager->remove($rolePer);
           }
           if($resources){
               foreach($resources as $perm)
                {
                  $permissionRole = new AclRolePermission();
                  $permissionRole->setRoleId($role_id);
                  $permissionRole->setStatus(1);
                  $permissionRole->setAccess(1);
                  $permissionRole->setResourceId($perm);
                  $permissionRole->setCreatedAt(new \DateTime("now"));
                  $permissionRole->setModifiedAt(new \DateTime("now"));
                  $this->entityManager->persist($permissionRole);
                  $this->entityManager->flush();
                }
           }
          return true;
        } catch(Exception $exec)
        {
          return false;
        }
    }

    /**
    * @author Bhanu
    * @since 04/March/2016
    * @description Updates the role details
    * @param array $data
    * @return Number|id
    */
    public function editRole($data)
    { 
        $return = array(
            'statusCode' => 201,
            'status' => 'F',
            'message' => 'Role Not Updated',
            'data' => null
        );
        $role = $this->entityManager->getRepository('App\Entity\AclRoles')->findOneBy(
            array('id' => $data['id'],
                'isReadOnly' => 0)
        );
        if (!$role) {
            $return['status']= 'F';
            $return['statusCode'] = 404;
            $return['message'] = 'Role Not Found';
            return $return;
        }
       
        $now = new \DateTime("now");
        $role->setName(ucfirst($data['name']));
        $role->setDescription($data['description']);
        //$role->setCreatedBy($data['created_by']);
        $role->setProductId($data['productId']);
        $role->setTicketTypeId($data['ticketTypeId']);
        $role->setWhitelistIp($data['whitelistIp']);
        if($data['roleTypeId']){
            $role->setRoleTypeId($data['roleTypeId']);
        }
        
        if(isset($data['autoActiveUser'])){
            $role->setAutoActiveUser($data['autoActiveUser']?1:0);
        }
        if(isset($data['modelData'])) {
          $mapperId = array();
          foreach($data['modelData'] as $subSorceId) {
             $mapperId[] =  $subSorceId['id'];
          } 
          $mapperIdStr = implode(',', $mapperId);
          $role->setMappedSubsorceId($mapperIdStr);
        }
        
        $role->setBlockedIp($data['blockedIp']);
        $role->setStatus(1);
        //$permission_ids = $data['permission_ids'];
        $role->setModifiedAt($now);
        $this->entityManager->persist($role);
        $this->entityManager->flush();
        $return['status']= 'T';
        $return['data'] = $role;
        $return['statusCode'] = 201;
        $return['message'] = 'Role Updated Successfully.';
        return $return;
    }

    public function validate($type, $data = array()){
        return true;
        // switch ($type) {
        //     case 'addRole':
        //             $validator = v::key('created_by', v::notEmpty())
        //                     ->key('name', v::notEmpty()->regex('/^[a-zA-Z ]{1}[ a-zA-Z0-9]*$/i'))
        //                     //->key('permission_ids', v::notEmpty())
        //                     ->key('product', v::notEmpty())
        //                     ->key('ticketTypeId', v::notEmpty())
        //                     //->key('roleTypeId', v::notEmpty())
        //             ;
        //             break;
        //     case 'editRole':
        //             $validator = v::key('name', v::notEmpty()->regex('/^[a-zA-Z ]{1}[ a-zA-Z0-9]*$/i'))
        //                     //->key('permission_ids', v::notEmpty())
        //                     ->key('productId', v::notEmpty())
        //                     ->key('ticketTypeId', v::notEmpty());
        //             break;
        //     default:
        //             break;
        // }
        // try {
        //     $validator->assert($data);
        //     return true;
        // } catch (\InvalidArgumentException $e) {
        //     $errors = $e->findMessages([
        //         'name' => 120,
        //         'name.notEmpty' => 120,
        //         'name.regex' => 121,
        //         'permission_ids' => 122,
        //         'permission_ids.notEmpty' => 122,
        //         'created_by' => 123,
        //         'created_by.notEmpty' => 123,
        //         'product_id' => 124,
        //         'product_id.notEmpty' => 124,
        //         'productId' => 124,
        //         'productId.notEmpty' => 124,
        //         'roleTypeId' => 130,
        //         'roleTypeId.notEmpty' => 130
        //     ]); 
        //     $this->errors = $e->getMessages();
            
        //     return false;
        // }
    }

    /**
    * @author Bhanu
    * @since 30/May/2016
    * @description Deletes a role
    * @param array $id
    * @return null|boolean
    */
    public function deleteRole($id){
        $return = array(
            'statusCode' => 200,
            'status' => 'F',
            'message' => 'Role Not Deleted',
            'data' => null
        );
        $roleData = $this->entityManager->getRepository('App\Entity\AclRoles')->findOneBy(
            array('id' => $id)
        );
        if (!$roleData) {
            $return['status']= 'F';
            $return['statusCode'] = 404;
            $return['message'] = 'Role Not Found';
            return $return;        
        } 
        $roleData->setStatus(0);
        $roleData->setIsDeleted(1);
        $this->entityManager->persist($roleData);
        $this->entityManager->flush();
        $return['status']= 'T';
        $return['data']['id'] = $id;
        $return['statusCode'] = 200;
        $return['message'] = 'Role Deleted Successfully.';
        return $return;
    }

    public function getErrors(){
        return $this->errors;
    }
	
    /**
     * Get array copy of object
     *
     * @return array
     */
    public function getArrayCopy() {
        return get_object_vars($this);
    }

    public function objectToArray($object) {
       if (!is_object($object) && !is_array($object)) {
           return $object;
       }
       if (is_object($object) && !preg_match("/entity/i", get_class($object))) {
           return $object;
       }
       if (is_object($object) && preg_match("/entity/i", get_class($object))) {
           $object = $object->getArrayCopy();
       }
       return array_map(
                       function ($objList) {
                   return $this->objectToArray($objList);
               }, $object
               );
   }
    
    /**
     * @author Umardeen
     * @name getPermissionWithCategory
     * @description Retrieves the All capabilities With Categories
     * @param $args
     * @return details array
     * 
     */
    public function getPermissionWithCategory($args) {
        $return = array(
            'statusCode' => 200,
            'status' => 'F',
            'message' => 'No Role Permissions Found',
            'data' => null
        );
        
        $query = $this->entityManager->getRepository('App\Entity\AclPermissions')
            ->createQueryBuilder('permission')
                ->select("permission.allowedGroupCategory","permission.allowGroups","permission.id","permission.code","permission.label","permission.categoryId","permissionCateg.name")
                ->addSelect("CASE WHEN rolePermission.id IS NULL THEN false ELSE true END as status")
                ->join('App\Entity\AclPermissionCategories','permissionCateg','WITH','permissionCateg.id = permission.categoryId')
                ->leftJoin('App\Entity\AclRolePermissions', 'rolePermission','WITH',"rolePermission.roleId = {$args['roleId']} AND rolePermission.permissionId = permission.id")
                ->groupBy('permission.id')
                ->addOrderBy("permission.rank")
                ;
//                die($query->getQuery()->getSQL());
        $returnData = array();
        foreach($query->getQuery()->getResult() as $permissionRow){
          $returnData[$permissionRow['categoryId']]['name'] = $permissionRow['name'];
          $returnData[$permissionRow['categoryId']]['role'][] = array(
              'id' => $permissionRow['id'],
              'label' => $permissionRow['label'],
              'code' => $permissionRow['code'],
              'status' => $permissionRow['status']?true:false,
              'allowGroups' => $permissionRow['allowGroups']?true:false,
              'allowedGroupCategory' => $permissionRow['allowedGroupCategory'] ? explode(",",$permissionRow['allowedGroupCategory']) : array()
              );
        }
        usort($returnData, function ($item1, $item2) {
            if($item2['name'] == $item1['name']){
                return 0;
            }
            return $item2['name'] < $item1['name'];
        });
        if (empty($returnData)) {
            $return['statusCode'] = 404;
            $return['status'] = 'F';
            $return['message'] = 'No Role Permissions found';
        } else {
            $return['statusCode'] = '200';
            $return['status'] = 'T';
            $return['message'] = 'Role Permissions Listed Successfully';
            $return['data'] = $returnData;
        }
        return $return;
                
        // return array_values($return);
    }
    /**
     * @author Umardeen
     * @name updatePermissions
     * @description Retrieves the All capabilities With Categories
     * @param $data
     * @return details array
     * 
     */
    public function updatePermissions($data) {
        
        $return = array(
            'statusCode' => 200,
            'status' => 'F',
            'message' => 'Unable to Update Permissions',
            'data' => null
        );        
        $permission = $this->entityManager->getRepository('App\Entity\AclPermissions')->findOneBy(
            array(
                'id' => $data['permissionId']
            )
        );
        
        if(!self::$_LOGGED_IN_USER->getIsSuperAdmin() && (!$permission->getCode() || !isset(self::$_USER_CAPABILIES[$permission->getCode()]))){
            $return['statusCode'] = '500';
            $return['status'] = 'F';
            $return['message'] = 'Invalid Permission';
            return $return;
        }
        
        $rolePermission = $this->entityManager->getRepository('App\Entity\AclRolePermissions')->findOneBy(
                array(
                    'roleId' => $data['roleId'],
                    'permissionId' => $data['permissionId']
                )
        );
        if (!$data['status']) {
            if($rolePermission){
                $rolePermissionId = $rolePermission->getId();
                $deleteQuery = $this->entityManager->createQueryBuilder()
                    ->delete('App\Entity\PermissionGroups', 'permGroup')
                    ->where('permGroup.permissionId = :permissionId')
                    ->setParameter('permissionId', $rolePermissionId)
                    ->getQuery();
                $deleteQuery->execute();
                $this->entityManager->remove($rolePermission);
                $this->entityManager->flush();
                
            }
            $return['statusCode'] = '200';
            $return['status'] = 'T';
            $return['message'] = 'Removed Permission';
        } elseif ($data['status']) {
            if (!$rolePermission) {
                $rolePermission = new AclRolePermissions();
                $rolePermission->setRoleId($data['roleId']);
                $rolePermission->setPermissionId($data['permissionId']);
                $this->entityManager->persist($rolePermission);
                $this->entityManager->flush();
            }
            $rolePermissionId = $rolePermission->getId();
            $deleteQuery = $this->entityManager->createQueryBuilder()
                    ->delete('App\Entity\PermissionGroups', 'permGroup')
                    ->where('permGroup.permissionId = :permissionId')
                    ->setParameter('permissionId', $rolePermissionId)
                    ->getQuery();
            $deleteQuery->execute();
            foreach($data['groups'] as $group){
                $permissionGroup = new PermissionGroups();
                $permissionGroup->setGroupId($group);
                $permissionGroup->setPermissionId($rolePermissionId);
                $this->entityManager->persist($permissionGroup);
                $this->entityManager->flush();
            }            
            $return['statusCode'] = '200';
            $return['status'] = 'T';
            $return['message'] = 'Added Permission';        }
        return $return;
    }
    
    public function getAllRoles($requestParams)
    {
        $return = array(
            'statusCode' => 200,
            'status' => 'F',
            'message' => 'No Roles Found',
            'data' => null
        );
        $query = $this->entityManager->getRepository('App\Entity\AclRoles')
            ->createQueryBuilder('role')
            ->select(
                'role.id','role.name'
            )
            ->andWhere('role.isDeleted = 0')
            ->andWhere('role.productId=1')
                ->orderBy('role.name');
        if(!empty($requestParams['searchTerm']))
        {
            $query->andWhere('role.name like :keyword')
            ->setParameter('keyword', '%' . $requestParams['searchTerm'] . '%');
        }
        $result = $query->getQuery()->getResult();

        if (empty($result)) {
            $return['statusCode'] = 404;
            $return['status'] = 'F';
            $return['message'] = 'No Roles found';
        } else {
            $return['statusCode'] = '200';
            $return['status'] = 'T';
            $return['message'] = 'Roles Listed Successfully';
            $return['data'] = $result;
        }
        return $return;
    }

}

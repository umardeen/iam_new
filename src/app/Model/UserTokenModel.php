<?php

namespace App\Model;

use App\Model\Root\AbstractModel;
use App\Entity\UserTokens;
use App\Constant as Constant;
/**
 * Class Model
 * @package App
 */
class UserTokenModel extends AbstractModel
{
    
    /**
    * @author Bhanu
    * @name authenticate
    * @param $headers
    * @return array
    */
    public function authenticate($x_auth_id, $x_auth_token) {
        $userId = $x_auth_id;
        $accessToken = $x_auth_token;
        $response = array();
        if (empty($userId) || empty($accessToken)) {
            $response['status'] = false;
            $response['code'] = 401;
            $response['message'] = 'Authentication Headers Required';
            return $response;
        }
        try {
            $userToken = $this->entityManager->getRepository('App\Entity\UserTokens')->findOneBy(
                    array('user_id' => $userId, 'access_token' => $accessToken)
            );
            if (empty($userToken)) {
                $response['status'] = false;
                $response['code'] = 401;
                $response['message'] = 'Invalid Credentials.';
            } else {
                $user = $this->entityManager->getRepository('App\Entity\Users')->findOneBy(
                                array('id' => $userId)
                        )->getArrayCopy();
                //check if user is active or not
                if (!$user['status']) {
                    $response['status'] = false;
                    $response['code'] = 401;
                    $response['message'] = 'Inactive User.';
                    if($userId != Constant::CRON_USER_ID){
                        $this->__deleteUserLogin($userToken);
                    }
                } elseif(!$user['isLogin']) {
                    $response['status'] = false;
                    $response['code'] = 401;
                    $response['message'] = 'Please, Login first.';
                    if($userId != Constant::CRON_USER_ID){
                        $this->__deleteUserLogin($userToken);
                    }
                }else{
                    //$this->__updateUserLogin($userTokenArray['id']);
                    //check its Roles & Products & ProductTypes
//                    $qb = $this->entityManager->getRepository('App\Entity\AclRoleUser')->createQueryBuilder('ARU');
//                    $rolesProducts = $qb->select('GROUP_CONCAT(ARU.roleId) as roles, GROUP_CONCAT(Role.productId) as products, GROUP_CONCAT(Role.ticketTypeId) as productTypes')
//                            ->leftJoin('App\Entity\AclRoles', 'Role', 'WITH', 'Role.id = ARU.roleId AND Role.status = 1')
//                            ->where('ARU.userId = :user_id')
//                            ->setParameter('user_id', $user['id'])
//                            ->addGroupBy('ARU.userId')
//                            ->getQuery()
//                            ->getResult();
//                    if ($rolesProducts) {
//                        $user['roles'] = explode(',', $rolesProducts[0]['roles']);
//                        $user['products'] = explode(',', $rolesProducts[0]['products']);
//                        $user['productTypes'] = explode(',', $rolesProducts[0]['productTypes']);
//                    }
                    $response['status'] = true;
                    $response['code'] = 200;
                    $response['user'] = $user;
                    $response['message'] = 'Login Successfull';
                }
            }
        } catch (Exception $exception) {
            $response['status'] = false;
            $response['code'] = 500;
            $response['message'] = $exception->getMessage();
        }
        return $response;
    }

    /**
    * @author Bhanu
    * @name sessionTimeOutRemoveToken
    * @param $headers
    * @return array
    */
    public function sessionTimeOutRemoveToken($headers)
    {
        $userId = isset($headers['HTTP_X_AUTH_ID'][0]) ? $headers['HTTP_X_AUTH_ID'][0] : '';
        $accessToken = isset($headers['HTTP_X_AUTH_TOKEN'][0]) ? $headers['HTTP_X_AUTH_TOKEN'][0] : '';
        
        if($accessToken != '0696aa04f49b05da1730a9e573c1aafc'){
            $userToken = $this->entityManager->getRepository('App\Entity\UserTokens')->findOneBy(
                        array('user_id' => $userId, 'access_token' => $accessToken)
                    );

            if($userToken->getLastLoginTime()){
                $idleTime = 10*60;
                $lastupdatedtime = $userToken->getLastLoginTime()->getTimestamp();
                if(time()-$lastupdatedtime > $idleTime){
                    $this->__deleteUserLogin($userToken);
                }
            }
        }
       
    }
    
    
/**
 * @author Bhanu
 * @name deleteUserLogin
 * @since 22/June/2016
 * @description Delete the user login from tokens table
 * 
 */
	private function __deleteUserLogin($userToken)
	{
            if($userToken){
		$this->entityManager->remove($userToken);
                $this->entityManager->flush();
            }
	}

 /**
 * @author Bhanu
 * @name updateUserLogin
 * @since 22/June/2016
 * @description Update the user login time in tokens table
 * 
 */
	private function __updateUserLogin($id)
	{
        $userData = $this->entityManager->getRepository('App\Entity\UserTokens')->findOneBy(
            array('id' => $id)
        );
		if($userData)
		{
		    $userData->setLastLoginTime();
		    $this->entityManager->flush();
		}
	}

    public function generateToken() {
    	
        
    }
    
    //TODO Add Token Will Have a method which will opt for previous token retreival , so no need to generate same tokens over and over
    /**
     * @name addToken
     * @param string|null $slug
     * @return inserted id
     */
    public function addToken($user_id)
    {
        $uToken = new UserTokens();
        $token = md5(uniqid(rand(), true));
        $uToken->setAccessToken($token);
        $uToken->setUserId($user_id);
        $uToken->setCreated();
        $this->entityManager->persist($uToken);
        $this->entityManager->flush();
        if($uToken->getId()){
            return $token;  
        }else{
            return false;
        }
    }	
	
    /**
     * @author Agam
	 * @name checkRestrictionIP
	 * @since 1/Sep/2016
	 * @description checks the ip restrictions of user
     */
    public function checkRestrictionIP($user_id, $ipAddress)
    {
    	// print_r($user_id);exit;
		if(!empty($user_id))
		{
			$roles = $this->entityManager->getRepository('App\Entity\AclRoleUser')
				->createQueryBuilder('aru')
				->leftJoin('App\Entity\AclRoles', 'ar', 'WITH', 'aru.roleId = ar.id')
				->select('ar.id,ar.whitelistIp, ar.blockedIp')
				->where('aru.userId = :userId')
				->setParameter('userId', $user_id)
				->getQuery()
				->getResult();
			$isValidIp = true;
			foreach($roles as $role)
			{
				//check only until valid Ip is FALSE
				if($isValidIp)
				{
					//meaning the IP should be in the given Ip's 
					if(!empty($role['whitelistIp']))
					{
						$isValidIp = false;
						$ips = explode(',', $role['whitelistIp']);
						foreach($ips as $whiteIp)
						{
							if(!$isValidIp)
							{
								//if IP is range
								if (strpos($whiteIp, '-') !== false)
								{
									if(self::__ip_in_range($ipAddress, $whiteIp))
									{
										$isValidIp = true;
									}
								} else {
									//IP is Single
									if($ipAddress == $whiteIp)
									{
										$isValidIp = true;
									}
								}
							}
						}
					}
					//meaning the IP should not be in the given Ip's 
					if(!empty($role['blockedIp']))
					{
						$ips = explode(',', $role['blockedIp']);
						foreach($ips as $blockIp)
						{
							if($isValidIp)
							{
								//if IP is range
								if (strpos($blockIp, '-') !== false)
								{
									if(self::__ip_in_range($ipAddress, $blockIp))
									{
										$isValidIp = false;
									}
								} else {
									//IP is Single
									if($ipAddress == $blockIp)
									{
										$isValidIp = false;
									}
								}
							}
						}
					}
				}
			}
			if($isValidIp)
			{
				return array('status' => true, 'message' => 'IP Valid');
			} else
			{
				return array('status' => false, 'message' => 'IP Not allowed');
			}
		} else
		{
			return array('status' => false, 'message' => 'Invalid User');
		}
	}
	
	private function __ip_in_range($ip, $range )
	{
		list($range_low, $range_high) = explode('-', $range);
		$range_low = ip2long($range_low);
		$range_high = ip2long($range_high);
		$ip = ip2long($ip);
		return ($ip >= $range_low && $ip <= $range_high);
	}

    /**
     * @name registerDeviceToken
     * @param string|null $slug
     * @return inserted id
     */
    public function registerDeviceToken($headers, $deviceToken = '')
    {
    	$userId = isset($headers['HTTP_X_AUTH_ID'][0]) ? $headers['HTTP_X_AUTH_ID'][0] : '';
		$accessToken = isset($headers['HTTP_X_AUTH_TOKEN'][0]) ? $headers['HTTP_X_AUTH_TOKEN'][0] : '';
    	$userToken = $this->entityManager->getRepository('App\Entity\UserTokens')->findOneBy(
            array('user_id' => $userId, 'access_token' => $accessToken)
        );
        if($userToken)
        {
        	if($deviceToken)
            {
                $putData = array(
                    'deviceToken' => $deviceToken,
                    'accessToken' => $accessToken,
                    'userId' => $userId,
                    'action' => 'add'
                );
            } else
            {
                $putData = array(
                    'deviceToken' => $userToken->getDeviceToken(),
                    'userId' => $userId,
                    'accessToken' => $accessToken,
                    'action' => 'delete'
                );
            }
        	$userToken->setDeviceToken($deviceToken);
	        $this->entityManager->persist($userToken);
	        $this->entityManager->flush();
	        if($userToken->getId()){
	        	$this->__deviceTokenUpdateOneView($putData, $headers);
	            return true;  
	        }
        }
        return false;
    }

    private function __deviceTokenUpdateOneView($putData, $headers) {
        try {
            $qf = $this->entityManager->getRepository('App\Entity\Configuration')->createQueryBuilder('cf');
            $query = $qf->select('cf.config_values')
                ->Where('cf.config_name=:name')
                ->setParameter('name', 'SOCKET_API_URL');
            $configData = $query->getQuery()->getSingleResult();
            $oneViewApiUrl = $configData['config_values'];
            $userId = isset($headers['HTTP_X_AUTH_ID'][0]) ? $headers['HTTP_X_AUTH_ID'][0] : '';
			$accessToken = isset($headers['HTTP_X_AUTH_TOKEN'][0]) ? $headers['HTTP_X_AUTH_TOKEN'][0] : '';
            $oneViewApiHeader = array('x-auth-token:' . $accessToken, 'x-auth-id:' . $userId);
			$this->curlRequest($oneViewApiUrl . 'api/consumer/deviceToken', $putData, $oneViewApiHeader, 'PUT');
        } catch(\Exception $ee){}
    }

    /**
     * @name listAllDeviceTokens
     * @author Agam
     * @return inserted id
     */
    public function listAllDeviceTokens()
    {
        $userTokens = $this->entityManager->getRepository('App\Entity\UserTokens')->createQueryBuilder('uToken')
                ->select('uToken.user_id as userId, uToken.device_token as Token, uToken.access_token as accessToken')
                ->where('uToken.device_token IS NOT NULL')
                ->getQuery()->getResult();
        if($userTokens)
        {
            return $userTokens;  
        }
        return array();
    }   

 /**
 * @author Ruchi
 * @name checkB2BDashboardPermission
 * @since 22/June/2016
 * @description Update the user login time in tokens table
 * 
 */
	public function checkB2BDashboardPermission($id,$token)
	{
        $query = $this->entityManager->getRepository('App\Entity\UserTokens')
                ->createQueryBuilder('ut');
        $result= $query->Where('ut.user_id = :user_id')
                    ->andWhere('ut.access_token = :token')
                    ->setParameter('user_id', $id)
                    ->setParameter('token', $token)
                    ->getQuery()
          			->getResult();
         if($result)
         {
          	return $id;
         }
         else
         {
          	return array();
          				
         }
	}
        
        public function removeToken($loginId,$token)
        {
            $userToken = $this->entityManager->getRepository('App\Entity\UserTokens')->findOneBy(
                        array('user_id' => $loginId, 'access_token' => $token)
                    );
            $this->__deleteUserLogin($userToken);
        }
        
        public function removeAllToken($loginId)
        {
            if($loginId != Constant::CRON_USER_ID){
                $userToken = $this->entityManager->getRepository('App\Entity\UserTokens')->findBy(
                            array('user_id' => $loginId)
                        );
                if(!empty($userToken)){
                    foreach($userToken as $k=>$v){
                        $this->__deleteUserLogin($v);
                    }
                }
            }
            return true;
        }
        
        public function getExistingActiveToken($user_id)
        {
            $token="";
            $user = $this->entityManager->getRepository('App\Entity\Users')->findOneBy(
                array('id' => $user_id, 'status' => 1,'isDeleted' => 0, 'isLogin' =>1)
            );
            if(!empty($user)){
                $query = $this->entityManager->getRepository('App\Entity\UserTokens')
                ->createQueryBuilder('ut');
                $userToken= $query->Where('ut.user_id = :user_id')
                            ->setParameter('user_id', $user_id)->orderBy('ut.id','DESC')
                            ->setMaxResults(1)->getQuery()->getResult();
                
                if(!empty($userToken)){
                    $token = $userToken[0]->getAccessToken();
                }
            }
            return $token;
        }
        
}
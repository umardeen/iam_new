<?php 
echo 'Initiating the Constant File Generation . </br>';
// Instantiate the app
$mysql = require __DIR__ . '/../config/mysql.php';
$settings = $mysql();
$servername = $settings['connection']['master']['host'];
$username = $settings['connection']['master']['user'];
$password = $settings['connection']['master']['password'];
$dbname = $settings['connection']['master']['dbname'];


// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
	echo 'Database Connection Failed';
	exit(1);
}
$sql = "SELECT key_name, value FROM tbl_constants ORDER BY id ASC";
$constantArray = array();
$result = $conn->query($sql);
$stringToReplace = "\r\n";
if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
    	$rowValue = is_numeric($row['value']) ? $row['value'] : "'" . $row['value'] . "'";
    	$stringToReplace .= "	const {$row['key_name']} = {$rowValue};\r\n";
    }
} else
{
	echo 'No Record Found In Constant Table';
	exit(1);
}
$filePath = __DIR__ . '/../src/app/Constant.php';
$phpContent = htmlentities(file_get_contents($filePath));
$startPoint='//-------------------------CONSTANTS_START-------------------------//';
$endPoint='//-------------------------CONSTANTS_END-------------------------//';
$newConstantString = replaceTags($startPoint, $endPoint, $stringToReplace, $phpContent);
function replaceTags($startPoint, $endPoint, $newText, $source) {
	return preg_replace('#('.preg_quote($startPoint).')(.*)('.preg_quote($endPoint).')#si', '$1'.$newText.'$3', $source);
}
$Write = file_put_contents($filePath, html_entity_decode($newConstantString));
if(!$Write)
{
	echo 'Constant File Permission Error';
	exit(1);
}
echo 'Successfully Generated Constant File';

?>